#!/usr/bin/env python
# coding: utf-8

from typing import TypeVar, List, Tuple, Callable  # , Set, Any

# Some generic types

A = TypeVar('A')

B = TypeVar('B')

C = TypeVar('C')

D = TypeVar('D')


# # Lists

# ## Eliminators

def head(xs: List[A]) -> A:
    if (xs == []):
        raise ValueError("List is empty!")
    else:
        return xs[0]


def tail(xs: List[A]) -> List[A]:
    if (xs == []):
        raise ValueError("List is empty!")

    elif len(xs) == 1:
        return []
    else:
        return xs[1:]


# ## Constructors

def cons(a: A) -> Callable[[List[A]], List[A]]:
    return lambda l: [a] + l


# ## Pattern Matching

def is_nil(xs: List[A]) -> bool:
    return xs == []


def is_singleton(xs: List[A]) -> bool:
    return len(xs) == 1


# attention: in Python, a singleton list `[x]` is *not* `cons(x,[])`
#            thus `[x][0] ~> x` but `[x][1] ~> index out of bound error`!
def is_cons(xs: List[A]) -> bool:
    return xs != []
    # better a three cases variant?
    # return len(xs) > 1



# # Tuples

def fst(ab: Tuple[A, B]) -> A:
    return ab[0]


def snd(ab: Tuple[A, B]) -> B:
    return ab[1]


# Preparation for category of types

# `domain` and `codomain` actually don't work the way they were intended to.

def domain(_: Callable[[A], B]) -> type:
    return A


def codomain(_: Callable[[A], B]) -> type:
    return B


def identity(a: A) -> A:
    return a


def compose(f: Callable[[B], C], g: Callable[[A], B]) -> Callable[[A], C]:
    return lambda x: f(g(x))

