#!/usr/bin/env python
# coding: utf-8

# imports

from typing import Callable

# pythonLibs imports


# ------------------
# # Category Theory
# ------------------


class Category:

    def __init__(self, obj, mor, dom, cod, id_mor, compose):
        self.obj = obj
        self.mor = mor
        self.dom = dom
        self.cod = cod
        self.id_mor = id_mor
        self.compose = compose

    def obj(self) -> type:
        return self.obj

    def mor(self) -> type:
        return self.mor

    def dom(self) -> Callable[[mor], obj]:
        return self.dom

    def cod(self) -> Callable[[mor], obj]:
        return self.cod

    def id_mor(self) -> Callable[[obj], mor]:
        return self.id_mor

    def compose(self) -> Callable[[mor, mor], mor]:
        return self.compose


class Functor:

    def __init__(self, src, tgt, f_obj, f_mor):
        self.src = src
        self.tgt = tgt
        self.f_obj = f_obj
        self.f_mor = f_mor

    def src(self) -> Category:
        """returns the domain category of the functor."""
        return self.src

    def tgt(self) -> Category:
        """returns the codomain category of the functor."""
        return self.tgt

    def __so(self) -> type:
        return self.src.obj

    def __sm(self) -> type:
        return self.src.mor

    def __to(self) -> type:
        return self.tgt.obj

    def __tm(self) -> type:
        return self.tgt.mor

    def f_obj(self) -> Callable[[__so], __to]:
        """returns the object function of the functor."""
        return self.f_obj

    def f_mor(self) -> Callable[[__sm], __tm]:
        """returns the morphism function of the functor."""
        return self.f_mor


class Monad:

    def __init__(self, m_functor: Functor, m_pure, m_join):
        self.m_functor = m_functor
        self.m_pure = m_pure
        self.m_join = m_join

    def m_functor(self) -> Functor:
        """returns the monad functor."""
        return self.m_functor

    # typing notation

    def __mso(self) -> type:
        return self.m_functor.src.obj

    def __M(self, o):
        return self.m_functor.f_obj(o)

    def __m_map(self, f):
        return self.m_functor.f_mor(f)

    # methods

    def m_pure(self, x: __mso) -> '__M(__mso)':
        """returns the `unit`/`pure`/`return` natural transformation of the monad."""
        return self.m_pure(x)

    def m_join(self, mmx: '__M(__M(__mso))') -> '__M(__mso)':
        """returns the `join`/`mu` natural transformation of the monad."""
        return self.m_join(mmx)

    def m_bind(self, f: Callable[[__mso], '__M(__mso)'], ma: '__M(__mso)') -> '__M(__mso)':
        """default implementation of `bind(f,ma)` as `join(f_mor(f,ma))`"""
        return self.m_join(self.__m_map(f)(ma))


class ContainerMonad(Monad):

    def __init__(self, m_functor: Functor, m_pure, m_join, m_elem, m_not_empty, m_all):
        super().__init__(m_functor, m_pure, m_join)
        self.m_elem = m_elem
        self.m_not_empty = m_not_empty
        self.m_all = m_all

    # typing notation

    def __mso(self) -> type:
        return super().m_functor.src.obj

    def __M(self, o):
        return super().m_functor.f_obj(o)

    def m_elem(self, x:__mso, mx: '__M(__mso)') -> bool:
        """returns the membership test of the container"""
        return self.m_elem(x, mx)

    def m_not_empty(self, mx: '__M(__mso)') -> bool:
        """returns the non-emptiness test of the container"""
        return self.m_not_empty(mx)

    Pred = Callable[[__mso], bool]

    def m_all(self, P: Pred, mx: '__M(__mso)') -> bool:
        """returns the forall-test function of the container"""
        return self.m_elem(P, mx)

