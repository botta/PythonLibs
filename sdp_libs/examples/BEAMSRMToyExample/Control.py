# TODO: - Harmonize types with AbstractControlClass
#       - Controls parametrized by time step
#       - Improve docstrings
#       - Add more controls

# imports

## project

from sdp_libs.sequential_decision_problems.core_theory_dbc_parent import AbstractControlClass
from sdp_libs.sequential_decision_problems.default_simple_prob_parent import *


# -------------------------------------------------------------------------------------------------------------------- #

# For the moment we do not control anything, the control is just a placeholder
list_of_ctrls: List[bool] = [True]

class Ctrl(AbstractControlClass):
    """A Control class needs to implement the methods `getCtrl` and `checkCtrl` from its parent AbstractControlClass.
    It is convenient to also implement the methods `__repr__` and `__str__` for pretty-printing. """

    list_of_ctrls: List[bool] = list_of_ctrls

    def __init__(self, y: bool):
        """Initializes the object with the given control value y,
        if it is a valid control according to checkCtrl and
        raises a ValueError otherwise."""
        if self.checkCtrl(y):
            self.y = y
        else:
            raise ValueError("Invalid control value!")

    def __repr__(self):
        if self.y == True:
            stry = "noOp"
        else:
            stry = "noOp"

        return "<Control: %s>" % stry

    def __str__(self):
        if self.y == True:
            stry = "noOp"
        else:
            stry = "noOp"

        return stry

    def checkCtrl(self, y: bool) -> bool:
        """Returns `True` if `y` is a valid control and `False` otherwise."""
        if y in self.list_of_ctrls:
            return True
        else:
            return False

    def getCtrl(self) -> bool:
        """Returns the control value. """
        return self.y
