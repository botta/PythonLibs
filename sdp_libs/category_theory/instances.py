# imports

from functools import reduce
import operator

# pythonLibs imports
from sdp_libs.category_theory.definitions import Category, Functor, Monad
from sdp_libs.utils.base import *  # identity, fst, snd, head, tail, domain, codomain, compose
from sdp_libs.probability_theory.base import *


# Some generic types

A = TypeVar('A')

B = TypeVar('B')

C = TypeVar('C')

D = TypeVar('D')

# A category of types
typeCat = Category(type, Callable[[type], type], domain, codomain, id, compose)


# Functors/ Monads

# Deviating from naming convention to emphasize special role of functors

# Identity Monad


def IdFun(X: type) -> type:
    return X


def id_map (f: Callable[[A], B], a : IdFun(A)) -> IdFun(B):
    return f(a)


def id_pure(x: A) -> IdFun(A):
    return x


def id_join(xss: IdFun(IdFun(A))) -> IdFun(A):
    return xss


idFunctor = Functor(typeCat, typeCat, IdFun, id_map)

idMonad = Monad(idFunctor, id_pure, id_join)


# List Monad

def ListFun(X: type) -> type:
    return List[X]


list_map = map


def list_pure(x: A) -> List[A]:
    return [x]


def list_join(xss: List[List[A]]) -> List[A]:
    return sum(xss, [])


def list_map_curry(f: Callable[[A], B]) -> Callable[[List[A]], List[B]]:
    return lambda l: list(map(f, l))

listFunctor = Functor(typeCat, typeCat, ListFun, list_map_curry)

listMonad = Monad(listFunctor, list_pure, list_join)


# Pair functor

def map_pair(f: Callable[[A], C], g: Callable[[B], D], p: Tuple[A, B]) -> Tuple[C, D]:
    c = f(fst(p))
    d = g(snd(p))

    return c, d


def map_pair_list(f: Callable[[A], C], g: Callable[[B], D], abs: List[Tuple[A, B]]) -> List[Tuple[C, D]]:
    return [map_pair(f, g, ab) for ab in abs]


def l_map(f: Callable[[A], C], abs: List[Tuple[A, B]]) -> List[Tuple[C, B]]:
    cbs = map_pair_list(f, identity, abs)
    return cbs


def r_map(g: Callable[[B], D], abs: List[Tuple[A, B]]) -> List[Tuple[A, D]]:
    ads = map_pair_list(identity, g, abs)
    return ads


# SimpleProb monad

GenSimpleProbFun = SimpleProb


def FastSimpleProbFun(X: type) -> type:
    return List[Tuple[X, float]]


def ExactSimpleProbFun(X: type) -> type:
    return List[Tuple[X, Fraction]]


def sp_map(f):
    return lambda x: l_map(f, x)


def sp_pure(a: A) -> GenSimpleProbFun(A):
    return [(a, 1)]


def sp_join(mma: GenSimpleProbFun(GenSimpleProbFun(A))) -> GenSimpleProbFun(A):

    sp_lists = [r_map(lambda inner: inner * snd(ma), fst(ma)) for ma in mma]
    return reduce(operator.concat, sp_lists)


def sp_bind(ma: GenSimpleProbFun(A), f: Callable[[A], GenSimpleProbFun(B)]) -> GenSimpleProbFun(B):
    return sp_join(sp_map(f)(ma))


genSimpleProbFunctor = Functor(typeCat, typeCat, GenSimpleProbFun, sp_map)

genSimpleProbMonad = Monad(genSimpleProbFunctor, sp_pure, sp_join)

