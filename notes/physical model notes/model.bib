@article{Gregory2000,
	abstract = {In response to increasing atmospheric concentrations of greenhouse gases, the rate of time-dependent climate change is determined jointly by the strength of climate feedbacks and the efficiency of processes which remove heat from the surface into the deep ocean. This work examines the vertical heat transport processes in the ocean of the HADCM2 atmosphere-ocean general circulation model (AOGCM) in experiments with CO2 held constant (control) and increasing at 1% per year (anomaly). The control experiment shows that global average heat exchanges between the upper and lower ocean are dominated by the Southern Ocean, where heat is pumped downwards by the wind-driven circulation and diffuses upwards along sloping isopycnals. This is the reverse of the low-latitude balance used in upwelling-diffusion ocean models, the global average upward diffusive transport being against the temperature gradient. In the anomaly experiment, weakened convection at high latitudes leads to reduced diffusive and convective heat loss from the deep ocean, and hence to net heat uptake, since the advective heat input is less affected. Reduction of deep water production at high latitudes results in reduced upwelling of cold water at low latitudes, giving a further contribution to net heat uptake. On the global average, high-latitude processes thus have a controlling influence. The important role of diffusion highlights the need to ensure that the schemes employed in AOGCMs give an accurate representation of the relevant sub-grid-scale processes.},
	author = {Gregory, J. M.},
	doi = {10.1007/s003820000059},
	file = {:home/martinez/Documents/papers/Gregory - 2000 - Vertical heat transports in the ocean and their effect on time-dependent climate change.pdf:pdf},
	issn = {14320894},
	journal = {Climate Dynamics},
	number = {7},
	pages = {501--515},
	title = {{Vertical heat transports in the ocean and their effect on time-dependent climate change}},
	volume = {16},
	year = {2000}
}


@article{Held2010,
	abstract = {The fast and slow components of global warming in a comprehensive climate model are isolated by examining the response to an instantaneous return to preindustrial forcing. The response is characterized by an initial fast exponential decay with an e-folding time smaller than 5 yr, leaving behind a remnant that evolves more slowly. The slow component is estimated to be small at present, as measured by the global mean near-surface air temperature, and, in the model examined, grows to 0.4°C by 2100 in the A1B scenario from the Special Report on Emissions Scenarios (SRES), and then to 1.4°C by 2300 if one holds radiative forcing fixed after 2100. The dominance of the fast component at present is supported by examining the response to an instantaneous doubling of CO2 and by the excellent fit to the model's ensemble mean twentieth-century evolution with a simple one-box model with no long times scales. {\textcopyright} 2010 American Meteorological Society.},
	author = {Held, Isaac M. and Winton, Michael and Takahashi, Ken and Delworth, Thomas and Zeng, Fanrong and Vallis, Geoffrey K.},
	doi = {10.1175/2009JCLI3466.1},
	file = {:home/martinez/Documents/papers/Held et al. - 2010 - Probing the fast and slow components of global warming by returning abruptly to preindustrial forcing.pdf:pdf},
	issn = {08948755},
	journal = {Journal of Climate},
	number = {9},
	pages = {2418--2427},
	title = {{Probing the fast and slow components of global warming by returning abruptly to preindustrial forcing}},
	volume = {23},
	year = {2010}
}


@article{Pattyn2006,
	abstract = {Over the last decades, the response of large ice sheets on Earth, such as the Greenland and Antarctic ice sheets, to changes in climate has been successfully simulated with large-scale numerical ice-sheet models. Since these models are highly sophisticated, they are only applicable on the scientific level as they demand a large amount of CPU time. Based on similar physics, a computationally fast flowline model of the Greenland and Antarctic ice sheet is presented here, primarily designed for educational purposes. Using an over-implicit numerical scheme, the model runs fast and behaves in a similar way to changes in background temperature forcing as major ice-sheet models do. A user-friendly interface and the implementation within a common spreadsheet program (Excel™) make the model suitable for the classroom. {\textcopyright} 2005 Elsevier Ltd. All rights reserved.},
	author = {Pattyn, Frank},
	doi = {10.1016/j.cageo.2005.06.020},
	file = {:home/martinez/Documents/papers/Pattyn - 2006 - GRANTISM An Excel™ model for Greenland and Antarctic ice-sheet response to climate changes.pdf:pdf},
	issn = {00983004},
	journal = {Computers and Geosciences},
	keywords = {Antarctica,Climate change,Greenland,Ice-sheet model,Spreadsheet},
	number = {3},
	pages = {316--325},
	title = {{GRANTISM: An Excel™ model for Greenland and Antarctic ice-sheet response to climate changes}},
	volume = {32},
	year = {2006}
}

@article{Glotter2014,
	abstract = {Integrated Assessment Models (IAMs) that couple the climate system and the economy require a representation of ocean CO2 uptake to translate human-produced emissions to atmospheric concentrations and in turn to climate change. The simple linear carbon cycle representations in most IAMs are not however physical at long timescales, since ocean carbonate chemistry makes CO2 uptake highly nonlinear. No linearized representation can capture the ocean's dual-mode behavior, with initial rapid uptake and then slow equilibration over ∽10,000 years. In a business-as-usual scenario followed by cessation of emissions, the carbon cycle in the 2007 version of the most widely used IAM, DICE (Dynamic Integrated model of Climate and the Economy), produces errors of ∽2∘C by the year 2300 and ∽6∘C by the year 3500. We suggest here a simple alternative representation that captures the relevant physics and show that it reproduces carbon uptake in several more complex models to within the inter-model spread. The scheme involves little additional complexity over the DICE model, making it a useful tool for economic and policy analyses.},
	author = {Glotter, Michael J. and Pierrehumbert, Raymond T. and Elliott, Joshua W. and Matteson, Nathan J. and Moyer, Elisabeth J.},
	doi = {10.1007/s10584-014-1224-y},
	file = {:home/martinez/Documents/papers/Glotter et al. - 2014 - A simple carbon cycle representation for economic and policy analyses.pdf:pdf},
	issn = {01650009},
	journal = {Climatic Change},
	number = {3-4},
	pages = {319--335},
	title = {{A simple carbon cycle representation for economic and policy analyses}},
	volume = {126},
	year = {2014}
}

@article{Joos2013,
	abstract = {The responses of carbon dioxide (CO2) and other climate variables to an emission pulse of CO2 into the atmosphere are often used to compute the Global Warming Potential (GWP) and Global Temperature change Potential (GTP), to characterize the response timescales of Earth System models, and to build reduced-form models. In this carbon cycle-climate model intercomparison project, which spans the full model hierarchy, we quantify responses to emission pulses of different magnitudes injected under different conditions. The CO2 response shows the known rapid decline in the first few decades followed by a millennium-scale tail. For a 100 Gt-C emission pulse added to a constant CO2 concentration of 389 ppm, 25 ± 9 {\%} is still found in the atmosphere after 1000 yr; the ocean has absorbed 59 ± 12 {\%} and the land the remainder (16 ± 14 {\%}). The response in global mean surface air temperature is an increase by 0.20 ± 0.12°C within the first twenty years; thereafter and until year 1000, temperature decreases only slightly, whereas ocean heat content and sea level continue to rise. Our best estimate for the Absolute Global Warming Potential, given by the time-integrated response in CO2 at year 100 multiplied by its radiative efficiency, is 92.5 × 10-15 yr W m-2 per kg-CO2. This value very likely (5 to 95 {\%} confidence) lies within the range of (68 to 117) × 10-15 yr W m-2 per kg-CO2. Estimates for time-integrated response in CO2 published in the IPCC First, Second, and Fourth Assessment and our multi-model best estimate all agree within 15{\%} during the first 100 yr. The integrated CO2 response, normalized by the pulse size, is lower for pre-industrial conditions, compared to present day, and lower for smaller pulses than larger pulses. In contrast, the response in temperature, sea level and ocean heat content is less sensitive to these choices. Although, choices in pulse size, background concentration, and model lead to uncertainties, the most important and subjective choice to determine AGWP of CO2 and GWP is the time horizon. {\textcopyright} Author(s) 2013.},
	author = {Joos, F. and Roth, R. and Fuglestvedt, J. S. and Peters, G. P. and Enting, I. G. and {Von Bloh}, W. and Brovkin, V. and Burke, E. J. and Eby, M. and Edwards, N. R. and Friedrich, T. and Fr{\"{o}}licher, T. L. and Halloran, P. R. and Holden, P. B. and Jones, C. and Kleinen, T. and Mackenzie, F. T. and Matsumoto, K. and Meinshausen, M. and Plattner, G. K. and Reisinger, A. and Segschneider, J. and Shaffer, G. and Steinacher, M. and Strassmann, K. and Tanaka, K. and Timmermann, A. and Weaver, A. J.},
	doi = {10.5194/acp-13-2793-2013},
	file = {:home/martinez/Documents/papers/Joos et al. - 2013 - Carbon dioxide and climate impulse response functions for the computation of greenhouse gas metrics A multi-model a.pdf:pdf},
	issn = {16807316},
	journal = {Atmospheric Chemistry and Physics},
	number = {5},
	pages = {2793--2825},
	title = {{Carbon dioxide and climate impulse response functions for the computation of greenhouse gas metrics: A multi-model analysis}},
	volume = {13},
	year = {2013}
}

@article{Helwegen2019,
	abstract = {Solar radiation management (SRM) has been proposed as a means to reduce global warming in spite of high greenhouse-gas concentrations and to lower the chance of warming-induced tipping points. However, SRM may cause economic damages and its feasibility is still uncertain. To investigate the trade-off between these (economic) gains and damages, we incorporate SRM into a stochastic dynamic integrated assessment model and perform the first rigorous cost-benefit analysis of sulfate-based SRM under uncertainty, treating warming-induced climate tipping and SRM failure as stochastic elements. We find that within our model, SRM has the potential to greatly enhance future welfare and merits being taken seriously as a policy option. However, if only SRM and no CO2 abatement is used, global warming is not stabilised and will exceed 2 K. Therefore, even if successful, SRM can not replace but only complement CO2 abatement. The optimal policy combines CO2 abatement and modest SRM and succeeds in keeping global warming below 2 K.},
	author = {Helwegen, Koen G. and Wieners, Claudia E. and Frank, Jason E. and Dijkstra, Henk A.},
	doi = {10.5194/esd-10-453-2019},
	file = {:home/martinez/Documents/papers/Helwegen et al. - 2019 - Complementing CO2 emission reduction by solar radiation management might strongly enhance future welfare.pdf:pdf},
	issn = {21904987},
	journal = {Earth System Dynamics},
	number = {3},
	pages = {453--472},
	title = {{Complementing CO2 emission reduction by solar radiation management might strongly enhance future welfare}},
	volume = {10},
	year = {2019}
}



@article{Weertman1976,
abstract = {The fluctuations in the size of ice age ice sheets are calculated using glacier mechanics and the Milankovitch solar radiation variations. The calculations are greatly simplified by considering only two-dimensional ice sheets with profiles that would be appropriate if ice obeyed the flow law of a perfectly plastic solid. The solar radiation variations seem to be large enough to account for ice ages. {\textcopyright} 1976 Nature Publishing Group.},
author = {Weertman, Johannes},
doi = {10.1038/261017a0},
file = {:home/martinez/Documents/papers/weertman1976.pdf:pdf},
issn = {00280836},
journal = {Nature},
number = {5555},
pages = {17--20},
title = {{Milankovitch solar radiation variations and ice age ice sheet sizes}},
volume = {261},
year = {1976}
}

@article{Birchfield1977,
author = {Birchfield, G E},
doi = {10.1029/JC082i031p04909},
file = {:home/martinez/Documents/papers/Birchfield - 1977 - A study of the stability of a model continental ice sheet subject to periodic variations in heat input.pdf:pdf},
issn = {01480227},
journal = {Journal of Geophysical Research},
keywords = {doi:10.1029/JC082i031p04909,http://dx.doi.org/10.1029/JC082i031p04909},
month = {oct},
number = {31},
pages = {4909--4913},
title = {{A study of the stability of a model continental ice sheet subject to periodic variations in heat input}},
url = {http://doi.wiley.com/10.1029/JC082i031p04909},
volume = {82},
year = {1977}
}

@article{Heitzig2016,
abstract = {Abstract. To keep the Earth system in a desirable region of its state space, such as defined by the recently suggested "tolerable environment and development window", "guardrails", "planetary boundaries", or "safe (and just) operating space for humanity", one needs to understand not only the quantitative internal dynamics of the system and the available options for influencing it (management) but also the structure of the system's state space with regard to certain qualitative differences. Important questions are, which state space regions can be reached from which others with or without leaving the desirable region, which regions are in a variety of senses "safe" to stay in when management options might break away, and which qualitative decision problems may occur as a consequence of this topological structure? In this article, we develop a mathematical theory of the qualitative topology of the state space of a dynamical system with management options and desirable states, as a complement to the existing literature on optimal control which is more focussed on quantitative optimization and is much applied in both the engineering and the integrated assessment literature. We suggest a certain terminology for the various resulting regions of the state space and perform a detailed formal classification of the possible states with respect to the possibility of avoiding or leaving the undesired region. Our results indicate that, before performing some form of quantitative optimization such as of indicators of human well-being for achieving certain sustainable development goals, a sustainable and resilient management of the Earth system may require decisions of a more discrete type that come in the form of several dilemmas, e.g. choosing between eventual safety and uninterrupted desirability, or between uninterrupted safety and larger flexibility. We illustrate the concepts and dilemmas drawing on conceptual models from climate science, ecology, coevolutionary Earth system modelling, economics, and classical mechanics, and discuss their potential relevance for the climate and sustainability debate, in particular suggesting several levels of planetary boundaries of qualitatively increasing safety.},
author = {Heitzig, J. and Kittel, T. and Donges, J. F. and Molkenthin, N.},
doi = {10.5194/esd-7-21-2016},
file = {:home/martinez/Documents/papers/Heitzig et al. - 2016 - Topology of sustainable management of dynamical systems with desirable states from defining planetary boundaries.pdf:pdf},
issn = {2190-4987},
journal = {Earth System Dynamics},
month = {jan},
number = {1},
pages = {21--50},
title = {{Topology of sustainable management of dynamical systems with desirable states: from defining planetary boundaries to safe operating spaces in the Earth system}},
url = {https://www.earth-syst-dynam.net/7/21/2016/},
volume = {7},
year = {2016}
}

@article{botta2018impact,
abstract = {Abstract. We apply a computational framework for specifying and solving sequential decision problems to study the impact of three kinds of uncertainties on optimal emission policies in a stylized sequential emission problem.We find that uncertainties about the implementability of decisions on emission reductions (or increases) have a greater impact on optimal policies than uncertainties about the availability of effective emission reduction technologies and uncertainties about the implications of trespassing critical cumulated emission thresholds. The results show that uncertainties about the implementability of decisions on emission reductions (or increases) call for more precautionary policies. In other words, delaying emission reductions to the point in time when effective technologies will become available is suboptimal when these uncertainties are accounted for rigorously. By contrast, uncertainties about the implications of exceeding critical cumulated emission thresholds tend to make early emission reductions less rewarding.},
author = {Botta, Nicola and Jansson, Patrik and Ionescu, Cezar},
doi = {10.5194/esd-9-525-2018},
file = {:home/martinez/Documents/papers/Botta, Jansson, Ionescu - 2018 - The impact of uncertainty on optimal emission policies.pdf:pdf},
issn = {2190-4987},
journal = {Earth System Dynamics},
month = {may},
number = {2},
pages = {525--542},
title = {{The impact of uncertainty on optimal emission policies}},
url = {https://www.earth-syst-dynam.net/9/525/2018/},
volume = {9},
year = {2018}
}
