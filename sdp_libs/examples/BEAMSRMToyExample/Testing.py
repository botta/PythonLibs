
# Imports
from sdp_libs.examples.BEAMSRMToyExample.SDP import SDP_BEAM_SRM

#----------------------------------------------------------------------------------------------------------------------#
# Testing ...

for i in range(3):
    for j in range(4):

        print((i, j), ":")
        SDP_BEAM_SRM(i, j).compute_SDP()

