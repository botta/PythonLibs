# TODO list:
#  - Add types where they are still missing
#  - Add missing documentation
#  - Remove boilerplate and move non-problem-specific functionality to parent class
#  - Decide how to treat `horizon`
#  - Move dt values to global context if they remain constant during decision process
#  - Fix/extend plotting
#  - Clean up unused parameters
#  - Should the arguments for fix_df_steady be additional arguments for __init__?
#  - Save results with pickle?
#  - Include proper logging

# Imports

# general
import matplotlib.pyplot as plt
import warnings
import time

## project
from sdp_libs.examples.BEAMSRMICEControlsExample.Control import *  # Ctrl
from sdp_libs.examples.BEAMSRMICEControlsExample.State import *  # State
from sdp_libs.sequential_decision_problems.default_simple_prob_parent import SP_SDP

counter = 0

class SDP_BEAM_SRM(SP_SDP):
    """Class for computing an SDP with parameter uncertainty on top of the BEAM+SRM carbon and temperature model.

    Attributes:
    -----------
    Attributes for sequential decision problem

    horizon : int
        terminal time for decision process
    State : AbstractStateClass
        class describing the state space
    Ctrl : AbstractControlClass
        class describing the control space
    all_ctrls : List[Ctrl] (possibly adjust)
        list of all possible controls


    Attributes of Glotter et al. BEAM model and Helwegen et al. SRM model

    * BEAM carbon following Glotter et al. (2015):

        ka : float
            time constant
            default value: 0.2 years^{-1}
            from Bolin and Eriksson (1959)
        kd : float
            time constant
            default value: 0.05 years^{-1}
            from Bolin and Eriksson (1959)
        delta : int
            default value: 50 (dimensionless)
            from Bolin and Eriksson (1959)
        kH : float
            default value: 1.23E3 (dimensionless)
            from Weiss (1974)
        k1 : float
            default value: 8.00E-7 mol/Kg
            from Mehrbach et al (1973)
        k2 : float
            default value: 4.53E-10 mol/Kg
            from Mehrbach et al (1973)
        AM : float
            default value: 1.77E20 mol
            from Warneck (1999)
        OM : float
            default value: 7.8E22 mol
            from Garrison (2009), conversion:
                OM
                ~~ 1.37 · 10^9 km^3 ocean · 10^15 cm^3 / km^3 · 1.027g seawater / cm^3 seawater · 1 mol water / 18 g
                ~~ 7.8 · 10^22 moles
        Alk : float
            default value: 767.0 Gt C
            from Glotter et al. (2015)
        GtCtoppm : float
            default value: 0.47 ppm in atmosphere / Gt C in atmosphere

    * Starting conditions year 2000

        Mat2000 : float
            default value: 809 Gt C
        Mup2000 : float
            default value: 725 Gt C
        Mlo2000 : float
            default value: 35642 Gt C


    * Temperature:

        * BEAM temperature:

            alpha: float
                DICE assumptions of the forcing per doubling of CO_2
                default value: 3.8 Watts / m^2
            gamma: float
                relates atmosphere-ocean heat transfer to temperature anomaly
                default value: 0.3 Watts / (m^2 * Celsius)
            Lambda: float
                climate sensitivity
                default value: 1.3 Watts / (m^2 * Celsius)
                --- if we want to incorporate uncertainty its value,
                this will contained in the unobservable part of the state ---
            muat: float
                default value: 0.022 years^{-1}
            mulo: float
                default value: 1 / 60 years^{-1}
            MatPreInd: float
                default value: 596 Gt C

        * Aerosol following Helwegen et al.:

            eta: float
                default value: 0.742 (dimensionless)
            alphaSO2: float
                default value: 65 Watts / m^2
            betaSO2: float3
                default value: 2246 Mt of S / year
            gammaSO2: float
                default value: 0.23 (dimensionless)

        * Starting conditions year 2000

            Tat2000 : float
                default value: 0.633
                Temperature anomaly in year 2000 with respect to pre-indstrial (1750)
            Tlo2000 : float
                default value: 0.084
                Temperature anomaly in year 2000 with respect to pre-indstrial (1750)


    """

    # ---------------- #
    # Setup parameters #
    # ---------------- #

    # fixed parameters
    # ---------------- #

    ka: float = 0.2  # 1/years
    kd: float = 0.05  # 1/years
    delta: int = 50  # dimensionless
    kH: float = 1.23E3  # dimensionless
    k1: float = 8.00E-7  # mol/Kg
    k2: float = 4.53E-10  # mol/Kg
    AM: float = 1.77E20  # mol
    OM: float = 7.8E22  # mol
    Alk: float = 767.0  # Gt C
    GtCtoppm: float = 0.47  # ppm in atmosphere/ Gt C in atmosphere

    ## Starting conditions year 2000

    Mat2000: float = 809  # Gt C
    Mup2000: float = 725  # Gt C
    Mlo2000: float = 35642  # Gt C

    # parameters entering temperature computation
    # ------------------------------------------- #

    ## carbon parameters

    alpha: float = 3.8  # Watts / m^2
    gamma: float = 0.3  # Watts / (m^2 * Celsius)
    # Lambda: float    = 1.3              # Watts / (m^2 * Celsius)
    muat: float = 0.022  # 1/years
    mulo: float = 1 / 60  # 1/years
    MatPreInd: float = 596  # Gt C

    ## aerosol parameters (taken from Helwegen2019)

    eta: float = 0.742  # dimensionless
    alphaSO2: float = 65  # Watts / m^2
    betaSO2: float = 2246  # Mt of S / year
    gammaSO2: float = 0.23  # dimensionless

    ## Starting conditions year 2000

    Tat2000: float = 0.633  # Celsius
    Tlo2000: float = 0.084  # Celsius

    # ICE dynamics parameters
    # ----------------------- #
    # (Remark: these two parameters might need to be tuned if we modify too much the steady states)

    tau_melt = 320  # years
    tau_freeze = 1500  # years

    # Parameter Uncertainty
    # --------------------- #
    # A dummy probability distribution over dummy values for climate sensitivity
    prob_cs = [(1.3, 0.1), (2.5, 0.3), (4.0, 0.5), (6.5, 0.1)]
    prob_no_action = 0.6  # pessimistic?
    prob_action = 1.0 - prob_no_action


    # Scenarios
    # --------- #

    ScenarioT = int

    zero_emissions: ScenarioT = 0
    constant_emissions_injections: ScenarioT = 1
    linear_decarbon_50: ScenarioT = 2
    linear_decarbon_200: ScenarioT = 3
    linear_decarbon_50_sequest_low: ScenarioT = 4
    linear_decarbon_200_sequest_low: ScenarioT = 5
    linear_decarbon_50_sequest_med: ScenarioT = 6
    linear_decarbon_200_sequest_med: ScenarioT = 7
    linear_decarbon_50_sequest_high: ScenarioT = 8
    linear_decarbon_200_sequest_high: ScenarioT = 9
    linear_decarbon_50_inj_med_200: ScenarioT = 10
    linear_decarbon_200_inj_med_200: ScenarioT = 11
    linear_decarbon_50_inj_high_200: ScenarioT = 12
    linear_decarbon_200_inj_high_200: ScenarioT = 13
    sequest_low: ScenarioT = 14
    sequest_medium: ScenarioT = 15
    sequest_high: ScenarioT = 16
    zero_emissions_inj_med: ScenarioT = 17
    zero_emissions_inj_high: ScenarioT = 18

    all_scenarios: List[ScenarioT] = [zero_emissions,
                                      constant_emissions_injections,
                                      linear_decarbon_50,
                                      linear_decarbon_200,
                                      linear_decarbon_50_sequest_low,
                                      linear_decarbon_200_sequest_low,
                                      linear_decarbon_50_sequest_med,
                                      linear_decarbon_200_sequest_med,
                                      linear_decarbon_50_sequest_high,
                                      linear_decarbon_200_sequest_high,
                                      linear_decarbon_50_inj_med_200,
                                      linear_decarbon_200_inj_med_200,
                                      linear_decarbon_50_inj_high_200,
                                      linear_decarbon_200_inj_high_200,
                                      sequest_low,
                                      sequest_medium,
                                      sequest_high,
                                      zero_emissions_inj_med,
                                      zero_emissions_inj_high,
                                     ]

    scenario_names = scenarios = ['ZeroEmissions',
             'ConstantEmissionsInj',
             'LinDec50',
             'LinDec200',
             'LinDec50+SeqLow',
             'LinDec200+SeqLow',
             'LinDec50+SeqMed',
             'LinDec200+SeqMed',
             'LinDec50+SeqHigh',
             'LinDec200+SeqHigh',
             'LinDec50+InjMed200',
             'LinDec200+InjMed200',
             'LinDec50+InjHigh200',
             'LinDec200+InjHigh200',
             'SeqLow',
             'SeqMed',
             'SeqHigh',
             'ZeroEmissions+InjMed',
             'ZeroEmissions+InjHigh']

    scenario_rate_functions = []

    # ----------------------------------------------- #
    # #### Initialize the class                       #
    # ----------------------------------------------- #

    ctrl_aux = Ctrl()
    ControlT = ctrl_aux.get_control_type()
    # StateT = State.getType()
    State = State
    Ctrl = Ctrl
    all_ctrls: List[ControlT] = ctrl_aux.list_of_ctrls

    def __init__(self):
        super().__init__(self.State, self.Ctrl, self.all_ctrls)
        self.eq_rhs_ice = self.fix_df_steady(3.5, 0.3, 0.72, 0.2)
        self.scenario_rate_functions = [partial(self.init_scenarios(), s) for s in self.all_scenarios]
        self.scenario_rate_functions_aux = [partial(self.init_scenarios(), s) for s in self.all_scenarios]

    # ---------------------------------------------------------------------------------------------------------------- #
    # ---------------------------------------------------------------------------------------------------------------- #

    # ---------------------------------------------------------- #
    # Transition functions
    # ---------------------------------------------------------- #

    @cached
    def nexts(self, t: int, x: State, y: Ctrl) -> 'SimpleProb(State)':
        """An experimental transition function using the carbon cycle step function
            combined with the temperature and ice volume step function;
            all uncertainty comes from parameter uncertainty about climate sensitivity which is treated in
            the first transition step. All subsequent transitions are deterministic.

            Args
            ----

            t: int
                the current decision step
            x: State
                the current state
            y: Ctrl
                the current control

            Result
            ------

            xps: SimpleProb(State)
                a probability distribution of successor states
            """

        if t == 0:

            xps = [(self.make_xp(x, cs), pr) for (cs, pr) in self.prob_cs]

        elif t > 0:

            current_co2_emission_rate = x.get_co2_emission_rate()
            current_so2_injection_rate = x.get_so2_injection_rate()

            # Controls:
            # ----------

            co2_action, so2_action = self.get_control_action(y, current_co2_emission_rate)

            # Consider the case that decisions are not implemented
            co2_no_action = current_co2_emission_rate
            so2_no_action = current_so2_injection_rate

            xp_action = self.next_coupled_models(x, co2_action, so2_action)
            xp_no_action = self. next_coupled_models(x, co2_no_action, so2_no_action)

            xps = [(xp_action, self.prob_action), (xp_no_action, self.prob_no_action)]

        else:
            raise ValueError("Invalid time.")

        return xps

    @cached
    def next_coupled_models(self, x: State, co2_action: CO2ActT, so2_action: SO2ActT) -> State:
        """Deterministic transition function that evolves the coupled models."""

        tx = x.get_time()

        dt = x.get_dt()
        dt1 = x.get_dt1()
        dt2 = x.get_dt2()

        steps_carbon = int(dt / dt1)
        ratio_temp = int(dt2 / dt1)
        ratio_ice = int(dt / dt1)

        # Carbon:
        # -------

        Mat = x.get_mat()
        Mup = x.get_mup()
        Mlo = x.get_mlo()

        tc = tx

        # Temperature:
        # ------------

        Tat = x.get_tat()
        Tlo = x.get_tlo()
        Lambda = x.get_climate_sensitivity()

        # ice:
        # -----

        volume = x.get_ice_volume()

        i = 0
        while i < steps_carbon:
            tc, Mat, Mup, Mlo = self.next_carbon(tc, dt1, Mat, Mup, Mlo, co2_action)
            i += 1

            if i % ratio_temp == 0:
                Tat, Tlo = self.next_temp(tc, dt2, Tat, Tlo, Lambda, Mat, so2_action)

            if i % ratio_ice == 0:
                Tforcing_ice = 1.33 * Tat
                volume = self.next_ice(dt, volume, Tforcing_ice)

        # Prepare next state:
        # -------------------

        xp = x.__copy__()
        xp.set_time(tx + dt)
        xp.set_co2_emission_rate(co2_action)
        xp.set_so2_injection_rate(so2_action)
        xp.set_mat(Mat)
        xp.set_mup(Mup)
        xp.set_mlo(Mlo)
        xp.set_tat(Tat)
        xp.set_tlo(Tlo)
        xp.set_ice_volume(volume)
        # print(xp)

        return xp

    @cached
    def next_carbon(self, t: float, dt: CarbonDtT, Mat: MatT, Mup: MupT, Mlo: MloT, co2_emission_rate: CO2ActT) \
            -> Tuple[TimeT, MatT, MupT, MloT]:
        """Euler integrator for carbon cycle eqs - transition function for the carbon cycle subsystem."""

        flowATtoUP = self.ka * (Mat -
                                (self.AM * self.kH * (1 + self.delta)
                                 * (- self.Alk * self.k1 + 4 * self.Alk * self.k2 + self.k1 * Mup - 8 * self.k2 * Mup
                                    + np.sqrt(
                                                    self.k1 * (self.Alk ** 2 * (self.k1 - 4 * self.k2)
                                                               - 2 * self.Alk * (
                                                                       self.k1 - 4 * self.k2) * Mup + self.k1 * Mup ** 2))
                                    )
                                 )
                                / (2 * (self.k1 - 4 * self.k2) * self.OM))

        flowUPtoLO = self.kd * (-(Mlo / self.delta) + Mup)

        dMat = co2_emission_rate - flowATtoUP
        dMup = - flowUPtoLO + flowATtoUP
        dMlo = flowUPtoLO

        return t + dt, Mat + dMat * dt, Mup + dMup * dt, Mlo + dMlo * dt

    @cached
    def next_temp(self,
                  t: float, dt: TempDtT,
                  Tat: TatT, Tlo: TloT,
                  Lambda: ClimateSensitivityT,
                  Mat: MatT,
                  so2_injection_rate: SO2ActT) -> Tuple[TatT, TloT]:
        """Transition function for the temperature subsystem."""

        flowATtoLO = self.gamma * (Tat - Tlo)
        dTat = self.muat * (Lambda * (self.equilibrium_temperature(t, Lambda, Mat, so2_injection_rate) - Tat)
                            - flowATtoLO)
        dTlo = self.mulo * flowATtoLO

        return Tat + dTat * dt, Tlo + dTlo * dt

    @cached
    def next_ice(self, dt: DtTimeT, volume: IceVolumeT, forcing_temperature: TatT) -> IceVolumeT:
        """Transition function for the icesheet subsystem."""

        if dt > 50:
            warnings.warn("WARNING: Careful, numerical instability might appear in ice sheet evolution. "
                          "To fix it decrease ::dt:: to smaller than 50.")

        eq_rhs_vol_ft = self.eq_rhs_ice(volume, forcing_temperature)  # what would be a better name for the variable?
        if eq_rhs_vol_ft > 0:
            next_volume = volume + dt * (1 / self.tau_freeze) * eq_rhs_vol_ft
        else:
            if volume <= 1e-4:
                next_volume = volume
            else:
                next_volume = max(0, volume + dt * (1 / self.tau_melt) * eq_rhs_vol_ft)
        return next_volume

    # ---------------------------------------------------------------------------------------------------------------- #
    # ---------------------------------------------------------------------------------------------------------------- #

    # ---------------------- #
    # Reward function        #
    # ---------------------- #

    @cached
    def reward(self, t: int, x: State, y: Ctrl, xp: State):
        """A tentative reward function.

        Remark (Marina): There is a sign, in the temperature damage that we have to be careful with.
        (Tat/TatPI - 1) can be either positive or negative (due to the SRM options).
        In the damage function what appears is  (Tat/TatPI - 1)^2 , which is always positive.
        It might be ok that the lowest damage is for Tat = TatPI and that higher or lower than
        PI temp induces the same damage. I am really not sure on whether we should perhaps consider
        that colder Tat than PI is better and change the minus sign (outside of the term  (Tat/TatPI - 1)^2 )
        when that happens.
        """

        if (t == 0):

            rew = 0

        else:

            co2_emission_rate = xp.get_co2_emission_rate()
            so2_injection_rate = xp.get_so2_injection_rate()
            Mat = xp.get_mat()
            Tat = xp.get_tat()
            MatPreInd = self.MatPreInd

            co2_emission_rate_max = 50
            co2_ratio = co2_emission_rate / co2_emission_rate_max
            so2_injection_rate_max = 80
            so2_ratio = so2_injection_rate / so2_injection_rate_max
            tat_max = 10.0
            tat_ratio = Tat / tat_max
            commitment_penalty = 0

            # Plan: weight_aerosol_damage and weight_CO2_damage have to be lower than weight_temp_damage
            # and weight_aerosol_injection has to be smaller than weight_co2_emission

            # placeholder values for the moment
            # ...and a case split on the value of climate sensitivity to obtain an optimal
            # policy sequence with non-constant policies.
            if x.climate_sensitivity > 3:
                weight_co2_emission = 1
                weight_so2_injection = 1
                weight_temp_damage = 1  # 3
                weight_co2_damage = 1  # 2
                weight_so2_damage = 1
                weight_commitment = 1

            else:
                weight_co2_emission = 1  # 6
                weight_so2_injection = 1
                weight_temp_damage = 1  # 3
                weight_co2_damage = 1  # 2
                weight_so2_damage = 1
                weight_commitment = 1

            """
            if self.is_terminal(xp):
                # global counter
                # print(counter, "terminal")
                # counter += 1
                if self.test_commitment(xp):
                    print("committed!")
                    commitment_penalty = 1000  # large dummy value
            """

            economy = + co2_ratio * weight_co2_emission

            so2_injection_costs = - so2_ratio * weight_so2_injection

            temperature_damage = - tat_ratio ** 2 * weight_temp_damage

            co2_concentration_damage = - (Mat / MatPreInd - 1) ** 2 * weight_co2_damage

            so2_injection_damage = - so2_ratio ** 2 * weight_so2_damage

            commitment_damage = - commitment_penalty * weight_commitment


            rew = economy \
                  + so2_injection_costs \
                  + temperature_damage \
                  + co2_concentration_damage \
                  + so2_injection_damage \
                  + commitment_damage

        return rew

    # ---------------------------------------------------------------------------------------------------------------- #
    # ---------------------------------------------------------------------------------------------------------------- #

    # ---------------------- #
    # Auxiliary functions    #
    # ---------------------- #

    @staticmethod
    def make_xp(x: State, cs: ClimateSensitivityT) -> State:
        xp = x.__copy__()
        xp.set_climate_sensitivity(cs)
        return xp

    @cached
    def is_terminal(self, x: State) -> bool:
        """Check whether x is a terminal state."""
        logging.debug("t:" + str(x.get_time()) + ", h:" + str(self.horizon))

        t = x.get_time()
        h = self.horizon
        return t == h


    @cached
    def get_control_action(self, y: Ctrl, current_co2: CO2ActT) -> Tuple[CO2ActT, SO2ActT]:
        """Get concrete CO2 and SO2 action from abstract control values."""

        control = y.getCtrl()
        co2_control = control[0]
        so2_control = control[1]

        if co2_control == co2_down:
            co2_action = current_co2 + self.co2_decrease(current_co2)
        elif co2_control == co2_maintain:
            co2_action = current_co2
        elif co2_control == co2_up:
            co2_action = current_co2 + self.co2_increase(current_co2)
        else:
            raise ValueError("Problem with CO2 action.")

        if so2_control == so2_none:
            so2_action = 0
        elif so2_control == so2_low:
            so2_action = 40
        elif so2_control == so2_high:
            so2_action = 80
        else:
            raise ValueError("Problem with SO2 action.")

        return co2_action, so2_action


    @staticmethod
    def co2_increase(current_co2_emission_rate: int) -> int:
        r = current_co2_emission_rate + 2

        if r < 50:
            return 2
        elif current_co2_emission_rate == 50:
            return 0
        else:
            return 50 - current_co2_emission_rate

    @staticmethod
    @cached
    def co2_decrease(current_co2_emission_rate: int) -> int:

        if (current_co2_emission_rate - 2) > 0:
            return - 2
        elif current_co2_emission_rate == 0:
            return 0
        else:
            return - current_co2_emission_rate

    # Added parameter :Lambda: for climate sensitivity as part of state
    @cached
    def equilibrium_temperature(self, t: float, Lambda: float, Mat, so2_injections: int):
        """Computes equilibrium temperature ::Teq:: due to carbon and aerosol forcing"""

        if so2_injections == 0:  # no SRM case
            return (1 / Lambda) * (self.alpha * np.log2(Mat / self.MatPreInd))

        else:  # all other cases
            return (1 / Lambda) * (self.alpha * np.log2(Mat / self.MatPreInd) -
                                   self.eta * self.alphaSO2
                                   * np.exp(-np.power(self.betaSO2 / so2_injections, self.gammaSO2)))

    # ---------------------------------------------------------------------------------------------------------------- #
    # ---------------------------------------------------------------------------------------------------------------- #

    # ----------- #
    # Preparation #
    # ----------- #

    # ----------------------------------------------------------------------- #
    # Function to fix tipping points on double fold greenland ice sheet model #
    # ------------------------------------------------------------------------#
    def fix_df_steady(self, Tp: float, Tm: float, Vp: float, Vm: float) -> Callable[[float, float], float]:
        if Tp < Tm or Vp < Vm:
            raise ValueError('PHYSICAL ERROR: check that Tp > Tm and Vp > Vm\n');
        # set coeffs
        a = 3 * (Vp + Vm) / 2
        b = - 3 * Vp * Vm

        def c(T):
            return (Tp * Vm ** 2 * (3 * Vp - Vm) - Tm * Vp ** 2 * (3 * Vm - Vp) + T * (Vm - Vp) ** 3) / (2 * (Tp - Tm))

        def eq_rhs_ice(V, T):
            return - V ** 3 + a * V ** 2 + b * V + c(T)

        """
        # computing steady states
        Tflist = np.linspace(0, 1.2 * Tp, 200)
        steadystates = []
        root1 = []
        root2 = []
        root3 = []
        if abs(np.roots([-1, a, b, c(0)])[0] - 1) > 0.05:
            print('ALARM: Check parameter values, the value of roots at Tf = 0 are ', np.roots([-1, a, b, c(0)]))
        else:
            print('Value of roots at Tf = 0 are ', np.roots([-1, a, b, c(0)]))
        for Tf in Tflist:
            allroots = np.roots([-1, a, b, c(Tf)])
            steadystates.append(allroots)
            if np.imag(allroots[0]) == 0:
                root1.append([Tf, allroots[0]])
            if np.imag(allroots[1]) == 0:
                root2.append([Tf, allroots[1]])
            if np.imag(allroots[2]) == 0:
                root3.append([Tf, max(0, allroots[2])])
        steadystates = np.array(steadystates)
        root1 = np.array(root1)
        root2 = np.array(root2)
        root3 = np.array(root3)
        # plotting steady states
        plt.plot(root1[:, 0], root1[:, 1])
        plt.plot(root2[:, 0], root2[:, 1])
        plt.plot(root3[:, 0], root3[:, 1])
        plt.plot(Tp, Vp, 'o')
        plt.plot(Tm, Vm, 'o')
        plt.title('Steady states')
        plt.xlabel('Local temperature anomaly (Celcius)')
        plt.ylabel('Volume fraction')
        plt.grid()
        #plt.show()
        plt.savefig("../BEAMSRMICEControlsExample/img/icesheet_steady_states.png")
        plt.close()
        """
        return eq_rhs_ice

    # TODO: - Further scenarios
    def init_scenarios(self) -> Callable[[int, State, Number], Tuple[Number, Number]]:
        """Set up function that return CO2 emission and SO2 injection rates at time t
        depending on the scenario.
        The returned function will be partially applied to all scenarios at
        initialization of the class to obtain an individual rates function for all
        each scenario."""

        # -----------------------------------------------------------
        seq_low_rate = 0.5  # GtC/yr
        seq_med_rate = 2  # GtC/yr
        seq_high_rate = 4  # GtC/yr
        inj_med = 40  # MtS/yr
        inj_high = 80  # MtS/yr
        # -----------------------------------------------------------

        ScenarioT = self.ScenarioT

        def get_rates_at_time(scenario: ScenarioT, x: State, t: Number) -> Tuple[Number, Number]:

            t0 = x.get_time()
            current_co2_emission_rate = x.get_co2_emission_rate()
            current_so2_injection_rate = x.get_so2_injection_rate()

            if scenario == self.zero_emissions:
                emissions = 0
                injections = 0
            # -----------------------------------------------------------
            elif scenario == self.constant_emissions_injections:
                emissions = current_co2_emission_rate
                injections = current_so2_injection_rate
            # -----------------------------------------------------------
            elif scenario == self.linear_decarbon_50:
                if t > (t0 + 50):
                    emissions = 0
                elif t <= (t0 + 50):
                    emissions = -(t - t0) * \
                                current_co2_emission_rate / 50 + current_co2_emission_rate
                injections = 0
            # -----------------------------------------------------------
            elif scenario == self.linear_decarbon_200:
                if t > (t0 + 200):
                    emissions = 0
                elif t <= (t0 + 200):
                    emissions = -(t - t0) * \
                                current_co2_emission_rate / 200 + current_co2_emission_rate
                injections = 0
            # -----------------------------------------------------------
            elif scenario == self.linear_decarbon_50_sequest_low:
                if t > (t0 + 50):
                    emissions = -seq_low_rate
                elif t <= (t0 + 50):
                    emissions = -(t - t0)*(seq_low_rate + current_co2_emission_rate)/50 + current_co2_emission_rate

                injections = 0
            # -----------------------------------------------------------
            elif scenario == self.linear_decarbon_200_sequest_low:
                if t > (t0 + 200):
                    emissions = -seq_low_rate
                elif t <= (t0 + 200):
                    emissions = -(t - t0)*(seq_low_rate + current_co2_emission_rate)/200 + current_co2_emission_rate

                injections = 0
            # -----------------------------------------------------------
            elif scenario == self.linear_decarbon_50_sequest_med:
                if t > (t0 + 50):
                    emissions = -seq_med_rate
                elif t <= (t0 + 50):
                    emissions = -(t - t0)*(seq_med_rate + current_co2_emission_rate)/50 + current_co2_emission_rate

                injections = 0
            # -----------------------------------------------------------
            elif scenario == self.linear_decarbon_200_sequest_med:
                if t > (t0 + 200):
                    emissions = -seq_med_rate
                elif t <= (t0 + 200):
                    emissions = -(t - t0)*(seq_med_rate + current_co2_emission_rate)/200 + current_co2_emission_rate

                injections = 0
            # -----------------------------------------------------------
            elif scenario == self.linear_decarbon_50_sequest_high:
                if t > (t0 + 50):
                    emissions = -seq_high_rate
                elif t <= (t0 + 50):
                    emissions = -(t - t0)*(seq_high_rate + current_co2_emission_rate)/50 + current_co2_emission_rate

                injections = 0
            # -----------------------------------------------------------
            elif scenario == self.linear_decarbon_200_sequest_high:
                if t > (t0 + 200):
                    emissions = -seq_high_rate
                elif t <= (t0 + 200):
                    emissions = -(t - t0)*(seq_high_rate + current_co2_emission_rate)/200 + current_co2_emission_rate

                injections = 0
            # -----------------------------------------------------------
            elif scenario == self.linear_decarbon_50_inj_med_200:
                if t > (t0 + 50):
                    emissions = 0
                elif t <= (t0 + 50):
                    emissions = -(t - t0)*current_co2_emission_rate/50 + current_co2_emission_rate

                if t > (t0 + 200):
                    injections = 0
                elif t <= (t0 + 200):
                    injections = inj_med
            # -----------------------------------------------------------
            elif scenario == self.linear_decarbon_200_inj_med_200:
                if t > (t0 + 200):
                    emissions = 0
                    injections = 0
                if t <= (t0 + 200):
                    emissions = -(t - t0)*current_co2_emission_rate/200 + current_co2_emission_rate
                    injections = inj_med
            # -----------------------------------------------------------
            elif scenario == self.linear_decarbon_50_inj_high_200:
                if t > (t0 + 50):
                    emissions = 0
                elif t <= (t0 + 50):
                    emissions = -(t - t0)*current_co2_emission_rate/50 + current_co2_emission_rate

                if t > (t0 + 200):
                    injections = 0
                elif t <= (t0 + 200):
                    injections = inj_high
            # -----------------------------------------------------------
            elif scenario == self.linear_decarbon_200_inj_high_200:
                if t > (t0 + 200):
                    emissions = 0
                    injections = 0
                elif t <= (t0 + 200):
                    emissions = -(t - t0) * \
                                current_co2_emission_rate / 200 + current_co2_emission_rate
                    injections = inj_high
            # -----------------------------------------------------------
            elif scenario == self.sequest_low:
                emissions = -seq_low_rate

                injections = 0
            # -----------------------------------------------------------
            elif scenario == self.sequest_medium:
                emissions = -seq_med_rate

                injections = 0
            # -----------------------------------------------------------
            elif scenario == self.sequest_high:
                emissions = -seq_high_rate

                injections = 0
            # -----------------------------------------------------------
            elif scenario == self.zero_emissions_inj_med:
                emissions = 0

                injections = inj_med
            # -----------------------------------------------------------
            elif scenario == self.zero_emissions_inj_high:
                emissions = 0

                injections = inj_high
            # -----------------------------------------------------------
            else:
                raise ValueError("Invalid scenario!")

            return emissions, injections

        return get_rates_at_time


    # ---------------------------------------------------------------------------------------------------------------- #
    # ---------------------------------------------------------------------------------------------------------------- #

    # ---------------- #
    # Test function(s) #
    # ---------------- #

    def test_commitment(self, x: State) -> bool:
        """Placeholder for a potential commitment test."""
        dt_test = 4000  # years
        bad_ice_volume = 0.8  # corresponds to 1.4m of sea level rise. 0 ice vol corresponds roughly to 7m.
        scenario_results = 2 * np.ones(len(self.all_scenarios))  # 2s should be changed to 0s if good and to 1s if bad
        rate_functions = [partial(self.scenario_rate_functions[i], x) for i in self.all_scenarios]
        sequest_stopped = False

        print("terminal state:", x)

        tx = x.get_time()
        dt1 = x.get_dt1()
        dt2 = x.get_dt2()

        horizon = tx + dt_test - dt1
        # print("Horizon:", horizon)

        for scenario in self.all_scenarios:
            # Carbon:
            # -------

            Mat = x.get_mat()
            Mup = x.get_mup()
            Mlo = x.get_mlo()

            # Temperature:
            # ------------

            Tat = x.get_tat()
            Tlo = x.get_tlo()
            Lambda = x.get_climate_sensitivity()

            # Ice:
            # -----

            volume = x.get_ice_volume()

            # Counters:
            # ---------
            t = tx
            i = 0

            # Traces:
            # -------
            t_list = [tx]
            volume_list = [volume]
            temp_list = [Tat]
            co2_list = [rate_functions[scenario](tx)[0]]
            so2_list = [rate_functions[scenario](tx)[1]]

            # BEAM step loop (step size 0.05 yr)
            while t < horizon:

                # stops carbon seq if atmosphere C02 reaches pre industrial levels.
                if (Mat <= self.MatPreInd) | sequest_stopped:
                    co2_emission_rate = 0
                    sequest_stopped = True
                else:
                    co2_emission_rate = rate_functions[scenario](t)[0]
                t, Mat, Mup, Mlo = self.next_carbon(t, dt1, Mat, Mup, Mlo, co2_emission_rate)
                i += 1

                # temperature step (step size 1 yr)
                if i % 20 == 0:
                    so2_injection_rate = rate_functions[scenario](t)[1]
                    Tat, Tlo = self.next_temp(t, dt2, Tat, Tlo, Lambda, Mat, so2_injection_rate)

                # ice step (step size 10 yrs)
                if i % 200 == 0:
                    Tforcing_ice = 1.33 * Tat
                    volume = self.next_ice(10, volume, Tforcing_ice)

                    # Traces
                    t_list.append(t)
                    volume_list.append(volume)
                    temp_list.append(Tat)
                    co2_list.append(co2_emission_rate)
                    so2_list.append(so2_injection_rate)

                    # check if vol is smaller than badIceVolume at any time in the evolution and stop evolution
                    if volume <= bad_ice_volume:
                        print("Breaking at t=", t)
                        break
            if volume <= bad_ice_volume:
                scenario_results[scenario] = 1
            else:
                scenario_results[scenario] = 0
            print("Scenario:", self.scenario_names[scenario])
            print("Tat:", Tat, "Tlo:", Tlo)
            print("Ice volume:", volume)

            self.plot_scenario(scenario, t_list, co2_list, so2_list, temp_list, volume_list)

            sequest_stopped = False

        # placeholder
        initial_commitment = 1
        terminal_commitment = sum(scenario_results)
        print("Commitment result:", terminal_commitment)
        print("Individual scenario results:", scenario_results)

        if terminal_commitment > initial_commitment:
            return True
        else:
            return False

    def test_ews(self, x: State) -> bool:
        """Placeholder for a possible test concerning the presence of *early warning signals*."""

        return False

    # ---------------------------------------------------------------------------------------------------------------- #
    # ---------------------------------------------------------------------------------------------------------------- #

    # ---------------------- #
    # Computation of the SDP #
    # ---------------------- #

    def compute_SDP(self,
                    x0: State = State((2000, 10),
                                      (8, 0),
                                      (0.05, 809, 725, 35642, np.array(2000), np.array(809)),
                                      (1, 0.633, 0.084, np.array(2000), np.array(0.633)), 1, 1,
                                      ),
                    steps: int = 4,
                    compute_trjs: bool = False,
                    print_trjs : bool = False,
                    test_commit: bool = False):
        """Wrapper for default computations."""

        self.horizon = x0.get_time() + x0.get_dt() * (steps - 1)

        print("\n---------------------------------------------------------\n")
        print("Setting:")
        print("--------")
        print("Computing optimal policy sequence for", steps, "steps")
        print("with initial state ", x0)
        print("Time frame:", x0.get_time(), "-", self.horizon)
        print("in which decisions are taken every", x0.get_dt(), "years.")
        print("\n---------------------------------------------------------\n")
        print("Results:")
        print("--------")

        bi = self.timed_backwards_induction_iter(0, steps)
        value = self.timed_val_iter(0, x0, bi)
        print("Expected value for the computed optimal policy sequence, starting in x0: ", value)

        if compute_trjs:
            traj = self.trj(0, bi, x0)
            rewards = self._m_map(lambda scs: self.sum_of_rewards(0, scs))(traj)

            if(print_trjs):
                print("Possible trajectories:\n\t", traj, "\n")
                print("Rewards for these trajectories:\n\t", rewards)

            print("\n---------------------------------------------------------\n")

            if test_commit:
                xfs = self.get_final_states(traj)

                print("\nRunning commitment tests for final states...\n")
                for xf in xfs:
                    self.test_commitment(xf)

                print("\n---------------------------------------------------------\n")

            print("Log files can be found in the `log` subfolder.")
            print("Plots can be found in the `img` subfolder.")


            # ------------ #
            # plot results #
            # ------------ #

            ts = time.gmtime()
            timestamp = time.strftime("%Y-%m-%d_%H:%M:%S", ts)
            self.plot_trajectories(traj, timestamp + "_bi_" + str(steps))
            self.log_results(value, traj, timestamp + "_bi_" + str(steps))

    def run_ps(self, x0: State, ps: 'PolicySeq', print_trjs: bool = False, test_commit: bool = True) -> None:
        """Running policy sequence ps from state x.
        If :print_trjs: is :True:, the resulting trajectories are printed.
        If :test_commit: is :True:, the commitment test is applied to the resulting final states."""

        print("\n---------------------------------------------------------\n")
        print("Running policy sequence")
        pp = PrettyPrinter(compact=True)
        # pp.pprint(ps)

        # setup
        steps = len(ps)
        self.horizon = x0.get_time() + x0.get_dt() * (steps - 1)

        exp_value = self.timed_val_iter(0, x0, ps)
        print("Expected value:", exp_value)

        trjs = self.trj(0, ps, x0)
        if print_trjs:
            print("Trajectories:", trjs)

        rewards = self._m_map(lambda scs: self.sum_of_rewards(0, scs))(trjs)
        print("Sums of rewards for the resulting trajectories:", rewards)
        print("Consistency check: measuring the sums of rewards:", self.measure(rewards))

        ctrl_lists = self.get_control_seqs(trjs)
        print("Controls selected by the policies:")
        pp.pprint(ctrl_lists)

        if test_commit:
            xfs = self.get_final_states(trjs)
            print("\n--------------------------------------------\n")
            print("\nRunning commitment tests for final states...\n")
            for xf in xfs:
                print("\n------------------\n")
                self.test_commitment(xf)
            print("\n------------------\n")

        print("Log files can be found in the `log` subfolder.")
        print("Plots can be found in the `img` subfolder.")

        # ------------ #
        # plot results #
        # ------------ #

        ts = time.gmtime()
        timestamp = time.strftime("%Y-%m-%d_%H:%M:%S", ts)
        self.plot_trajectories(trjs, timestamp + "_ps_" + str(steps))
        self.log_results(exp_value, trjs, timestamp + "_ps_" + str(steps))

    # ---------------------------------------------------------------------------------------------------------------- #
    # ---------------------------------------------------------------------------------------------------------------- #

    # -------------------- #
    # Working with results #
    # -------------------- #

    def plot_trajectories(self, sp_traj: 'SimpleProb(StateCtrlSeq)', filename: str = "plot_") -> None:

        # Sort by probabilities first?

        list_of_traj = get_elements(sp_traj)
        list_of_probs = get_probabilities(sp_traj)

        list_of_state_lists = [self.map_scs(lambda xy: fst(xy), tr) for tr in list_of_traj]

        # Need to get rid of the last state:
        list_of_control_lists_aux = [self.map_scs(lambda xy: snd(xy), tr) for tr in list_of_traj]
        list_of_control_lists = [l[:-1] for l in list_of_control_lists_aux]

        list_of_time_lists = [list(map(lambda x: x.get_time(), l)) for l in list_of_state_lists]

        list_of_co2_act_lists = [list(map(lambda x: x.get_co2_emission_rate(), l)) for l in list_of_state_lists]
        list_of_so2_act_lists = [list(map(lambda x: x.get_so2_injection_rate(), l)) for l in list_of_state_lists]

        list_of_mat_lists = [list(map(lambda x: x.get_mat(), l)) for l in list_of_state_lists]
        list_of_mup_lists = [list(map(lambda x: x.get_mup(), l)) for l in list_of_state_lists]
        list_of_mlo_lists = [list(map(lambda x: x.get_mlo(), l)) for l in list_of_state_lists]

        list_of_tat_lists = [list(map(lambda x: x.get_tat(), l)) for l in list_of_state_lists]
        list_of_tlo_lists = [list(map(lambda x: x.get_tlo(), l)) for l in list_of_state_lists]

        # The more fine-grained mat and tat lists:
        list_of_micro_mat_lists = [list(map(lambda x: x.get_Mat_list(), l)) for l in list_of_state_lists]
        list_of_micro_tc_lists = [list(map(lambda x: x.get_tc_list(), l)) for l in list_of_state_lists]

        list_of_micro_tat_lists = [list(map(lambda x: x.get_Tat_list(), l)) for l in list_of_state_lists]
        list_of_micro_tt_lists = [list(map(lambda x: x.get_tt_list(), l)) for l in list_of_state_lists]

        list_of_ice_volume_lists = [list(map(lambda x: x.get_ice_volume(), l)) for l in list_of_state_lists]

        list_of_co2_control_lists = [list(map(lambda y: y.getCtrl()[0], l)) for l in list_of_control_lists[1:]]
        list_of_so2_control_lists = [list(map(lambda y: y.getCtrl()[1], l)) for l in list_of_control_lists[1:]]

        mat_lists = np.array(list_of_mat_lists)
        mup_lists = np.array(list_of_mup_lists)
        mlo_lists = np.array(list_of_mlo_lists)

        tat_lists = np.array(list_of_tat_lists)
        tlo_lists = np.array(list_of_tlo_lists)

        ice_volume_lists = np.array(list_of_ice_volume_lists)

        co2_act_lists = np.array(list_of_co2_act_lists)
        so2_act_lists = np.array(list_of_so2_act_lists)

        co2_lists = np.array(list_of_co2_control_lists)
        so2_lists = np.array(list_of_so2_control_lists)

        steps = len(mat_lists[0])

        # creating time data for plotting
        list_of_time = np.linspace(0, steps, steps, endpoint=False)

        # Plots are written to files at the moment. The version in which the plot pops up are commented out.
        # Mat
        self.plot_multiple_traj_shaded_by_prob(list_of_time, mat_lists, list_of_probs,
                                               "CO2 in atmosphere", filename + "_Mat")
        # Mup
        self.plot_multiple_traj_shaded_by_prob(list_of_time, mup_lists, list_of_probs,
                                               "CO2 in upper ocean", filename + "_Mup")
        # Mlo
        self.plot_multiple_traj_shaded_by_prob(list_of_time, mlo_lists, list_of_probs,
                                               "CO2 in lower ocean", filename + "_Mlo")

        # Tat
        self.plot_multiple_traj_shaded_by_prob(list_of_time, tat_lists, list_of_probs,
                                               "temperature anomaly atmosphere", filename + "_Tat")
        # Tlo
        self.plot_multiple_traj_shaded_by_prob(list_of_time, tlo_lists, list_of_probs,
                                               "temperature anomaly lower ocean", filename + "_Tlo")

        # Ice volume
        self.plot_multiple_traj_shaded_by_prob(list_of_time, ice_volume_lists, list_of_probs,
                                               "ice volume", filename + "_ice_volume")

        # CO2 emissions
        self.plot_multiple_traj_shaded_by_prob(list_of_time, co2_act_lists, list_of_probs,
                                               "CO2 emissions", filename + "_co2_action")
        # Aerosol injections
        self.plot_multiple_traj_shaded_by_prob(list_of_time, so2_act_lists, list_of_probs,
                                               "SO2 injections", filename + "_co2_action")

        """
        # Mat
        self.plot_multiple_traj_shaded_by_prob(list_of_time, mat_lists, list_of_probs, "CO2 in atmosphere")
        # Mup
        self.plot_multiple_traj_shaded_by_prob(list_of_time, mup_lists, list_of_probs, "CO2 in upper ocean")
        # Mlo
        self.plot_multiple_traj_shaded_by_prob(list_of_time, mlo_lists, list_of_probs, "CO2 in lower ocean")

        # Tat
        self.plot_multiple_traj_shaded_by_prob(list_of_time, tat_lists, list_of_probs, "temperature anomaly atmosphere")
        # Tlo
        self.plot_multiple_traj_shaded_by_prob(list_of_time, tlo_lists, list_of_probs, "temperature anomaly lower ocean")

        # Ice volume
        self.plot_multiple_traj_shaded_by_prob(list_of_time, ice_volume_lists, list_of_probs, "ice volume")

        # CO2 emissions
        self.plot_multiple_traj_shaded_by_prob(list_of_time, co2_act_lists, list_of_probs,
                                               "CO2 emissions")
        # Aerosol injections
        self.plot_multiple_traj_shaded_by_prob(list_of_time, so2_act_lists, list_of_probs,
                                               "SO2 injections")
        """

        # # CO2 controls
        # self.plot_multiple_traj_shaded_by_prob(list_of_time[:-1], co2_lists, list_of_probs[:-1], "CO2 controls")
        # # Aerosol controls
        # self.plot_multiple_traj_shaded_by_prob(list_of_time[:-1], so2_lists, list_of_probs[:-1], "SO2 controls")


    @staticmethod
    def plot_multiple_traj_shaded_by_prob(list_of_time: np.ndarray,
                                          list_of_data: np.ndarray,
                                          list_of_probs: List[Number],
                                          label_data: str,
                                          filename: str = None) -> None:
        """
        Plotting multiple possible trajectories in one diagram, shaded by probability.

        Remark: At the moment, we have a fixed number of possible trajectories (namely four) and
        a different base color is used for each of them. It might be better to use just one base color,
        so the distinction by probability shading is clearer (and it doesn'tmatter how many possible
        trajectories we have), and rather use different colors to mark
        qualitative differences between trajectories (e.g. in Marina's example to indicate whether
        the state of the world was "good" or "bad").

        :rtype: None
        """
        for i in range(len(list_of_data)):

            if i % 3 == 0:
                color = "blue"  # "green"
            elif i % 3 == 1:
                color = "blue"  # "red"
            else:
                color = "blue"

            plt.plot(list_of_time, list_of_data[i], color=color, alpha=list_of_probs[i], linewidth=2)

        plt.xlabel('step')
        plt.ylabel(label_data)
        plt.title('Trajectories shaded by probability')
        plt.grid()
        if filename == None:
            plt.show()
        else:
            plt.savefig('img/' + filename + '.png')
        plt.close()


    def plot_scenario(self, scenario, t_list, co2_list, so2_list, temp_list, volume_list) -> None:
        """Plots scenarios from commitment test."""

        plt.figure()
        # upper subplot with emission and injection scenario
        ax1 = plt.subplot(211)
        ax1.set_title('Scenario: ' + self.scenario_names[scenario])
        color = 'tab:blue'
        ax1.set_xlabel('year')
        pl1 = ax1.plot(t_list, co2_list, color=color, label='CO2 rate')
        ax1.tick_params(axis='y')
        ax1.legend(loc='upper left')
        plt.grid()
        ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis
        color = 'tab:red'
        pl2 = ax2.plot(t_list, so2_list, color=color, label='aerosol rate')
        ax2.tick_params(axis='y')
        ax2.legend(loc='upper right')
        # lower subplot with Tat and ice volume
        ax3 = plt.subplot(212)
        color = 'tab:blue'
        ax3.set_xlabel('year')
        pl3 = ax3.plot(t_list, volume_list, color=color, label='ice volume')
        ax3.tick_params(axis='y')
        ax3.legend(loc='upper left')
        plt.grid()
        ax4 = ax3.twinx()  # instantiate a second axes that shares the same x-axis
        color = 'tab:red'
        pl4 = ax4.plot(t_list, temp_list, color=color, label='temperature')
        ax4.tick_params(axis='y')
        ax4.legend(loc='upper right')

        ts = time.gmtime()
        timestamp = time.strftime("%Y-%m-%d_%H:%M:%S", ts)
        plt.savefig("img/" + timestamp + "_commitment_scenario_" + str(scenario))
        plt.close()


# TODO: Implement this properly.
# just a hack for the time being
class SDP_family_deterministic(SDP_BEAM_SRM):
    """Value for climate sensitivity parameter is fixed at initialization."""

    def __init__(self, cs: ClimateSensitivityT):
        super().__init__()
        self.cs = cs

    def get_cs(self) -> ClimateSensitivityT:
        return self. cs

    @cached
    def nexts(self, t: int, x: State, y: Ctrl) -> 'SimpleProb(State)':
        """An experimental transition function using the carbon cycle step function
            combined with the temperature and ice volume step function;
            The resulting probability distribution is degenerated: Its support is always a singleton,
            i.e. transitions are in fact deterministic.

            Args
            ----

            t: int
                the current decision step
            x: State
                the current state
            y: Ctrl
                the current control

            Result
            ------

            xps: SimpleProb(State)
                a probability distribution of successor states
            """

        # Hack to ensure the "right" value for the climate sensitivity parameter
        x.set_climate_sensitivity(self.get_cs())

        current_co2_emission_rate = x.get_co2_emission_rate()

        # Controls:
        # ----------

        co2_action, so2_action = self.get_control_action(y, current_co2_emission_rate)

        xp_action = self.next_coupled_models(x, co2_action, so2_action)

        xp = [(xp_action, 1.0)]

        return xp


# TODO: Implement this properly.
# just a hack for the time being
class SDP_family_binary_uncertainty(SDP_BEAM_SRM):
    """Value for climate sensitivity parameter and probability that
    decisions are not implemented are fixed at initialization."""

    def __init__(self, cs: ClimateSensitivityT, prob_no_action):
        super().__init__()
        self.cs = cs
        self.prob_no_action = prob_no_action

    def get_cs(self) -> ClimateSensitivityT:
        return self. cs

    @cached
    def nexts(self, t: int, x: State, y: Ctrl) -> 'SimpleProb(State)':
        """An experimental transition function using the carbon cycle step function
        combined with the temperature and ice volume step function;
        treats uncertainty related to the implementation of decisions.

        Args
        ----

        t: int
            the current decision step
        x: State
            the current state
        y: Ctrl
            the current control

        Result
        ------

        xps: SimpleProb(State)
            a probability distribution of successor states
        """

        # Hack to ensure the "right" value for the climate sensitivity parameter
        x.set_climate_sensitivity(self.get_cs())

        current_co2_emission_rate = x.get_co2_emission_rate()
        current_so2_injection_rate = x.get_so2_injection_rate()

        # Controls:
        # ----------

        co2_action, so2_action = self.get_control_action(y, current_co2_emission_rate)

        # Consider the case that decisions are not implemented
        co2_no_action = current_co2_emission_rate
        so2_no_action = current_so2_injection_rate

        xp_action = self.next_coupled_models(x, co2_action, so2_action)
        xp_no_action = self.next_coupled_models(x, co2_no_action, so2_no_action)

        xps = [(xp_action, self.prob_action), (xp_no_action, self.prob_no_action)]

        return xps
