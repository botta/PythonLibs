\begin{thebibliography}{1}

\bibitem{Glotter2014}
Michael~J. Glotter, Raymond~T. Pierrehumbert, Joshua~W. Elliott, Nathan~J.
  Matteson, and Elisabeth~J. Moyer.
\newblock {A simple carbon cycle representation for economic and policy
  analyses}.
\newblock {\em Climatic Change}, 126(3-4):319--335, 2014.

\bibitem{Joos2013}
F.~Joos, R.~Roth, J.~S. Fuglestvedt, G.~P. Peters, I.~G. Enting, W.~{Von Bloh},
  V.~Brovkin, E.~J. Burke, M.~Eby, N.~R. Edwards, T.~Friedrich, T.~L.
  Fr{\"{o}}licher, P.~R. Halloran, P.~B. Holden, C.~Jones, T.~Kleinen, F.~T.
  Mackenzie, K.~Matsumoto, M.~Meinshausen, G.~K. Plattner, A.~Reisinger,
  J.~Segschneider, G.~Shaffer, M.~Steinacher, K.~Strassmann, K.~Tanaka,
  A.~Timmermann, and A.~J. Weaver.
\newblock {Carbon dioxide and climate impulse response functions for the
  computation of greenhouse gas metrics: A multi-model analysis}.
\newblock {\em Atmospheric Chemistry and Physics}, 13(5):2793--2825, 2013.

\bibitem{Helwegen2019}
Koen~G. Helwegen, Claudia~E. Wieners, Jason~E. Frank, and Henk~A. Dijkstra.
\newblock {Complementing CO2 emission reduction by solar radiation management
  might strongly enhance future welfare}.
\newblock {\em Earth System Dynamics}, 10(3):453--472, 2019.

\bibitem{Gregory2000}
J.~M. Gregory.
\newblock {Vertical heat transports in the ocean and their effect on
  time-dependent climate change}.
\newblock {\em Climate Dynamics}, 16(7):501--515, 2000.

\bibitem{Held2010}
Isaac~M. Held, Michael Winton, Ken Takahashi, Thomas Delworth, Fanrong Zeng,
  and Geoffrey~K. Vallis.
\newblock {Probing the fast and slow components of global warming by returning
  abruptly to preindustrial forcing}.
\newblock {\em Journal of Climate}, 23(9):2418--2427, 2010.

\end{thebibliography}
