#!/usr/bin/env python
# coding: utf-8

# ESD example in the generic setting
# TODO: Clean up

# imports
from typing import GenericAlias, Callable
# Iterable, TupleMeta, CallableMeta, GenericMeta

# pythonLibs imports
from sdp_libs.category_theory.instances import genSimpleProbMonad
from sdp_libs.utils.base import *  # identity, fst, snd, head, tail, domain, codomain, compose
from sdp_libs.probability_theory.base import *
from sdp_libs.sequential_decision_problems.simple_core_theory import *


# Constants

L: bool = True
H: bool = False

U: bool = True
A: bool = False

B: bool = True
G: bool = False

LH: type = bool
UA: type = bool
BG: type = bool

crE: int = 4

crN: int = 3

pS1: float = 9 / 10  # prob. of staying in a good world if cumulated emissions <= crE

pS2: float = 1 / 10  # prob of staying in a good world if cumulated emissions > crE

b: float = 0.5

h: float = 0.3

la: float = 0.2

lu: float = 0.1


# Controls

Ctrl = LH


def to_iterable_LH() -> Iterable:
    return [L, H]


# States

State: GenericAlias = Tuple[int, Tuple[LH, Tuple[UA, BG]]]
print(type(State))


# initial state

x0: State = (0, (H, (U, G)))


# Prepare Val

base_type = float
zero = 0


def comparison_op(x: base_type, y: base_type) -> bool:
    return x <= y


def bin_op(x: base_type, y: base_type) -> base_type:
    return x + y


Val = PointedPreorderSemiGroup(base_type, comparison_op, zero, bin_op)


# Observer functions for State

def cumulatedEmissions(x: State) -> int:
    return x[0]


def currentEmissions(x: State) -> LH:
    return x[1][0]


def technology(x: State) -> UA:
    return x[1][1][0]


def stateOfTheWorld(x: State) -> BG:
    return x[1][1][1]


# Other helpers

def mk_state(x1: int, x2: LH, x3: UA, x4: BG) -> State:
    return (x1, (x2, (x3, x4)))

def constH(x: State) -> Ctrl:
    return H


def constL(x: State) -> Ctrl:
    return L


# Policies and PolicySequences

Policy: GenericAlias = Callable[[State], Ctrl]
PolicySeq: GenericAlias = List[Policy]


def nexts(t: int, x: State, y: Ctrl) -> SimpleProb(State):

    e = cumulatedEmissions(x)

    if y == L:

        if stateOfTheWorld(x) == B:

            if t <= crN:

                xps = [(mk_state(e, L, U, B), 1.0)]

            elif t > crN:

                xps = [(mk_state(e, L, A, B), 1.0)]

        elif stateOfTheWorld(x) == G:

            if e <= crE:

                p = pS1

            elif e > crE:

                p = pS2

            if t <= crN:

                xps = [(mk_state(e, L, U, G), p), (mk_state(e, L, U, B), 1.0 - p)]

            elif t > crN:

                xps = [(mk_state(e, L, A, G), p), (mk_state(e, L, A, B), 1.0 - p)]

    elif y == H:

        if stateOfTheWorld(x) == B:

            if t <= crN:

                xps = [(mk_state(e + 1, H, U, B), 1.0)]

            elif t > crN:

                xps = [(mk_state(e + 1, H, A, B), 1.0)]

        elif stateOfTheWorld(x) == G:

            if e <= crE:

                p = pS1

            elif e > crE:

                p = pS2

            if t <= crN:

                xps = [(mk_state(e + 1, H, U, G), p), (mk_state(e + 1, H, U, B), 1.0 - p)]

            elif t > crN:

                xps = [(mk_state(e + 1, H, A, G), p), (mk_state(e + 1, H, A, B), 1.0 - p)]

    return xps


def reward(t: int, x: State, y: Ctrl, xp: State) -> Val:

    rew = 0

    if currentEmissions(xp) == H:

        rew = h

    elif currentEmissions(xp) == L:

        if technology(xp) == A:

            rew = la

        elif technology(xp) == U:

            rew = lu

    if stateOfTheWorld(xp) == G:

        rew = rew + 1.0

    elif stateOfTheWorld(xp) == B:

        rew = rew + b

    return rew


# FCSDP instance

sdp = FiniteControlSequentialDecisionProblem(genSimpleProbMonad, State, Ctrl, nexts,
      Val, expected_value, reward, to_iterable_LH())


# Tests

x1: State = mk_state(1, H, U, G)

print('reward(0, x0, H, x1) = ', sdp.reward(0, x0, H, x1))

print('nexts(0, x0, H) = ', sdp.nexts(0, x0, H))

print('sdp.val(0, x0, [constH,constL]) = ', sdp.val(0, x0, [constH, constL]))

print('sdp.valp(0, x0, [constH,constL]) = ', sdp.valp(0, x0, [constH, constL]))

print('trj:', sdp.trj(0, [constH, constH], x0))

print('val(0, x0, bi(0, 9)) = ', sdp.val(0, x0, sdp.backwards_induction(0, 9)))

print('valp(0, 9, bi(0, 9), x0) = ', sdp.valp(0, x0, sdp.backwards_induction(0, 9)))

