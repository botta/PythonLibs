# TODO: - Wrappers for time, carbon and temperature state types?
#       - Distinguish between "micro" and "macro" states?
#       - Harmonize methods with AbstractStateClass
#       - Docstrings


# imports

## general

import numpy as np

## project

from sdp_libs.sequential_decision_problems.core_theory_dbc_parent import AbstractStateClass


# -------------------------------------------------------------------------------------------------------------------- #

class State(AbstractStateClass):

    def __init__(self,
                 time_state: (int, int),
                 carbon_state: (float, float, float, float, np.ndarray, np.ndarray),
                 temp_state: (float, float, float, np.ndarray, np.ndarray),
                 climate_sensitivity: float):
        """Initializes the object with the given state components t, lh, ua, bg,
        if they form a valid state according to checkState and
        raises a ValueError otherwise."""
        if self.checkState(time_state, carbon_state, temp_state, climate_sensitivity):
            self.t = time_state[0]
            self.dt = time_state[1]
            self.dt1 = carbon_state[0]
            self.Mat = carbon_state[1]
            self.Mup = carbon_state[2]
            self.Mlo = carbon_state[3]
            self.tc_list = carbon_state[4]
            self.Mat_list = carbon_state[5]
            self.dt2 = temp_state[0]
            self.Tat = temp_state[1]
            self.Tlo = temp_state[2]
            self.tt_list = temp_state[3]
            self.Tat_list = temp_state[4]
            self.climate_sensitivity = climate_sensitivity  # will be set "properly" in first transition step
        else:
            raise ValueError("Invalid state value!")

    def __copy__(self):
        s = State((self.t, self.dt),
                  (self.dt1, self.Mat, self.Mup, self.Mlo, self.tc_list, self.Mat_list),
                  (self.dt2, self.Tat, self.Tlo, self.tt_list, self.Tat_list),
                  self.climate_sensitivity)
        return s

    def __repr__(self):  # Tatlist:%s), # Matlist:%s),
        """Pretty printing."""
        return "\n<State: [Time: (t:%i, dt:%i), " \
               "CO2: (dt1:%f, Mat:%f, Mup:%f, Mlo:%f, " \
               "Temp: (dt2:%f, Tat:%f,Tlo:%f, " \
               "CS:%f]>" \
               % (self.t, self.dt, self.dt1,
                  self.Mat, self.Mup, self.Mlo, # str(self.Mat_list),
                  self.dt2, self.Tat, self.Tlo, # str(self.Tat_list),
                  self.climate_sensitivity)

    def __str__(self): # Tatlist:%s), # Matlist:%s),
        """Pretty printing."""
        return "\n<State: [Time: (t:%i, dt:%i), " \
               "CO2: (dt1:%f, Mat:%f, Mup:%f, Mlo:%f, " \
               "Temp: (dt2:%f, Tat:%f,Tlo:%f,  " \
               "CS:%f]>" \
               % (self.t, self.dt, self.dt1,
                  self.Mat, self.Mup, self.Mlo, # str(self.Mat_list),
                  self.dt2, self.Tat, self.Tlo, # str(self.Tat_list),
                  self.climate_sensitivity)

    # TODO: Align type information with abstract class
    def checkState(self, time_s, co2_s, temp_s, cs) -> bool:
        """Returns `True` if ::time_s::, ::co2_s::, ::temp_s::, ::cs:: define a valid state and `False` otherwise.
        Should be extended."""
        if ((type(time_s) == (int, int))
            & (type(co2_s) == (float, float, float, float, np.ndarray, np.ndarray)) \
            & (type(temp_s) == (float, float, float, np.ndarray, np.ndarray)) \
            & (time_s[1] % co2_s[0] == 0) \
            & (time_s[1] % temp_s[0] == 0) \
            & (cs > 0)):
            return True
        else:
            #return False
            return True

    def getState(self):
        """Returns the state value as quadruple. """
        return ((self.t, self.dt),
                (self.dt1, self.Mat, self.Mup, self.Mlo, self.tc_list, self.Mat_list),
                (self.dt2, self.Tat, self.Tlo, self.tt_list, self.Tat_list),
                self.climate_sensitivity)

    def get_observable_state(self):
        """Returns the state value as quadruple. """
        return (self.t, self.dt), \
               (self.Mat, self.Mup, self.Mlo, self.tc_list, self.Mat_list), \
               (self.Tat, self.Tlo, self.tt_list, self.Tat_list)

    def set_observable_state(self, time_state, carbon_state, temp_state) -> None:
        self.t = time_state[0]
        self.dt = time_state[1]
        self.dt1 = carbon_state[0]
        self.Mat = carbon_state[1]
        self.Mup = carbon_state[2]
        self.Mlo = carbon_state[3]
        self.tc_list = carbon_state[4]
        self.Mat_list = carbon_state[5]
        self.dt2 = temp_state[0]
        self.Tat = temp_state[1]
        self.Tlo = temp_state[2]
        self.tt_list = temp_state[3]
        self.Tat_list = temp_state[4]

    def get_time(self) -> int:
        """Returns the current time ::t::. """
        return self.t

    def set_time(self, t: int) -> None:
        self.t = t

    def get_dt(self) -> int:
        """Returns the step size ::dt::. """
        return self.dt

    def set_dt(self, dt: int) -> None:
        self.dt = dt

    def get_dt1(self) -> float:
        """Returns the carbon step size ::dt1::. """
        return self.dt1

    def set_dt1(self, dt1: float) -> None:
        self.dt1 = dt1

    def get_mat(self) -> float:
        """Returns ::Mat::. """
        return self.Mat

    def set_mat(self, mat: float) -> None:
        self.Mat = mat

    def get_mup(self) -> float:
        """Returns ::Mup::. """
        return self.Mup

    def set_mup(self, mup: float) -> None:
        self.Mup = mup

    def get_mlo(self) -> float:
        """Returns ::Mlo::. """
        return self.Mlo

    def set_mlo(self, mlo: float) -> None:
        self.Mlo = mlo

    def get_tc_list(self) -> np.ndarray:
        """Returns ::tc_list::. """
        return self.tc_list

    def set_tc_list(self, tc_list: np.ndarray) -> None:
        self.tc_list = tc_list

    def get_Mat_list(self) -> np.ndarray:
        """Returns ::Mat_list::. """
        return self.Mat_list

    def set_Mat_list(self, Mat_list: np.ndarray) -> None:
        self.Mat_list = Mat_list

    def get_dt2(self) -> float:
        """Returns the temperature step size ::dt2::. """
        return self.dt2

    def set_dt2(self, dt2: float) -> None:
        self.dt2 = dt2

    def get_tat(self) -> float:
        """Returns ::Tat::. """
        return self.Tat

    def set_tat(self, tat: float) -> None:
        self.Tat = tat

    def get_tlo(self) -> float:
        """Returns ::Tlo::. """
        return self.Tlo

    def set_tlo(self, tlo: float) -> None:
        self.Tlo = tlo

    def get_tt_list(self) -> np.ndarray:
        """Returns ::tt_list::. """
        return self.tt_list

    def set_tt_list(self, tt_list: np.ndarray) -> None:
        self.tt_list = tt_list

    def get_Tat_list(self) -> np.ndarray:
        """Returns ::Tat_list::. """
        return self.Tat_list

    def set_Tat_list(self, Tat_list: np.ndarray) -> None:
        self.Tat_list = Tat_list

    def get_climate_sensitivity(self) -> float:
        """ Returns::slimate_sensitivity::."""
        return self.climate_sensitivity

    def set_climate_sensitivity(self, cs: float) -> None:
        self.climate_sensitivity = cs
