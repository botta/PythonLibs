# Questions about BEAMSRMICEControlsExample

## State.py
- What is the difference between `__repr__` and `__str__` ?

- Why does `CheckState` prints `print(type(co2_s[1]))` if state does not pass the type checking?

- The trajectories (for Mat and Tat) are stored in the state right? States further in the future are bigger then right? Is this better/faster than having all states be of the same 'small' size and reconstructing the trajectories afterwards? Is this the reason that you decided to do it like this?

## Control.py
- I did not see the actual value of the injection rates, or the changes in emission rates. Are they defined in SDP.py? ---> Yes, saw them there! :)

## SDP.py

- I am not familiar with the `partial` function from `functools`. I looked it up but I am still not fully understanding what it does and how are you using it in: 

``` self.scenario_rate_functions = [partial(self.init_scenarios(), s) for s in self.all_scenarios]```

- What is (or will be) the difference between `scenario_rate_functions` and `scenario_rate_functions_aux`?

- (curiosity) In the while loop of the carbon cycle. You decide to use `is` in `if i % ratio_temp is 0:` instead of `==`, is it because it is faster?

- In the `nexts` function, if I understood well, the evolution is happening with the old rates instead of the new ones. I think we should change the order of some things:
    - get current co2 and so2 rates from state
    - do the control section which determines what is the action to be applied in the period that the next function will evolve.
    - evolve the physical models with the actions derived in the control section, not with the current co2 and so2 rates that are in the state
    - prepare next state
    
- `reward`:
    - can we add a `weight_comitment`?
    - I would like to compute the reward at every step but to include the comitment test only in the last step. I think this makes sense if we are including all the other quantities in the reward function. This might be how it is being done already but I am not 100% sure. 
    - (suggestion) let's remove all the plotting related code (including the lists appending etc.) from the commitment function. It makes the test almost twice as fast (I timed it in the jupyter-notebook only.)

- (I might be saying something wrong here) In the staticmethods co2_increase the: `if r < 50: return min(2, r)` would be better returning just `return 2` no? I think the function should be:

```
    def co2_increase(current_co2_emission_rate: int) -> int:
        r = current_co2_emission_rate + 2

        if r < 50:
            return 2
        elif current_co2_emission_rate == 50:
            return 0
        else:
            return 50 - current_co2_emission_rate
```

- in init_scenarios in get_rate_at_time, can emissions and injections be functions of time? This would be desirable if we wanted to add some of the other scenarios that include decarbonisation transition.

