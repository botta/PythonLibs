
class Preorder:

    def __init__(self, base_type, comparison_op):
        self.base_type = base_type
        self.comparison_op = comparison_op

    def base_type(self) -> type:
        """returns the underlying type."""
        return self.base_type

    def comparison_op(self, v1: base_type, v2: base_type) -> bool:
        return self.comparison_op(v1, v2)


class PointedPreorder(Preorder):

    def __init__(self, base_type, comparison_op, base_point):
        super().__init__(base_type, comparison_op)
        self.base_point = base_point

    def __base(self) -> type:
        """returns the underlying type."""
        return super().base_type

    def base_point(self) -> __base:
        """returns the default value."""
        return self.base_point


class PointedPreorderSemiGroup(PointedPreorder):

    def __init__(self, base_type, comparison_op, base_point, bin_op):
        super().__init__(base_type, comparison_op, base_point)
        self.bin_op = bin_op

    def __base(self) -> type:
        return self.base_type

    def bin_op(self, v1: __base, v2: __base) -> __base:
        """returns the default value."""
        return self.bin_op(v1, v2)
