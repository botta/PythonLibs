#!/usr/bin/env python
# coding: utf-8

# TODO: - Clean up
#       - Docstrings
#       - Time-dependent Controls
#       - Move AbstractStateClass and AbstractControlClass to their own module
#       - Tabulated backwards induction



# Simplified port from *IdrisLibs/.../CoreTheory.lidr*

# imports

from typing import Any, Union
from abc import ABC, abstractmethod
from timeit import default_timer as timer
from memoization import cached

# for parallelization experiments
from functools import partial
from multiprocessing import cpu_count
from joblib import Parallel, delayed



# pythonLibs imports
from sdp_libs.category_theory.definitions import Monad
from sdp_libs.order_theory.preorder import PointedPreorderSemiGroup
from sdp_libs.utils.base import *  # head, tail, cons, fst, snd


## counter which can be used for illustrating complexity
## by uncommenting the accompanying code in cval_argmax :
# counter = 0


# Type variable
A = TypeVar('A')


"""
----------------------------------------
## Generic sequential decision processes
----------------------------------------
"""

class AbstractStateClass(ABC):

    @abstractmethod
    def getState(self) -> object:
        """Returns the state value. """
        return self.x

    @abstractmethod
    def checkState(self, x) -> bool:
        """Returns `true` if x is a valid control and `false` otherwise."""
        pass


class AbstractControlClass(ABC):

    @abstractmethod
    def getCtrl(self) -> str:
        """Returns the control value. """
        return self.y

    @abstractmethod
    def checkCtrl(self, y) -> bool:
        """Returns true if y is a valid control and false otherwise."""
        pass


# Deviating from the naming convention for fields that
# will serve as "types": for these we follow the naming convention for classes.


class SequentialDecisionProcess:

    def __init__(self, monad: Monad, StateClass, CtrlClass):
        if isinstance(monad, Monad):
            self.monad = monad
        else:
            raise ValueError("Invalid Monad instance!")

        if issubclass(StateClass, AbstractStateClass):
            self.State = StateClass
        else:
            raise ValueError("Invalid state class!")
        if issubclass(CtrlClass, AbstractControlClass):
            self.Ctrl = CtrlClass
        else:
            raise ValueError("Invalid control class!")


    def monad(self) -> Monad:
        """returns the monad."""
        return self.monad

    def _mso(self):
        return self.monad.m_functor.src.obj

    def _M(self, X: object) -> object:
        return self.monad.m_functor.f_obj(X)

    def _m_map(self, f: Callable[[_mso], _mso]) -> Callable[['_M(_mso)'], '_M(_mso)']:
        return self.monad.m_functor.f_mor(f)

    def _m_pure(self, x: _mso) -> '_M(_mso)':
        return self.monad.m_pure(x)

    def _m_bind(self, f: Callable[[_mso], '_M(_mso)'], mx: '_M(_mso)') -> '_M(_mso)':
        return self.monad.m_bind(f, mx)

    def State(self) -> object:
        """returns the state space."""
        return self.State

    def Ctrl(self) -> object:
        """returns the control space."""
        return self.Ctrl

    def nexts(self, t: int, x: State, y: Ctrl) -> '_M(State)':
        """the monadic transition function s.t.
        nexts(t,x,y) is the successor state at time t+1,
        to be implemented by concrete subclasses."""
        pass

    Policy = Callable[[State], Ctrl]

    PolicySeq = List[Policy]

    StateCtrlSeq = List[Union[Tuple[State, Ctrl], State]]

    @staticmethod
    def head_state(scs: StateCtrlSeq) -> State:
        if scs == []:
            raise ValueError("Empty StateCtrlSequence!")
        elif len(scs) == 1:
            return head(scs)
        else:
            return fst(head(scs))

    @staticmethod
    def map_scs(f, scs: StateCtrlSeq) -> StateCtrlSeq:
        scslast = scs[-1]
        scsp = scs[:-1]
        scspp = list(map(f, scsp))
        return scspp + [scslast]

    def trj(self, t: int, ps: PolicySeq, x: State) -> '_M(StateCtrlSeq)':
        # define some notation for readability and type annotations

        M = self._M
        State = self.State
        StateCtrlSeq = self.StateCtrlSeq
        PolicySeq = self.PolicySeq
        m_map = self._m_map
        m_bind = self._m_bind
        trj = self.trj

        # computation of the trajectories

        if is_nil(ps):
            result = self._m_pure([x])

        else:
            p = head(ps)
            psp = tail(ps)

            y = p(x)
            mxp = self.nexts(t, x, y)

            def trj_ps(t: int, ps: PolicySeq) -> Callable[[State], M(StateCtrlSeq)]:
                return lambda x: trj(t, ps, x)

            trajectories = trj_ps(t + 1, psp)
            bind_trj_mxp = m_bind(trajectories, mxp)
            result = m_map(cons((x, y)))(bind_trj_mxp)

        return result

    def trj_iter(self, t: int, ps: PolicySeq, x: State) -> '_M(StateCtrlSeq)':
        "Iterative version of `trj`."

        length_ps = len(ps)

        mscs = self._m_pure([x])

        def helper(t: int, scs: 'StateCtrlSeq', y: 'Ctrl') -> '_M(StateCtrlSeq)':
            x = scs[-1]

            mxp = self.nexts(t, x, y)
            return self._m_map(lambda xp: scs[:-1] + [(x, y)] + [xp])(mxp)

        for i in range(length_ps):

            p = ps[i]
            y = p(x)
            mscs = self._m_bind(lambda scs: (helper(t, scs, y)), mscs)
            t += 1

        return mscs

class FiniteControlSequentialDecisionProblem(SequentialDecisionProcess):

    def __init__(self, monad: Monad, State, Ctrl, val_ord: PointedPreorderSemiGroup, all_ctrls):
        super().__init__(monad, State, Ctrl)
        self.val_ord = val_ord
        self.all_ctrls = all_ctrls

    def val_ord(self) -> PointedPreorderSemiGroup:
        """returns the underlying type of val_ord."""
        return self.val_ord

    def _Val(self) -> type:
        return self.val_ord.base_type

    @property
    def _zero(self) -> _Val:
        return self.val_ord.base_point

    def _le(self, v1: _Val, v2: _Val) -> _Val:
        return self.val_ord.comparison_op(v1, v2)

    def _oplus(self, v1: _Val, v2: _Val) -> _Val:
        return self.val_ord.bin_op(v1, v2)

    def ooplus(self, f: Callable[[Any], _Val], g: Callable[[Any], _Val]) -> Callable[[Any], _Val]:
        return lambda x: self._oplus(f(x), g(x))

    def measure(self, mv: '__M(_Val)') -> _Val:
        """measure function, e.g. expectedValue, needs to be implemented in child class."""
        pass

    def reward(self, t: int, x: 'State', y: 'Ctrl', xp: 'State') -> _Val:
        """The *reward* / *cost* / *utility* function needs to be implemented in child class."""
        pass

    Policy = Callable[['State'], 'Ctrl']

    PolicySeq = List['Policy']

    StateCtrlSeq = List[Union[Tuple['State', 'Ctrl'], 'State']]

    def sum_of_rewards(self, t: int, scs: StateCtrlSeq) -> _Val:
        oplus = self._oplus

        if is_nil(scs):
            raise ValueError('Problem in sum_of_rewards function: empty StateCtrlSequence.')

        elif is_singleton(scs):
            result = self._zero

        else:
            sc = head(scs)
            s = fst(sc)
            c = snd(sc)
            scsp = tail(scs)

            if len(scsp) == 1:
                sp = head(scsp)

            else:
                spcp = head(scsp)
                sp = fst(spcp)

            sr = oplus(self.reward(t, s, c, sp), self.sum_of_rewards(t + 1, scsp))
            result = sr

        return result

    @cached
    def valp(self, t: int, x: 'State', ps: PolicySeq) -> _Val:

        m_map = self.monad.m_functor.f_mor
        trajectories = self.trj(t, ps, x)
        sr = m_map(lambda r: self.sum_of_rewards(t, r))(trajectories)
        #print(sr)
        meas = self.measure(sr)

        return meas

    @cached
    def sval(self, t: int, x: 'State', y: 'Ctrl', ps: PolicySeq) -> Callable[['State'], _Val]:

        def rv(xp):
            r = self.reward(t, x, y, xp)
            v = self.val_iter(t + 1, xp, ps)


            # print("r:", r, ",v: ", v)

            return self._oplus(r, v)

        return rv

    @cached
    def cval(self, t: int, x: 'State', ps: PolicySeq, y: 'Ctrl') -> _Val:

        m_map = self.monad.m_functor.f_mor

        mxp = self.nexts(t, x, y)
        mv = m_map(self.sval(t, x, y, ps))(mxp)
        cvy = self.measure(mv)

        return cvy

    def val(self, t: int, x: 'State', ps: PolicySeq) -> _Val:

        if is_nil(ps):
            result = self._zero
        else:
            p = head(ps)
            psp = tail(ps)
            y = p(x)

            result = self.cval(t, x, psp, y)

        return result

    def val_cont(self, t: int, ps: PolicySeq, k) -> Callable[['State'], _Val]:

        if is_nil(ps):
            def const_zero(x):
                return self._zero

            result = k(const_zero)

        else:
            p = head(ps)
            psp = tail(ps)
            m_map = self.monad.m_functor.f_mor

            def context(kp, x):

                y = p(x)

                def rv(xp):
                    r = self.reward(t, x, y, xp)
                    v = kp(xp)

                    #print("r:", r, ",v: ", v)

                    return self._oplus(r, v)

                mxp = self.nexts(t, x, y)
                cvy = self.measure(m_map(rv)(mxp))

                return cvy

            def cont(kp):
                def aux(x):
                    return context(kp, x)
                return k(aux)

            result = self.val_cont(t+1, psp, cont)

        return result

    @cached
    def val_iter(self, t: int, x: 'State', ps: PolicySeq) -> Callable[['State'], _Val]:

        m_map = self.monad.m_functor.f_mor

        def helper1(xp, t, x, y, v):
            return xp, self._oplus(self.reward(t, x, y, xp), v)

        def helper2(t, xv, p):
            x = fst(xv)
            v = snd(xv)
            y = p(x)
            mxp = self.nexts(t, x, y)
            return m_map(lambda xp: helper1(xp, t, x, y, v))(mxp)

        mxv = self._m_pure((x, 0))
        i = 0
        n = len(ps)

        while (i < n):

            p = ps[i]
            mxv = self._m_bind(lambda xv: helper2(t, xv, p), mxv)
            i = i + 1
            t = t + 1

        mv = m_map(lambda xv: snd(xv))(mxv)
        result = self.measure(mv)

        return result

    #"""
    @cached
    def cval_argmax(self, t: int, x: 'State', ps: PolicySeq) -> 'Ctrl':
        "parallel version"

        def f(t, x, ps, y):
            return y, self.cval(t, x, ps, self.Ctrl(y))

        fp = partial(f, t, x, ps)

        cval_list = Parallel(n_jobs=cpu_count())(delayed(fp)(y) for y in self.all_ctrls)

        argmax = fst(cval_list[0])
        v_argmax = snd(cval_list[0])

        for ycv in cval_list:

            if not self._le(snd(ycv), v_argmax):
                #print(argmax, v_argmax, ycv)
                argmax = fst(ycv)
                v_argmax = snd(ycv)


        # global counter
        # if counter == None:
        #     counter = 0
        # else:
        #     counter = counter + 1
        #print(counter, len(ps), argmax, x, "\n", cval_list)

        return self.Ctrl(argmax)


    """
    def cval_argmax(self, t: int, x: 'State', ps: PolicySeq) -> 'Ctrl':

        cval_list = [(y, self.cval(t, x, ps, self.Ctrl(y))) for y in self.all_ctrls]

        argmax = fst(cval_list[0])
        v_argmax = snd(cval_list[0])

        for ycv in cval_list:

            if not self._le(snd(ycv), v_argmax):
                # print(argmax, v_argmax, ycv)
                argmax = fst(ycv)
                v_argmax = snd(ycv)

        # global counter
        # if counter == None:
        #     counter = 0
        # else:
        #     counter = counter + 1
        # print(counter, len(ps), argmax, x, "\n", cval_list)

        return self.Ctrl(argmax)

    """

    @cached
    def optimal_extension(self, t: int, ps: PolicySeq) -> Policy:
        """returns the optimal_extension function"""
        return lambda x: self.cval_argmax(t, x, ps)

    def backwards_induction(self, t: int, n: int) -> PolicySeq:

        if n == 0:
            return []
        else:
            ps = self.backwards_induction(t + 1, n - 1)
            return [self.optimal_extension(t, ps)] + ps

    def backwards_induction_acc(self, t: int, n: int, acc=[]) -> PolicySeq:
        if n == 0:
            return [] + acc
        else:
            ps = self.backwards_induction_acc(t + 1, n - 1, [self.optimal_extension(t, acc)] + acc)
            return ps

    def backwards_induction_iter(self, t: int, n: int, acc: PolicySeq = []) -> PolicySeq:
        while n > 0:
            (t, n, acc) = (t + 1, n - 1, [self.optimal_extension(t, acc)] + acc)
        return acc

    def timed_val(self, t: int, x: 'State', ps: PolicySeq) -> _Val:
        t1 = timer()
        v = self.val(t, x, ps)
        t2 = timer()

        print(f"val: Computed in {t2 - t1:0.4f} seconds.")

        return v

    def timed_val_iter(self, t: int, x: 'State', ps: PolicySeq) -> _Val:
        t1 = timer()
        v = self.val_iter(t, x, ps)
        t2 = timer()

        print(f"val_iter: Computed in {t2 - t1:0.4f} seconds.")

        return v

    def timed_val_cont(self, t: int, x: 'State', ps: PolicySeq) -> _Val:
        t1 = timer()
        v = self.val_cont(t, ps, lambda x: x)(x)
        t2 = timer()

        print(f"val_cont: Computed in {t2 - t1:0.4f} seconds.")

        return v


    def timed_valp(self, t: int, x: 'State', ps: PolicySeq) -> _Val:
        t1 = timer()
        vp = self.valp(t, x, ps)
        t2 = timer()

        print(f"valp: Computed in {t2 - t1:0.4f} seconds.")

        return vp

    def timed_backwards_induction(self, t: int, n: int) -> PolicySeq:
        t1 = timer()
        bi = self.backwards_induction(t, n)
        t2 = timer()

        print(f"backward_induction(%i, %i): Computed in {t2 - t1:0.4f} seconds." % (t, n))

        return bi

    def timed_backwards_induction_iter(self, t: int, n: int) -> PolicySeq:
        t1 = timer()
        bi = self.backwards_induction_iter(t, n)
        t2 = timer()

        print(f"backward_induction_iter(%i, %i): Computed in {t2 - t1:0.4f} seconds." % (t, n))

        return bi
