# imports

from typing import Callable


class SemiGroup:

    def __init__(self, base_type, bin_op):
        self.base_type = base_type
        self.bin_op = bin_op

    def base_type(self) -> type:
        """returns the underlying type."""
        return self.base_type

    def bin_op(self, v1: base_type, v2: base_type) -> base_type:
        return self.bin_op(v1, v2)


class Monoid(SemiGroup):

    def __init__(self, base_type, bin_op, unit):
        super().__init__(base_type, bin_op)
        self.unit = unit

    def unit(self) -> super().base_type:
        """returns the neutral element of the semigroup."""
        return self.unit


class Group(Monoid):

    def __init__(self, base_type, bin_op, unit, inverse):
        super().__init__(base_type, bin_op, unit)
        self.inverse = inverse

    def __base(self) -> type:
        return self.base_type

    def inverse(self) -> Callable[[__base], __base]:
        """returns the inverse operation of the group."""
        return self.inverse
