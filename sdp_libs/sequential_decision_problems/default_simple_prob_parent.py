#!/usr/bin/env python
# coding: utf-8

# Experimenting with a SimpleProb-Default version of the framework


# imports

import logging

## project
from sdp_libs.category_theory.instances import genSimpleProbMonad
from sdp_libs.probability_theory.base import *
from sdp_libs.sequential_decision_problems.core_theory_dbc_parent import *


# Prepare Val

base_type = float
zero = 0


def comparison_op(x: base_type, y: base_type) -> bool:
    return x <= y


def bin_op(x: base_type, y: base_type) -> base_type:
    return x + y


Val = PointedPreorderSemiGroup(base_type, comparison_op, zero, bin_op)


class SP_SDP(FiniteControlSequentialDecisionProblem):

    def __init__(self, State, Ctrl, all_ctrls):
        super().__init__(genSimpleProbMonad, State, Ctrl, Val, all_ctrls)

    @cached
    def measure(self, mv: 'SimpleProb(Val)') -> 'Val':
        return expected_value(mv)

    def get_state_seqs_with_prob(self, trj_prob):
        state_seqs_prob = self.monad.m_functor.f_mor(lambda scs: (self.map_scs(lambda sc: fst(sc), scs)))(trj_prob)
        return state_seqs_prob

    # TODO: Needs checking
    def get_state_seqs(self, trj_prob):
        state_seqs = map(lambda scs: (self.map_scs(lambda sc: fst(sc), scs)), get_elements(trj_prob))
        return state_seqs

    def get_probs(self, trj_prob):
        probs = get_probabilities(trj_prob)
        return probs

    def get_final_states(self, sp_traj: 'SimpleProb(StateCtrlSeq)') -> List['State']:
        """Get final states of trajectories."""
        list_of_traj = get_elements(sp_traj)
        list_of_state_lists = [self.map_scs(lambda xy: fst(xy), tr) for tr in list_of_traj]
        return [xs[-1] for xs in list_of_state_lists]

    def get_control_seqs(self, sp_traj: 'SimpleProb(StateCtrlSeq)') -> List['Ctrl']:
        """Get final states of trajectories."""
        list_of_traj = get_elements(sp_traj)
        list_of_ctrl_lists = [self.map_scs(lambda xy: snd(xy), tr) for tr in list_of_traj]
        return [l[:-1] for l in list_of_ctrl_lists]

    def log_results(self, value: float, traj: 'SimpleProb(StateCtrlSeq)', filename: str) -> None:
        """Saves the result of a computation to a log file.
        *Remark*: This is just a very preliminary way to save results."""
        logging.basicConfig(filename='log/' + filename + '.log', level=logging.INFO)
        logging.info("\n\tExpected value:" + str(value) + "\n\t" + str(traj))


