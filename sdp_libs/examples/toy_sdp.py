#!/usr/bin/env python
# coding: utf-8

# Stripped down example

# imports
# from typing import Literal ~> new in Python 3.8

# pythonLibs imports
from sdp_libs.category_theory.instances import genSimpleProbMonad
from sdp_libs.probability_theory.base import *
from sdp_libs.sequential_decision_problems.simple_core_theory import *


# Problem specific definitions for small example

# Constants

# Does not work before Python 3.8
"""
LH: type = Literal['L', 'H']

BG: type = Literal['B', 'G']

L: LH = 'L'
H: LH = 'H'

B: BG = 'B'
G: BG = 'G'
"""

# For the time being
LH: type = bool

BG: type = bool

L: LH = True
H: LH = False

B: BG = True
G: BG = False


def to_iterable_LH() -> Iterable:
    return [L, H]


def to_iterable_BG() -> Iterable:
    return [B, G]


# Controls

Ctrl = LH

# States

State = BG

# initial state

x0: State = G

print('initial state x0 = ', x0)

# Prepare Val

base_type = float


def comparison_op(x: base_type, y: base_type) -> bool:
    return x <= y


zero = 0


def bin_op(x: base_type, y: base_type) -> base_type:
    return x + y


Val = PointedPreorderSemiGroup(base_type, comparison_op, zero, bin_op)


def const_H(x: State) -> Ctrl:
    return H


def const_L(x: State) -> Ctrl:
    return L


Policy = Callable[[State], Ctrl]
PolicySeq = List[Policy]


def nexts(t: int, x: State, y: Ctrl) -> SimpleProb(State):

    if y == L:

        if x == G:

            xps = [(G, 1)]

        elif x == B:

            xps = [(G, 0.2), (B, 0.8)]

        else:

            raise ValueError('problem with state value in nexts function.')

    elif y == H:

        if x == G:

            xps = [(B, 0.6), (G, 0.4)]

        elif x == B:

            xps = [(B, 1.0)]

        else:

            raise ValueError('problem with state value in nexts function.')

    else:

        raise ValueError('problem with control value in nexts function.')

    return xps


def reward(t: int, x: State, y: Ctrl, xp: State) -> Val.base_type:

    if xp == G:

        rew = 3

    elif xp == B:

        rew = 1

    else:

        raise ValueError('problem in reward function.')

    return rew


# FCSDP instance
sdp = FiniteControlSequentialDecisionProblem(genSimpleProbMonad, State, Ctrl, nexts,\
                                Val, expected_value, reward, to_iterable_LH())



# Testing

#print('val(0, x0, bi(0, 9)) = ', sdp.val(0, x0, sdp.backwards_induction(0, 9)))

print('val(0, x0, bi(0, 2)) = ', sdp.val(0, x0, sdp.backwards_induction(0, 2)))

print('sdp.val(0, x0, [constH,constL]) = ', sdp.val(0, x0, [const_H, const_L]))

print('sdp.valp(0, x0, [constH,constL]) = ', sdp.valp(0, x0, [const_H, const_L]))

print('sdp.trj(0, x0, [constH,constL]) = ', sdp.trj(0, [const_H, const_L], x0))
