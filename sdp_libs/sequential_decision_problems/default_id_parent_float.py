#!/usr/bin/env python
# coding: utf-8

# A deterministic default version of the framework with `Val = float`


# imports

import logging

## project
from sdp_libs.category_theory.instances import idMonad, idFunctor, IdFun
from sdp_libs.sequential_decision_problems.core_theory_dbc_parent import *


# Prepare Val

base_type = float
zero = 0


def comparison_op(x: base_type, y: base_type) -> bool:
    return x <= y


def bin_op(x: base_type, y: base_type) -> base_type:
    return x + y


Val = PointedPreorderSemiGroup(base_type, comparison_op, zero, bin_op)


class ID_SDP(FiniteControlSequentialDecisionProblem):

    def __init__(self, State, Ctrl, all_ctrls):
        super().__init__(idMonad, State, Ctrl, Val, all_ctrls)

    def measure(self, mv: IdFun(Val)) -> Val:
        return mv

    def get_final_states(self, id_traj: IdFun('StateCtrlSeq')) -> List['State']:
        """Get final states of trajectories."""
        list_of_state_lists = [self.map_scs(lambda xy: fst(xy), tr) for tr in id_traj]
        return [xs[-1] for xs in list_of_state_lists]

    def get_control_seqs(self, id_traj: IdFun('StateCtrlSeq')) -> List['Ctrl']:
        """Get control sequences of trajectories."""
        list_of_ctrl_lists = [self.map_scs(lambda xy: snd(xy), tr) for tr in id_traj]
        return [l[:-1] for l in list_of_ctrl_lists]

    def log_results(self, value: float, traj: IdFun('StateCtrlSeq'), filename: str) -> None:
        """Saves the result of a computation to a log file.
        *Remark*: This is just a very preliminary way to save results."""
        logging.basicConfig(filename='log/' + filename + '.log', level=logging.INFO)
        logging.info("\n\tExpected value:" + str(value) + "\n\t" + str(traj))


