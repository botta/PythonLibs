# Python References

*Disclaimer*: This list is by no means meant to be exhaustive, but rather as a list of pointers to where one might start reading.
  (Patrik: my emacs does not show any of these four "characters": 📙, 📗, 📘, 📕)

Some tentative classification of the references:

* 📙 : Introductory
* 📗 : Intermediate
* 📘 : In depth
* 📕 : Links to full texts/tutorials/documentation

## Code Style

* 📙 Hitchhiker's Guide: [Code Style](https://docs.python-guide.org/writing/style/#code-style)
* 📘 [PEP 8 - Style Guide for Python Code](https://www.python.org/dev/peps/pep-0008)


## Object-orientation

* 📙 RealPython on Classes: [Object-Oriented Programming](https://realpython.com/python3-object-oriented-programming/)
* 📘 Python Documentation Tutorial: [Classes](https://docs.python.org/3/tutorial/classes.html)
* 📙 RealPython on Data Classes: (new in Python 3.7) [Guide to Data Classes](https://realpython.com/python-data-classes/)
* 📘 Python Documentation: [Data Classes](https://docs.python.org/3/library/dataclasses.html?highlight=data%20classes#)
* 📗 RealPython: [Meta Classes](https://realpython.com/python-metaclasses/)
* 📘 Python Documentation: [MetaClasses](https://docs.python.org/3/reference/datamodel.html#metaclasses)
* 📘 [PEP 3115 -- Metaclasses in Python 3000](https://www.python.org/dev/peps/pep-3115/)


## Typing in Python

* 📙 Tutorial: [How to Use Static Type Checking](https://medium.com/@ageitgey/learn-how-to-use-static-type-checking-in-python-3-6-in-10-minutes-12c86d72677b)
* 📗 RealPython: [Python Type Checking](https://realpython.com/python-type-checking/)
* 📘 [PEP 484 -- Type Hints](https://www.python.org/dev/peps/pep-0484/)
* 📘 [PEP 526 -- Syntax for Variable Annotations](https://www.python.org/dev/peps/pep-0526/)
* 📘 Python Documentation: [typing — Support for type hints](https://docs.python.org/3/library/typing.html?highlight=typing#)
* 📘 Static Type Checker for Python: [mypy](http://mypy-lang.org/)
* **TODO**: Cezar mentioned some book about use of static typing in Python?

## Documentation

* 📙 Quick reference: "Step 5" of [Michael Dunn: Getting Started with Sphinx / Autodoc](https://medium.com/@eikonomega/getting-started-with-sphinx-autodoc-part-1-2cebbbca5365)
* 📙 Hitchiker's Guide on [documentation](https://docs.python-guide.org/writing/documentation/#project-documentation)
* 📙 [NumPy Style Example](https://sphinxcontrib-napoleon.readthedocs.io/en/latest/example_numpy.html)
* 📗 PyCon [Spinx-tutorial by Brandon Rhodes](https://buildmedia.readthedocs.org/media/pdf/brandons-sphinx-tutorial/latest/brandons-sphinx-tutorial.pdf)
* 📗 statemachine.py: [reStructuredText example](https://docutils.sourceforge.io/docutils/statemachine.py)
* 📗 [reStructuredText cheatsheet](https://docutils.sourceforge.io/docs/user/rst/cheatsheet.txt)
* 📕 📘 Full [Sphinx documentation](https://www.sphinx-doc.org)
* 📕 📘 [reStructuredText documentation](https://docutils.sourceforge.io/rst.html)


## Project Structure

* 📙 [Sample Module Repository](https://github.com/navdeep-G/samplemod)
* 📙 Tutorial: [Project structure](https://dev.to/codemouse92/dead-simple-python-project-structure-and-imports-38c6)
* 📗 Hitchiker's Guide on [Project Structure](https://docs.python-guide.org/writing/structure/)

## General "Best Practice"
* 📙 [PEP 20 -- The Zen of Python](https://www.python.org/dev/peps/pep-0020/)
* 📙 ["The Best of the Best Practices Guide for Python"](https://gist.github.com/sloria/7001839)
* 📕 📗 [The Hitchhiker's Guide to Python](https://docs.python-guide.org/)

## Official Python Sources
[Python Website](https://www.python.org/)
* 📕 📗 [Tutorial](https://docs.python.org/3/tutorial/index.html)
* 📕 📘 [Standard Library](https://docs.python.org/3/library/index.html)
* 📕 📘 [Language Reference](https://docs.python.org/3/reference/index.html)
* 📕 📘 [Full Documentation](https://docs.python.org/3/index.html)
* 📕 📘 [PEP List](https://www.python.org/dev/peps/) (*Python Enhancement Proposals*)

## Other
* 📕 Tutorial Website: [RealPython](https://realpython.com/)
* 📕 Online Book: [Think Python](http://greenteapress.com/thinkpython/html/index.html)
* 📕 📙 Thorsten Altenkirch's Python book [Conceptual Programming with Python](http://www.cs.nott.ac.uk/~pszit/cp.html):
  > "It is not mainly a Python course but we use Python as a vehicle to teach basic programming concepts. Hence, the words conceptual programming in the title."
