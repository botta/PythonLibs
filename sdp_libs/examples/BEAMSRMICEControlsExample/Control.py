# TODO: - Harmonize types with AbstractControlClass
#       - Controls parametrized by time step
#       - Improve docstrings

# imports

from pprint import PrettyPrinter

## project

from sdp_libs.sequential_decision_problems.core_theory_dbc_parent import AbstractControlClass
from sdp_libs.sequential_decision_problems.default_simple_prob_parent import *


# -------------------------------------------------------------------------------------------------------------------- #

# data CO2ControlT = CO2down | CO2maintain | CO2up

CO2ControlT = int
co2_down: CO2ControlT = 0
co2_maintain: CO2ControlT = 1
co2_up: CO2ControlT = 2

# data SO2ControlT = SO2none | SO2low | SO2high

SO2ControlT = int
so2_none: SO2ControlT = 0
so2_low: SO2ControlT = 1
so2_high: SO2ControlT = 2

ControlT = Tuple[CO2ControlT, SO2ControlT]

# list_of_ctrls: List[ControlT] = [(i, j) for i in range(3) for j in range(3)]

list_of_ctrls : List[ControlT] = [(y0, y1) for y0 in [co2_down, co2_maintain, co2_up]
                                           for y1 in [so2_none, so2_low, so2_high]]

class Ctrl(AbstractControlClass):
    """A Control class needs to implement the methods `getCtrl` and `checkCtrl` from its parent AbstractControlClass.
    It is convenient to also implement the methods `__repr__` and `__str__` for pretty-printing. """

    list_of_ctrls: List[ControlT] = list_of_ctrls

    def __init__(self, y: ControlT = None):
        """Initializes the object with the given control value y,
        if it is a valid control according to checkCtrl.
        If y has the default value ::None:: a dummy instance is created
        that can be used to retrieve the ::list_of_ctrls:: parameter.
        If y is not ::None:: and not in the list of all controls,
        a ValueError is raised."""
        if (y == None) & (len(list_of_ctrls) > 0):
            pass
            # print("List of all controls:")
            # pp = PrettyPrinter(compact=True)
            # pp.pprint([Ctrl(y) for y in self.list_of_ctrls])
        elif self.checkCtrl(y):
            self.y = y
        else:
            self.invalid_control("Not in list of all controls.")

    def invalid_control(self, msg: str) -> None:
        """Raise an error with message ::Control value error: msg::,
        if an invalied control value is encountered."""
        raise ValueError("Control value error: " + msg)

    def __repr__(self):

        y0: str = "init"
        y1: str = "init"

        if self.y[0] == 0:
            y0 = "CO2-"

        elif self.y[0] == 1:
            y0 = "CO2+-"

        elif self.y[0] == 2:
            y0 = "CO2+"

        else:
            self.invalid_control("CO2 component")

        if self.y[1] == 0:
            y1 = "SO2+-"

        elif self.y[1] == 1:
            y1 = "SO2+"

        elif self.y[1] == 2:
            y1 = "SO2++"

        else:
            self.invalid_control("SO2 component")

        return "<Control: (%s, %s)>" % (y0, y1)

    def __str__(self):

        y0: str = "init"
        y1: str = "init"

        if self.y[0] == 0:
            y0 = "CO2-"

        elif self.y[0] == 1:
            y0 = "CO2+-"

        elif self.y[0] == 2:
            y0 = "CO2+"

        else:
            self.invalid_control("CO2 component")

        if self.y[1] == 0:
            y1 = "SO2+-"

        elif self.y[1] == 1:
            y1 = "SO2+"

        elif self.y[1] == 2:
            y1 = "SO2++"

        else:
            self.invalid_control("SO2 component")

        return "<Control: (%s, %s)>" % (y0, y1)

    def checkCtrl(self, y: ControlT) -> bool:
        """Returns `True` if `y` is a valid control and `False` otherwise."""
        if y in self.list_of_ctrls:
            return True
        else:
            return False

    def getCtrl(self) -> ControlT:
        """Returns the control value. """
        return self.y

    def get_control_type(self) -> type:
        return type(self.list_of_ctrls[0])
