#!/usr/bin/env python
# coding: utf-8

# Stripped down example

# imports
from typing import Iterable  # , Literal ~> new in Python 3.8

# pythonLibs imports
from sdp_libs.category_theory.instances import genSimpleProbMonad
from sdp_libs.probability_theory.base import *
from sdp_libs.sequential_decision_problems.naive_theory import *


# Problem specific definitions for small example

# Constants

# Does not work before Python 3.8
"""
LH: type = Literal['L', 'H']

BG: type = Literal['B', 'G']

L: LH = 'L'
H: LH = 'H'

B: BG = 'B'
G: BG = 'G'
"""

# For the time being
LH: type = bool

BG: type = bool

L: LH = True
H: LH = False

B: BG = True
G: BG = False


def to_iterable_LH() -> Iterable:
    return [L, H]


def to_iterable_BG() -> Iterable:
    return [B, G]


# Controls

Ctrl = LH

# States

State = BG

# initial state

x0: State = G

print('initial state x0 = ', x0)

# Prepare Val

base_type = float


def comparison_op(x: base_type, y: base_type) -> bool:
    return x <= y


zero = 0


def bin_op(x: base_type, y: base_type) -> base_type:
    return x + y


Val = PointedPreorderSemiGroup(base_type, comparison_op, zero, bin_op)


def const_H(x: State) -> Ctrl:
    return H


def const_L(x: State) -> Ctrl:
    return L


Policy = Callable[[State], Ctrl]
PolicySeq = List[Policy]


def nexts(t: int, x: State, y: Ctrl) -> SimpleProb(State):

    if y == L:

        if x == G:

            xps = [(G, 1)]

        elif x == B:

            xps = [(G, 0.2), (B, 0.8)]

        else:

            raise ValueError('problem with state value in nexts function.')

    elif y == H:

        if x == G:

            xps = [(B, 0.6), (G, 0.4)]

        elif x == B:

            xps = [(B, 1.0)]

        else:

            raise ValueError('problem with state value in nexts function.')

    else:

        raise ValueError('problem with control value in nexts function.')

    return xps


def reward(t: int, x: State, y: Ctrl, xp: State) -> Val.base_type:

    if xp == G:

        rew = 3

    elif xp == B:

        rew = 1

    else:

        raise ValueError('problem in reward function.')

    return rew


# SDP instance with "dummy optimal_extension"
sdp = SequentialDecisionProblem(genSimpleProbMonad, State, Ctrl, nexts,\
                                Val, expected_value, reward, lambda x: lambda ps: const_H)


# Optimal extensions

def cval(t: int, x: 'State', ps: PolicySeq, y: 'Ctrl'):
    m_map = sdp.monad.m_functor.f_mor
    plus = sdp.val_ord.bin_op

    xpsy = sdp.nexts(t, x, y)
    fy = lambda xp: plus(sdp.reward(t, x, y, xp), sdp.val(t + 1, xp, ps))
    cvy = sdp.measure(m_map(fy)(xpsy))
    return cvy


def gen_optimal_extension(t: int, ps: PolicySeq) -> Policy:

    le = sdp.val_ord.comparison_op

    def cval_argmax(t: int, x: State) -> Ctrl:

        cval_list = [(y, cval(t, x, ps, y)) for y in to_iterable_LH()]

        argmax = fst(cval_list[0])
        v_argmax = snd(cval_list[0])

        for ycv in cval_list:

            if le(v_argmax, snd(ycv)):

                argmax = fst(ycv)

        return argmax

    return lambda x: cval_argmax(t, x)


# SDP instance update

sdp.optimal_extension = gen_optimal_extension

# Testing

print('val(0, x0, bi(0, 9)) = ', sdp.val(0, x0, sdp.backwards_induction(0, 9)))

print('val(0, x0, bi(0, 2)) = ', sdp.val(0, x0, sdp.backwards_induction(0, 2)))

print('sdp.val(0, x0, [constH,constL]) = ', sdp.val(0, x0, [const_H, const_L]))

print('sdp.valp(0, x0, [constH,constL]) = ', sdp.valp(0, x0, [const_H, const_L]))
