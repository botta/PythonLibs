# TODO: - Wrappers for time, carbon and temperature state types
#       - Distinguish between "micro" and "macro" states?
#       - Harmonize methods with AbstractStateClass
#       - Docstrings


# imports

# general

import numpy as np
from functools import reduce
from pprint import PrettyPrinter

# project

from sdp_libs.sequential_decision_problems.core_theory_dbc_parent import AbstractStateClass
from sdp_libs.probability_theory.base import *


# -------------------------------------------------------------------------------------------------------------------- #

TimeT = int
DtTimeT = int

CO2ActT = int
SO2ActT = int

CarbonDtT = float
MatT = float
MupT = float
MloT = float
CarbonTimeListT = np.ndarray  # (with float entries)
CarbonMatListT = np.ndarray  # (with float entries)

TempDtT = float
TatT = float
TloT = float
TempTimeListT = np.ndarray
TempTatListT = np.ndarray

IceVolumeT = float

ClimateSensitivityT = float

X_Time = Tuple[TimeT, DtTimeT]
X_Act = Tuple[CO2ActT, SO2ActT]
X_Carbon = Tuple[CarbonDtT, MatT, MupT, MloT, CarbonTimeListT, CarbonMatListT]
X_Temp = Tuple[TempDtT, TatT, TloT, TempTimeListT, TempTatListT]
X_Ice = IceVolumeT
X_Param = ClimateSensitivityT

X = Tuple[X_Time, X_Act, X_Carbon, X_Temp, X_Ice, X_Param]

# ---------------------------------------------- #
# The base type for the probability distribution #
# ---------------------------------------------- #

class StateBase(AbstractStateClass):

    def __init__(self,
                 time_state: (int, int),
                 action_state: (int, int),
                 carbon_state: (float, float, float, float, np.ndarray, np.ndarray),
                 temp_state: (float, float, float, np.ndarray, np.ndarray),
                 ice_state: float,
                 climate_sensitivity: float):
        """Initializes the object with the given state components t, lh, ua, bg,
        if they form a valid state according to checkState and
        raises a ValueError otherwise."""
        if self.checkState((time_state, action_state, carbon_state, temp_state, ice_state, climate_sensitivity)):
            self.t = time_state[0]
            self.dt = time_state[1]
            self.co2_emission_rate = action_state[0]
            self.so2_injection_rate = action_state[1]
            self.dt1 = carbon_state[0]
            self.Mat = carbon_state[1]
            self.Mup = carbon_state[2]
            self.Mlo = carbon_state[3]
            self.tc_list = carbon_state[4]
            self.Mat_list = carbon_state[5]
            self.dt2 = temp_state[0]
            self.Tat = temp_state[1]
            self.Tlo = temp_state[2]
            self.tt_list = temp_state[3]
            self.Tat_list = temp_state[4]
            self.ice_volume = ice_state
            self.climate_sensitivity = climate_sensitivity  # will be set "properly" in first transition step
        else:
            raise ValueError("Invalid state value!")

    def __copy__(self):
        s = StateBase((self.t, self.dt),
                  (self.co2_emission_rate, self.so2_injection_rate),
                  (self.dt1, self.Mat, self.Mup, self.Mlo, self.tc_list, self.Mat_list),
                  (self.dt2, self.Tat, self.Tlo, self.tt_list, self.Tat_list),
                  self.ice_volume,
                  self.climate_sensitivity)
        return s

    def __repr__(self):  # Tatlist:%s), # Matlist:%s),
        """Pretty printing."""
        return  "\n<State:\t [\n\tTime:\t (t:%i, dt:%i), " \
               "\n\tCO2/SO2: (%i GtC/yr, %i MtS/ yr), " \
               "\n\tCO2:\t (dt1:%f, Mat:%f, Mup:%f, Mlo:%f), " \
               "\n\tTemp:\t (dt2:%f, Tat:%f, Tlo:%f),  " \
               "\n\tIce:\t %f, " \
               "\n\tCS:\t\t %f \t]>" \
               % (self.t, self.dt,
                  self.co2_emission_rate, self.so2_injection_rate,
                  self.dt1, self.Mat, self.Mup, self.Mlo, # str(self.Mat_list),
                  self.dt2, self.Tat, self.Tlo, # str(self.Tat_list),
                  self.ice_volume,
                  self.climate_sensitivity)

    def __str__(self): # Tatlist:%s), # Matlist:%s),
        """Pretty printing."""
        return "\n<State:\t [\n\tTime:\t (t:%i, dt:%i), " \
               "\n\tCO2/SO2: (%i GtC/yr, %i MtS/ yr), " \
               "\n\tCO2:\t (dt1:%f, Mat:%f, Mup:%f, Mlo:%f), " \
               "\n\tTemp:\t (dt2:%f, Tat:%f, Tlo:%f),  " \
               "\n\tIce:\t %f, " \
               "\n\tCS:\t\t %f \t]>" \
               % (self.t, self.dt,
                  self.co2_emission_rate, self.so2_injection_rate,
                  self.dt1, self.Mat, self.Mup, self.Mlo, # str(self.Mat_list),
                  self.dt2, self.Tat, self.Tlo, # str(self.Tat_list),
                  self.ice_volume,
                  self.climate_sensitivity)

    # TODO: Align type information with abstract class
    def checkState(self, x: X) -> bool:
        """Returns `True` if ::time_s::, ::action_s::, ::co2_s::, ::temp_s::, ::ice_s::, ::cs:: define a valid state
        and `False` otherwise.
        Remark: Should be extended."""
        time_s = x[0]
        action_s = x[1]
        co2_s = x[2]
        temp_s = x[3]
        ice_s = x[4]
        cs = x[5]
        if ((type(time_s) == tuple)
            & (type(time_s[0]) == int)
            & (type(time_s[1]) == int)
            & (type(action_s) == tuple)
            & (type(action_s[0]) == int)
            & (type(action_s[1]) == int)
            & (type(co2_s) == tuple)
            & (type(co2_s[0]) == float)
            & ((type(co2_s[1]) == float) | (type(co2_s[1]) == int) | (type(co2_s[1]) == np.float64))
            & ((type(co2_s[2]) == float) | (type(co2_s[2]) == int) | (type(co2_s[2]) == np.float64))
            & ((type(co2_s[3]) == float) | (type(co2_s[3]) == int) | (type(co2_s[3]) == np.float64))
            & (type(co2_s[4]) == np.ndarray)
            & (type(co2_s[5]) == np.ndarray)
            & (type(temp_s) == tuple)
            & ((type(temp_s[0]) == float) | (type(temp_s[0]) == int) | (type(temp_s[0]) == np.float64))
            & ((type(temp_s[1]) == float) | (type(temp_s[1]) == int) | (type(temp_s[1]) == np.float64))
            & ((type(temp_s[2]) == float) | (type(temp_s[2]) == int) | (type(temp_s[2]) == np.float64))
            & (type(temp_s[3]) == np.ndarray)
            & (type(temp_s[4]) == np.ndarray)
            & ((type(ice_s) == float) | (type(ice_s) == int) | (type(ice_s) == np.float64))
            & ((type(cs) == float) | (type(cs) == int) | (type(cs) == np.float64))
            & (int(time_s[1] % co2_s[0]) == 0)
            & (int(time_s[1] % temp_s[0]) == 0)
            # & # TODO!
            & (cs > 0)):
            return True
        else:
            print(type(co2_s[1]))
            return False

    def getState(self):
        """Returns the state value as quintuple. """
        return ((self.t, self.dt),
                (self.co2_emission_rate, self.so2_injection_rate),
                (self.dt1, self.Mat, self.Mup, self.Mlo, self.tc_list, self.Mat_list),
                (self.dt2, self.Tat, self.Tlo, self.tt_list, self.Tat_list),
                self.ice_volume,
                self.climate_sensitivity)

    def get_observable_state(self):
        """Returns the state value as quadruple. """
        return (self.t, self.dt), \
               (self.co2_emission_rate, self.so2_injection_rate), \
               (self.Mat, self.Mup, self.Mlo, self.tc_list, self.Mat_list), \
               (self.Tat, self.Tlo, self.tt_list, self.Tat_list), \
               self.ice_volume

    def set_observable_state(self, time_state, action_state, carbon_state, temp_state, ice_state) -> None:
        self.t = time_state[0]
        self.dt = time_state[1]
        self.co2_emission_rate = action_state[0]
        self.so2_injection_rate = action_state[1]
        self.dt1 = carbon_state[0]
        self.Mat = carbon_state[1]
        self.Mup = carbon_state[2]
        self.Mlo = carbon_state[3]
        self.tc_list = carbon_state[4]
        self.Mat_list = carbon_state[5]
        self.dt2 = temp_state[0]
        self.Tat = temp_state[1]
        self.Tlo = temp_state[2]
        self.tt_list = temp_state[3]
        self.Tat_list = temp_state[4]
        self.ice_volume = ice_state

    def get_time(self) -> int:
        """Returns the current time ::t::. """
        return self.t

    def set_time(self, t: int) -> None:
        self.t = t

    def get_dt(self) -> int:
        """Returns the step size ::dt::. """
        return self.dt

    def set_dt(self, dt: int) -> None:
        self.dt = dt

    def get_co2_emission_rate(self) -> int:
        """Returns the current time ::co2_emission_rate::. """
        return self.co2_emission_rate

    def set_co2_emission_rate(self, rate: int) -> None:
        self.co2_emission_rate = rate

    def get_so2_injection_rate(self) -> int:
        """Returns the current time ::so2_injection_rate::. """
        return self.so2_injection_rate

    def set_so2_injection_rate(self, rate: int) -> None:
        self.so2_injection_rate = rate

    def get_dt1(self) -> float:
        """Returns the carbon step size ::dt1::. """
        return self.dt1

    def set_dt1(self, dt1: float) -> None:
        self.dt1 = dt1

    def get_mat(self) -> float:
        """Returns ::Mat::. """
        return self.Mat

    def set_mat(self, mat: float) -> None:
        self.Mat = mat

    def get_mup(self) -> float:
        """Returns ::Mup::. """
        return self.Mup

    def set_mup(self, mup: float) -> None:
        self.Mup = mup

    def get_mlo(self) -> float:
        """Returns ::Mlo::. """
        return self.Mlo

    def set_mlo(self, mlo: float) -> None:
        self.Mlo = mlo

    def get_tc_list(self) -> np.ndarray:
        """Returns ::tc_list::. """
        return self.tc_list

    def set_tc_list(self, tc_list: np.ndarray) -> None:
        self.tc_list = tc_list

    def get_Mat_list(self) -> np.ndarray:
        """Returns ::Mat_list::. """
        return self.Mat_list

    def set_Mat_list(self, Mat_list: np.ndarray) -> None:
        self.Mat_list = Mat_list

    def get_dt2(self) -> float:
        """Returns the temperature step size ::dt2::. """
        return self.dt2

    def set_dt2(self, dt2: float) -> None:
        self.dt2 = dt2

    def get_tat(self) -> float:
        """Returns ::Tat::. """
        return self.Tat

    def set_tat(self, tat: float) -> None:
        self.Tat = tat

    def get_tlo(self) -> float:
        """Returns ::Tlo::. """
        return self.Tlo

    def set_tlo(self, tlo: float) -> None:
        self.Tlo = tlo

    def get_tt_list(self) -> np.ndarray:
        """Returns ::tt_list::. """
        return self.tt_list

    def set_tt_list(self, tt_list: np.ndarray) -> None:
        self.tt_list = tt_list

    def get_Tat_list(self) -> np.ndarray:
        """Returns ::Tat_list::. """
        return self.Tat_list

    def set_Tat_list(self, Tat_list: np.ndarray) -> None:
        self.Tat_list = Tat_list

    def get_ice_volume(self) -> float:
        """ Returns ::ice_volume::."""
        return self.ice_volume

    def set_ice_volume(self, v: float) -> None:
        self.ice_volume = v

    def get_climate_sensitivity(self) -> float:
        """ Returns::slimate_sensitivity::."""
        return self.climate_sensitivity

    def set_climate_sensitivity(self, cs: float) -> None:
        self.climate_sensitivity = cs


# --------------- #
# The State class #
# --------------- #

StateT = SimpleProb(StateBase)


class State(AbstractStateClass):

    def __init__(self, x_prob: StateT):
        """Initializes the object with the given state components t, lh, ua, bg,
        if they form a valid state according to checkState and
        raises a ValueError otherwise."""
        if self.checkState(x_prob):
            self.prob_state = x_prob
        else:
            raise ValueError("Invalid state value!")

    def __copy__(self) -> StateT:
        return [(s.copy(), pr) for (s, pr) in self.prob_state]

    def __repr__(self):
        """Pretty printing."""
        pp = PrettyPrinter(compact=True)
        return pp.pformat(self.prob_state)

    def __str__(self):
        """Pretty printing."""
        print("\nProbability distribution of base states:")
        print("----------------------------------------\n")
        pp = PrettyPrinter(compact=True)
        return pp.pformat(self.prob_state)

    def checkState(self, x: X) -> bool:
        """Returns `True` if all states contained in prob_state are valid states."""
        check_types = [type(fst(xpr)) == StateBase for xpr in x]
        sum_prob = reduce(lambda p1, p2: p1 + p2, [snd(xpr) for xpr in x])
        sp_condition = np.isclose(sum_prob, 1.0)
        result = reduce(lambda b1, b2: b1 & b2, check_types) & sp_condition
        return result

    def getState(self) -> StateT:
        """Returns the probability distribution of base states. """
        return self.prob_state


# A bit of testing

x_base = StateBase((2000, 10),
                  (8, 0),
                  (0.05, 809, 725, 35642, np.array(2000), np.array(809)),
                  (1, 0.633, 0.084, np.array(2000), np.array(0.633)), 1, 1)

sps = [(x_base, 0.2)] * 5

x0: State = State(sps)

print(x0)
