# imports

from fractions import Fraction
from numbers import Number
from typing import TypeVar

from sdp_libs.utils.base import *

# Type variables

T = TypeVar('T')

# Probability theory

def SimpleProb(T: object) -> object:
    return List[Tuple[T, Number]]


def expected_value(xps: SimpleProb(Number)) -> Number:
    return sum(map(lambda xp: fst(xp) * snd(xp), xps))


def get_elements(sps: SimpleProb(T)) -> List[T]:
    return list(map(lambda sp: fst(sp), sps))


def get_probabilities(sps: SimpleProb(T)) -> List[Number]:
    return list(map(lambda sp: snd(sp), sps))


# Variants

def FastSimpleProbFun(X: type) -> type:
    return List[Tuple[X, float]]


def float_expected_value(xps: SimpleProb(float)) -> float:
    return sum(map(lambda xp: fst(xp) * snd(xp), xps))


def ExactSimpleProbFun(X: type) -> type:
    return List[Tuple[X, Fraction]]


def exact_expected_value(xps: SimpleProb(Fraction)) -> Fraction:
    return sum(map(lambda xp: fst(xp) * snd(xp), xps))


