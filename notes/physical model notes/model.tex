\documentclass[11pt]{scrartcl}
\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage{hyperref}
\usepackage{xcolor}
\usepackage{amsmath}

%opening
\title{Ice sheet, carbon and aerosols models}
\subtitle{the physics involved}
\author{}

\begin{document}

\maketitle
\section{Main idea}
The end goal is to study optimal policies with uncertainties in the context Greenland's tipping point due to anthropogenic forcing. We want to explore the possibility of aerosol emission together with CO2 abatement as controls in the decision problem. Here we discuss the physical models that we are relying on.

\section{BEAM+}
We call BEAM+ the model dealing with the carbon cycle and the climate model which includes the radiative forcing effect of both greenhouse effect due to CO2 emissions and aerosol injections. BEAM+ is our minor modification to the BEAM model; see Ref.~\cite{Glotter2014} for the BEAM model.
\subsection{Characteristics}
BEAM carbon cycle captures correctly timescales that we are interested in if we are going to care about ice sheets ($\sim 1000s$ yrs). During the decision period, ($\sim 100$ yrs) the processes of the carbon cycle involving the deep ocean dynamics, are sub-leading and could be ignored.

\paragraph{IMPORTANT: } The DICE carbon cycle and the carbon cycle of Ref.~\cite{Joos2013} which is used in Ref.~\cite{Helwegen2019} capture well timescales of $\sim 100$ yrs. However, the discrepancies are very big for the longer timescales relevant for ice sheet dynamics. In both of those models CO2 gets absorbed much faster than it should.\\

BEAM includes ocean chemistry which makes the model robust against different emission scenarios. It does not include rock weathering, and the results start to deviate from reality at $\sim 10$ kyears. It captures correctly the decadal, centennial and millennial timescales which are the relevant ones for our problem.\\

Because BEAM tracks ocean chemistry, it allows us to measure other damages related to the emission of CO2 that are not compensated by the injection of aerosols.\\

BEAM model does not directly include evolution and forcing effect of aerosols and this is exactly one of the modifications that we have done. Because aerosols last little in the atmosphere we assume that they don't accumulate, and that the only aerosols at a given timestep are the ones being injected at that timestep. This means that the injection rate enters directly the total radiative forcing. We use equation $(1)$ in Ref.~\cite{Helwegen2019} to model this behaviour. That reference is a good place to find what is a reasonable aerosol emission scenario and how much forcing can we expect from aerosols.

\subsection{Carbon cycle equations}
The carbon cycle model consists of 3 coupled ordinary differential equations for the quantities
\begin{itemize}
	\item $M_{AT}[t]$, carbon mass in atmosphere,
	\item $M_{UP}[t]$, carbon mass in ocean upper layer,
	\item $M_{LO}[t]$, carbon mass in ocean lower layer,
%	\item $T_{AT}[t]$, temperature of atmosphere,
%	\item $T_{OC}[t]$, temperature of ocean.
\end{itemize}

The equations given in Ref.\cite{Glotter2014} are
\begin{subequations}
	\begin{eqnarray}
	\frac{dM_{AT}[t]}{dt}&=&E[t] - k_a \Big(M_{AT}[t] - A\,B\,M_{UP}[t]\Big) \\
	\frac{dM_{UP}[t]}{dt}&=& k_a \Big(M_{AT}[t] - A\,B\,M_{UP}[t]\Big) - k_d \left( M_{UP}[t] - \frac{M_{LO}[t]}{\delta}\right) \\
	\frac{dM_{LO}[t]}{dt}&=& k_d \left( M_{UP}[t] - \frac{M_{LO}[t]}{\delta}\right),
	\end{eqnarray}
	\label{eq:beam}
\end{subequations}
where $k_a, A, k_d, \delta$ are constant parameters, $E[t]$ is the carbon emission rate and $1/B$ is the ``carbon storage factor'' of the ocean which is related to the ocean chemistry and therefore to $M_{UP}[t]$. Notice that \textbf{$B$ is not a constant} and this is what makes BEAM's carbon cycle different to DICE's. In Ref.~\cite{Glotter2014}, $B$ is explained to be solved for at every timestep by assuming a constant alkaninity, $Q$, and solving the system of equations:
\begin{subequations}
	\begin{eqnarray}
	\frac{1}{B} &=& 1 + \frac{k_1}{[H^+]} + \frac{k_2}{[H^+]^2},\\
	Q &=& \left(\frac{k_1}{[H^+]} + \frac{2k_1 k_2}{[H^+]^2}\right)M_{UP}[t]\, B,
	\end{eqnarray}
	\label{eq:alkalinity}
\end{subequations}
where $k_1$, $k_2$ and $Q$ are constants, and $[H^+]$ is the ion concentration in the ocean\footnote{$[H^+]$ relates to the ocean $pH$ through the expression $pH = -\log_{10}([H^+])$. And yes, it is unfortunate that the squared parenthesis indicate concentration in this case while in the rest of the note I am using them to indicate what a function depends on.} (which is \textbf{also not a constant}). 

In our BEAM+ model we have opted for solving analytically for the dependence of $B$ on $M_{UP}[t]$, hence making the non-linearity of the equations \eqref{eq:beam} (which are non-linear in $M_{UP}[t]$) explicit. Since we have two equations in \eqref{eq:alkalinity}, and two unknowns ($B$ and $[H^+]$) we can solve the system of equations to obtain $B$ and $[H^+]$ in terms of $M_{UP}[t]$: 
\begin{subequations}
	\begin{align}
	B[M_{UP}[t]] = & \frac{\sqrt{k_1 \left(Q^2 (k_1-4 k_2)-2 Q M_{UP}[t] (k_1-4 k_2)+k_1 M_{UP}[t]^2\right)}}{2 M_{UP}[t] (k_1-4 k_2)}\\ 
	 & -\, \frac{Q k_1+4 Q k_2+k_1 M_{UP}[t]-8 k_2 M_{UP}[t]}{2 M_{UP}[t] (k_1-4 k_2)},\notag\\
	[H^+][M_{UP}[t]]  = & \frac{\sqrt{(Q k_1-k_1 M_{UP}[t])^2-4 Q k_1 k_2 (Q-2 M_{UP}[t])}}{2 Q}\\
	& + \frac{-Q k_1+k_1 M_{UP}[t]}{2 Q}\notag.
	\end{align}
	\label{eq:BandH+}
\end{subequations}

So, actually, the equations that we are solving for modeling the carbon cycle are
\begin{subequations}
	\begin{eqnarray}
	\frac{dM_{AT}[t]}{dt}&=&E[t] - k_a \Big(M_{AT}[t] - A\,B[M_{UP}[t]]\,M_{UP}[t]\Big) \\
	\frac{dM_{UP}[t]}{dt}&=& k_a \Big(M_{AT}[t] - A\,B[M_{UP}[t]]\,M_{UP}[t]\Big) - k_d \left( M_{UP}[t] - \frac{M_{LO}[t]}{\delta}\right) \\
	\frac{dM_{LO}[t]}{dt}&=& k_d \left( M_{UP}[t] - \frac{M_{LO}[t]}{\delta}\right),
	\end{eqnarray}
	\label{eq:OurBeam}
\end{subequations}
with $B[M_{UP}[t]]$ as indicated in \eqref{eq:BandH+}. The values of the parameters $k_a, k_d, A, \delta, k_1, k_2, Q$\footnote{What I am calling $Q$ in this notes is called $Alk$ in Ref. \cite{Glotter2014}.} can be found in table 3 and equation (11) of Ref. \cite{Glotter2014}.

\subsection{Temperature equations}
The second system of ordinary differential equations are the ones that model the evolution of the upper and lower ocean temperature anomalies, $\delta T_{UP}[t]$ and $\delta T_{LO}[t]$ respectively. The upper ocean layer is assumed to be in equilibrium with the atmosphere.\\\\
\color{gray}
The BEAM paper, calls $\delta T_{UP}[t] \to T_{AT}$ and $\delta T_{LO}[t] \to T_{LO}$. They use the temperature equations of DICE and, as DICE, only give a discretised version of the equations:
\begin{subequations}
\begin{eqnarray}
	T_{AT}[t] &=& T_{AT}[t-1]\\ 
	& &+\, \mu_{AT} \Big(\Lambda \left(T_{eq}[t] - T_{AT}[t-1]\right)- \gamma\left( T_{AT}[t-1] - T_{OC}[t-1]\right) \Big)\notag \\
    T_{OC}[t] &=& T_{OC}[t-1] + \mu_{OC}\, \gamma\left( T_{AT}[t-1] - T_{OC}[t-1]\right),
\end{eqnarray}
	\label{eq:tempbeam}
\end{subequations}
with the second term in the right hand side of both equations being multiplied by $dt = 1$yr (which authors decided to omit) and
\begin{equation}
T_{eq}[t] = \frac{F[t]}{\Lambda} = \frac{\alpha}{\Lambda} \log_2\left(\frac{M_{AT}[t]}{M_{AT}(t = PreInd)}\right).
\end{equation}
Looking at the parameters' claimed units

\begin{center}
	\begin{tabular}[b]{ l | c | c | c | c }
		\hline
		Parameter & $\mu_{AT}$ & $\mu_{OC}$ & $\Lambda$ & $\gamma$ \\ \hline
		Units & 1/year & 1/year  & $W/(m^2 C^o)$ & $W/(m^2 C^o)$ \\ 
		\hline
	\end{tabular}
\end{center}
it is apparent that there is a typo (probably in the units of the $\mu$s). I went to the DICE papers and the original one (1999) has also wrong units, the one from (2013) has no units at all. This makes me feel uneasy so we are \textbf{not} going to use equations \eqref{eq:tempbeam}.\\
\color{black}
We will get inspired in the 2-box models of references \cite{Gregory2000,Held2010} and solve the following system
\begin{subequations}
	\begin{eqnarray}
	c_{vol}\, h_{UP} \frac{d \delta T_{UP}[t]}{dt} &=&  \beta \left(\frac{F[M_{AT}[t],I[t]]}{\beta} - \delta T_{AT}[t]\right) - \gamma\, \Big(\delta T_{UP}[t] - \delta T_{LO}[t]\Big),\\
	c_{vol}\, h_{LO} \frac{d\delta T_{LO}[t]}{dt} &=&  \gamma\, \Big(\delta T_{UP}[t] - \delta T_{LO}[t]\Big),
	\end{eqnarray}
	\label{eq:temp}
\end{subequations}
where $c_{vol}$ is the seawater's volumetric heat capacity\footnote{$c_{vol}$ is obtained by multiplying the specific heat capacity of seawater's and the density of seawater. It is the energy that you need to add, per volume of seawater, to raise the temperature of seawater.}, $h_{UP},\, h_{LO}$ are the thickness of the two ocean layers, $\beta$ is related to the climate sensitivity and $\gamma$ is a thermal conductivity. Remember that $\delta T_{UP}[t], \delta T_{LO}[t]$ are the temperature anomalies and not the absolute temperatures and that the atmosphere will be assumed to be in thermal equilibrium with the upper ocean layer\footnote{See Sec.\ref{sec:derivationTemp} for a rough derivation of equations \eqref{eq:temp}.}. The anthropogenic forcing responsible for the temperature anomalies, $F[M_{AT}[t],I[t]]$, will be computed in terms of the carbon mass in the atmosphere ($M_{AT}$ from the carbon cycle) and the aerosol injection rate ($I[t]$):
\begin{equation}
F[M_{AT}[t],I[t]] = F_{2X} \log_2\left[\frac{M_{AT}[t]}{M_{AT}[t = PreInd]}\right] - \eta\,\alpha_{SO2}\exp\left[-(\beta_{SO2}/I[t])^{\gamma_{SO2}}\right],
\label{eq:forcing}
\end{equation}
where $F_{2X} = 3.7\, W/(m^2 \cdot 2XCO_2)$ is the radiative forcing corresponding to doubling the CO2 concentration and the second term, corresponding to the negative radiative forcing due to aerosols, has been taken from Ref. \cite{Helwegen2019} where you can find that
\begin{equation}
\eta = 0.742,\quad \alpha_{SO2} = 65\,W/m^2,\quad \beta_{SO2} = 2246\, MtS/yr,\quad \gamma_{SO2} = 0.23.
\end{equation}\\\\
The system's equilibrium (formally only obtainable for a constant forcing $F[M_{AT}[t],I[t]] = F_c$) is given by
\begin{equation}
\delta T_{UP eq} =\delta  T_{LO eq} = \frac{F_c}{\beta}
\end{equation}
and hence, $\beta$ is related to the equilibrium climate sensitivity ($ECS$) in that $1/\beta$ is the amount of warming we expect to get per additional amount of radiative forcing. More concretely, in this model: $ECS = F_{2X}/\beta$, where $F_{2X}$ is the radiative forcing corresponding to doubling the CO2 concentration.

Notice that if we where to approximate the lower ocean layer as an infinite heat reservoir $h_{LO}\to\infty$, which is a zeroth order  approximation to $h_{LO} \gg h_{UP}$, $T_{LO}$ would always remain unchanged ($\delta T_{LO}=0$), and we would be left with only one equation
\begin{equation}
	c_{vol}\, h_{UP} \, \frac{d\delta T_{UP}[t]}{dt} = \beta\,\left(\frac{F_c}{\beta} -\delta  T_{UP}[t]\right) - \gamma\, \delta T_{UP}[t],
\end{equation}
whose equilibrium (again for a constant forcing $F[t] = F_c$) would in this case be
\begin{equation}
\delta T_{UP eq Fast} = \frac{F_c}{\beta + \gamma}.
\end{equation}
The quantity $1/(\beta + \gamma)$ (when measured in $^oC/2XCO_2$) is usually referred to Transient Climate Response ($TCR$). The $TCR$ refers to the amount of warming we can expect to get per doubling of CO2 concentration on shorter timescales of around 70 years. Notice however that $1/(\beta + \gamma)$ is not exactly the $TCR$ since it has other units $^oC/(W/m^2)$.\\

Some IPCC values:
\begin{itemize}
	\item $ECS = [1.5 - 4.5]\, (^oC/2XCO_2)$
	\item $TCR = [1. - 2.5]\, (^oC/2XCO_2)$
\end{itemize}
From which we can get that
\begin{itemize}
	\item $\beta = F_{2X}/ECS = [2.5 - 0.8]\,(W/(m^2 \cdot ^oC))$
	\item $\gamma = (F_{2X}/TCR) - \beta = [1.2 - 0.6]\,(W/(m^2 \cdot ^oC)) $
\end{itemize}

\paragraph{Clarification for sensitivity analysis of ECS:}If we want to do a sensitivity analysis for Climate Sensitivity ($ECS$), we can rewrite $\beta$ in terms of $ECS$ in our code. Notice, however, that $\gamma$ should remain constant because thermal conductivity across ocean layers should not change because the $ECS$ changes. Instead, the $TCR$ will change accordingly so that $\gamma$ remains constant.\\

The values (or ranges) for the parameters in eqs \eqref{eq:temp} can be found in Table \ref{table:params}.

\begin{table}[h]
	\begin{center}
	\begin{tabular}[b]{ c | c }
		Parameter & Value(s) \\
		\hline
		$\beta$ & $[2.5 - 0.8]\,W/(m^2 \cdot ^oC),\quad ECS=3^oC/2XCO_2 \to\beta=1.23\,W/(m^2 \cdot ^oC)$ \\
		\hline
		$\gamma$  & $0.8\, W/(m^2\cdot ^oC)$ \\
		\hline
		$h_{UP}$ & $\sim 150\, m$ \\
		\hline
		$h_{LO}$  & $\sim 2400\, m$ \\
		\hline
		$c_{vol}$ & $0.13\, W\cdot yr / (m^3\cdot ^oC)$\\
		\hline
	\end{tabular}	
	\end{center}
	\caption{Values of parameters we will use in the temperature model.}
	\label{table:params}
\end{table}

\subsubsection{Rough derivation of temperature equations}\label{sec:derivationTemp}
The equations for the temperature anomalies \eqref{eq:temp} can seem a bit mysterious. In this section we attempt to explain their origin. To lay the ground, let's start by considering the usual textbook example for the energy balance of a planet with greenhouse effect\footnote{This is accomplished by considering that the planet has two layers. The outer layer, is transparent to UV radiation and opaque to IR radiation, while the lower one is opaque to all radiation.}. 

Consider that, of all incoming solar radiation $(S_0 \pi r_\oplus^2)$, a fraction $\alpha$ gets reflected and the fraction $(1-\alpha)$ passes through the Top Of Atmosphere layer. All that energy gets absorbed by all the other planet components: lower atmosphere, surface, and oceans. The heated up Planet's lower atmosphere, surface, and oceans (which, for the purposes of this energy balance are assumed to be in thermal equilibrium at temperature $\bar{T}_{PL}$) radiate blackbody long wavelength radiation, $\sigma T^4 4\pi r_\oplus^2$. This infrared radiation heats up the Top Of Atmosphere layer, which in turn also emits blackbody long wavelength radiation at temperature $\bar{T}_{TOA}$.
The energy balance looks like
\begin{align}
	0 =& + (1-\alpha)S_0 \pi r_\oplus^2 - \sigma\, \bar{T}_{PL}^4 4\pi r_\oplus^2 + \sigma\, \bar{T}_{TOA}^4 4\pi r_\oplus^2,\label{eq:balance1}\\
	0 =& \,\sigma\, \bar{T}_{PL}^4 4\pi r_\oplus^2 - 2 \sigma\, \bar{T}_{TOA}^4 4\pi r_\oplus^2.\label{eq:balance2}
\end{align}
Where $(1-\alpha)S_0$ is the incoming solar radiation energy flux per unit area. This radiation hits the planet perpendicularly to its sun facing face, this is why the term is multiplied by the absorption cross section of the planet, $\pi r_\oplus^2$. The multiplied term gives the incoming solar energy flux. $\sigma\bar{T}_{PL}^4$ is the energy flux per unit area of a black body radiating with temperature $\bar{T}_{PL}$. IR radiation is emitted from all of the planet's surface, hence the factor $4\pi r_\oplus^2$.

We can devide equations \eqref{eq:balance1} and \eqref{eq:balance2} by the planet's surface area, and deal with energy fluxes per unit area instead of total energy fluxes,

\begin{align}
0 =& + \frac{(1-\alpha)S_0}{4} - \sigma\, \bar{T}_{PL}^4 + \sigma\, \bar{T}_{TOA}^4 ,\\
0 =& \,\sigma\, \bar{T}_{PL}^4 - 2 \sigma\, \bar{T}_{TOA}^4 .
\end{align}

Notice that we can use the second equation to rewrite the first one in terms of $\bar{T}_{PL}$
\begin{equation}
	0 = + \frac{(1-\alpha)S_0}{4} - \frac{\sigma\, \bar{T}_{PL}^4}{2}.
	\label{eq:balancedensity}
\end{equation}
Equation \eqref{eq:balancedensity} is, hence, an effective balance equation for the Planet layer that already accounts for the natural greenhouse effect.
Solving the energy balance equations gives 
\begin{equation}
\bar{T}_{TOA} = \sqrt[4]{\frac{S_0(1-\alpha)}{4\sigma}} = 255K ,\quad \bar{T}_{PL} = \sqrt[4]{\frac{S_0(1-\alpha)}{2\sigma}} = 303K,
\label{eq:equilibrium_temps}
\end{equation}
which is a bit high of a value for $\bar{T}_{PL}$, it should be closer to $\sim 288K$, but acceptable given all the simplifications that have been made.\\\\
Now we will consider that due to an anthropogenic perturbation in the radiative balance, $\epsilon\, F[t]$, the temperature of the atmosphere (which we will is still assume to be in thermal equilibrium with the upper ocean layer) gets perturbed from its original value $T_{AT+UP}[t] = \bar{T}_{AT+UP} + \epsilon\, \delta T_{AT+UP}[t]$, which in turn perturbs the lower ocean temperature, $T_{LO}[t] = \bar{T}_{LO} + \epsilon\, \delta T_{LO}[t]$ and the Top of Atmosphere\footnote{Recall that we are assuming that in the absence of anthropogenic forcing, the oceans and lower atmosphere layer are in equilibrium: $\bar{T}_{LO} = \bar{T}_{UP} =  \bar{T}_{AT} = \bar{T}_{PL}$ while the Top Of Atmosphere has a temperature $\bar{T}_{TOA}$, see eq. \eqref{eq:equilibrium_temps}.}. In this case:
\begin{subequations}
	\begin{eqnarray}
	c_{vol\,air}\, h_{TOA}\, \frac{dT_{TOA}[t]}{dt} &=&  \,\sigma\, \bar{T}_{AT+UP}[t]^4 - 2 \sigma\, \bar{T}_{TOA}[t]^4, \\
	\left(c_{vol\,air}\, h_{AT} + c_{vol\,sea}\, h_{UP}\right)\, \frac{dT_{AT+UP}[t]}{dt} &=&  + \frac{(1-\alpha)S_0}{4} - \sigma\, T_{AT+UP}[t]^4\\ & & + \sigma\, T_{TOA}[t]^4 + \epsilon\, F[t]\notag\\& &  - \gamma\,\Big(T_{AT+UP}[t] - T_{LO}[t]\Big),\notag\\
	c_{vol\, sea}\, h_{LO}\, \frac{dT_{LO}[t]}{dt} &=&  \gamma\,\Big(T_{AT+UP}[t] - T_{LO}[t]\Big).
	\end{eqnarray}
\end{subequations}
Now, we notice that the volumetric heat capacity of air $\sim 4\cdot10^{-5} W\, yr / (^oC\, m^3)$ is much smaller than the volumetric heat capacity of seawater $\sim 1\cdot10^{-1} W\, yr / (^oC\, m^3)$. Also, the differences in the layer thickness will be at most of two orders of magnitude. Because of this we will make two simplifications:
\begin{enumerate}
	\item The dynamics of the Top Of Atmosphere layer $(TOA)$ are so much faster than the ones of the mixed layer $(AT+UP)$ and than the lower ocean layer $(LO)$, that we will assume that it is always following closely its attractor. We will hence ignore the ODE for $T_{TOA}[t]$ and assume $T_{TOA}[t] = T_{AT+UP}[t]/2^{(1/4)}$.
	\item We will assume, due to $c_{vol\,air}\, h_{AT} \ll c_{vol\,sea}\, h_{UP}$, that the ocean's upper layer is responsible for storing all the heat content of the mixed layer. 
\end{enumerate} 
After these simplifications, and also making the change $T_{AT+UP}\to T_{UP}$  to lighten the notation, we have 
\begin{subequations}
	\begin{eqnarray}
	c_{vol\,sea}\, h_{UP}\, \frac{dT_{UP}[t]}{dt} &=&  + \frac{(1-\alpha)S_0}{4} - \sigma\, \frac{T_{UP}[t]^4}{2} + \epsilon\, F[t]\\& &  - \gamma\,\Big(T_{UP}[t] - T_{LO}[t]\Big),\notag\\
	c_{vol\, sea}\, h_{LO}\, \frac{dT_{LO}[t]}{dt} &=&  \gamma\,\Big(T_{UP}[t] - T_{LO}[t]\Big).
	\end{eqnarray}
\end{subequations}

Making the substitution 
\begin{equation}
T_{UP}[t] = \bar{T}_{UP} + \epsilon\, \delta T_{UP}[t],\quad
T_{LO}[t] = \bar{T}_{LO} + \epsilon\, \delta T_{LO}[t]
\end{equation}
 and then expanding for small $\epsilon$ one gets
\begin{subequations}
	\begin{eqnarray}
	0 &=& \Bigg(\frac{(1-\alpha)S_0}{4} - \frac{\sigma\, \bar{T}_{UP}^4}{2} + \gamma\,\Big(\bar{T}_{UP} - \bar{T}_{LO}\Big) \Bigg) + \\ &\,& \epsilon\Bigg(-c_{vol}\, h_{UP}\, \frac{d\,\delta T_{UP}[t]}{dt} + F[t] -2\sigma \bar{T}_{UP}^3 \delta T_{UP}[t]\notag \\&\,& - \gamma\,\Big(\delta T_{UP}[t] - \delta T_{LO}[t]\Big) \Bigg)  + \mathcal{O}[\epsilon]^2 ,\notag\\
	0 &=&  \Bigg(-\gamma\,\Big(\bar{T}_{UP} - \bar{T}_{LO}\Big)\Bigg)\\ &\,& + \epsilon\Bigg(-c_{vol}\, h_{LO}\, \frac{d\,\delta T_{LO}[t]}{dt} - \gamma\,\Big(\delta T_{UP}[t] - \delta T_{LO}[t]\Big) \Bigg) + \mathcal{O}[\epsilon]^2.\notag
	\end{eqnarray}
	\label{eq:tempDeriv}
\end{subequations}
Order 0 in $\epsilon$ of these equations give the equilibrium balance, while the first order in $\epsilon$ corresponds to equations \eqref{eq:temp} if we decide that the anthropogenic has the form \eqref{eq:forcing} and we identify $\beta = 2\sigma \bar{T}_{AT}^3$.

\paragraph{NOTE 1} Evaluating $2\sigma \bar{T}_{AT}^3$, either with $\bar{T}_{AT} = 303K$ or $\bar{T}_{AT} = 288K$ gives $\beta = 3.2W/(m^2\,K)$ or $\beta = 2.7W/(m^2\,K)$ respectively; which is slightly higher than the values recommended in references \cite{Gregory2000,Held2010}. 

\paragraph{NOTE 2} This analysis can be amplified to justify the fact that the atmosphere is always at thermal equilibrium with the upper ocean layer. The justification will be due to the fact that $c_{vol\,air}\, h_{AT} \ll c_{vol\,sea}\, h_{UP}$ and hence the atmosphere dynamics are faster than the upper ocean ones. I have not done it here because I was not completely sure on what terms of the energy balance to associate with the individual layers. 

\paragraph{NOTE 3} We are considering that the coupling of the climate (temperature) model, and the carbon cycle model, happens only through the anthropogenic forcing which depends on the carbon mass in the atmosphere. The original BEAM model of Ref.~\cite{Glotter2014} includes the possibility of a temperature back-reaction (or feedback) into the ocean chemistry. If we where to do that, the 5 ODEs \eqref{eq:OurBeam} and \eqref{eq:temp} would need to be solved together. We don't include this feature in our BEAM+ because the authors of Ref.~\cite{Glotter2014} say that the effects are subleading.


\section{Greenland's ice sheet model}
The Double Fold Greenland's Ice Sheet Model consists of a non-linear ordinary differential equation 
\begin{equation}
\label{eq:DF}
\frac{dV[t]}{dt} = \frac{1}{\tau[\text{sign}[dV[t]/dt]]}\left(-V[t]^3 + a\, V[t]^2 + b\, V[t] + c\, T_f[t] + d\right), 
\end{equation} where $V[t]$ represents the volume fraction of present ice $T_f[t]$ is the temperature forcing and the coefficients $a, b, c, d$
\begin{subequations}
	\label{eq:coeffs}
	\begin{align}
	a &= \frac{3(V_{-} + V_{+})}{2},\\
	b &= - 3 V_{-} V_{+},\\
	c &= -\frac{(V_{-} - V_{+})^3}{2(T_{-} - T_{+})},\\
	d &= + \frac{T_{+}V_{-}^2(V_{-} - 3V_{+}) - T_{-}V_{+}^2(V_{+} - 3V_{-})}{2(T_{-} - T_{+})}.  
	\end{align}
\end{subequations}
have been obtained analytically in terms of the bifurcation points of the steady states $T_\pm$, the volume fraction at the bifurcation points, $V_\pm$. We will in general consider that both $T_f$ and $T_\pm$ refer to local temperature anomalies.

We consider that $\tau$ is a function of whether the ice is melting or freezing. We have done this because the most sophisticated models exhibit this asymmetry. We could do more complicated things but for our purposes
\begin{equation}
\tau_\text{melt} \approx 300 \text{ yr}, \quad \tau_\text{freeze} \approx 2000 \text{ yr}.
\end{equation}

\paragraph{A word on the analytic procedure to obtain eqs. \eqref{eq:coeffs}} In order to obtain the coefficients in terms of $T_\pm$ and $V_\pm$ we have solved for the roots the cubic equation at the right hand side of eq. \eqref{eq:DF}, and we have imposed certain properties to the steady state structure. 
\begin{itemize}
	\item For temperatures between $T_-$ and $T_+$ we want that there exists 3 real solutions.
	\item We want that $T_\pm$ are bifurcation points. In terms of the cubic, this means zeros of the discriminant.
	\item We fix the volumes at the bifurcation points such that $T_+ > T_-$ and $V_+ > V_-$. This is what fixes the shape of the inverted S expected for the steady states of Greenland's ice sheet.
\end{itemize}
It would be ideal to also be able to fix the volume fraction at $T_f = 0$ as 1 but there is no more freedom left to do that. We have decided that the values ($T_\pm, V_\pm$) are more useful when comparing to other studies. When we use the model we will have to be careful and choose values ($T_\pm, V_\pm$) that give a steady state with $T_f=0$ very close to 1.


\section{Commitment}
TODO

\bibliography{model.bib}
\bibliographystyle{unsrt}




\end{document}