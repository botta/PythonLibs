# PythonLibs/sdp_libs

## README.md

**Nicola**: we have to provide some explanations about the content of
the directories and how to programs, e.g., in [examples](examples). See, for
instance, [IdrisLibs/README.md](https://gitlab.pik-potsdam.de/botta/IdrisLibs/-/blob/master/README.md)

## directory structure

**Nicola**: why are libraries like [base](base), [algebra](algebra) and
[category_theory](category_theory) beneath this
([sdp_libs](../sdp_libs)) library? Perhaps we can lift them at the same
level of ([sdp_libs](../sdp_libs))?

**Nuria**: Because [sdp_libs](../sdp_libs) basically serves as root for the project - we can change the name; the folder for sequential decision problems is [sequential_decision_problems](sequential_decision_problems). However, everything is very preliminary for the moment anyway!



## Makefile

**Nicola**: `make` yields `Makefile:2: *** missing separator.  Stop.`
error in my setup.

**Nuria**: This is not surprising, the Makefile is just a dummy for the moment, sorry.
The example files can e.g. be run as follows
* open the PythonLibs folder in PyCharm/IntelliJ and then one of the files in the example folder, choose `run (filename)` from the context menu **or**
* start the Python REPL from the terminal with `python3` while in the PythonLibs folder, and then type `import sdp_libs.examples.(filename)`
