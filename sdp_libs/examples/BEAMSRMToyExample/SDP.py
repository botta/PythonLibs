# TODO list:
#  - Add types where they are still missing
#  - Add missing documentation
#  - Decide how to treat `horizon`
#  - Fix/extend plotting
#  - Clean up unused parameters


# Imports

## general
import numpy as np
import matplotlib.pyplot as plt
from scipy import interpolate
from typing import Callable

## project
from sdp_libs.examples.BEAMSRMToyExample.Control import Ctrl
from sdp_libs.examples.BEAMSRMToyExample.State import State
from sdp_libs.sequential_decision_problems.default_simple_prob_parent import SP_SDP


class SDP_BEAM_SRM(SP_SDP):
    """Class for computing an SDP with parameter uncertainty on top of the BEAM+SRM carbon and temperature model.

    Attributes:
    -----------
    Attributes for sequential decision problem

    horizon : int
        terminal time for decision process
    co2_scenario : int
        determines the CO2 ::emissions:: function
        must be one of {0, 1, 2, 3}
    srm_scenario : int
        determines the SO2 ::injections:: function
        must be one of {0, 1, 2, 3, 4}
    State : AbstractStateClass
        class describing the state space
    Ctrl : AbstractControlClass
        class describing the control space
    all_ctrls : List[Ctrl] (possibly adjust)
        list of all possible controls
    (all_states : List[State]   (in case of a finite number of states)
        list of all possible states) -- not yet


    Attributes of Glotter et al. BEAM model and Helwegen et al. SRM model

    * BEAM carbon following Glotter et al. (2015):

        ka : float
            time constant
            default value: 0.2 years^{-1}
            from Bolin and Eriksson (1959)
        kd : float
            time constant
            default value: 0.05 years^{-1}
            from Bolin and Eriksson (1959)
        delta : int
            default value: 50 (dimensionless)
            from Bolin and Eriksson (1959)
        kH : float
            default value: 1.23E3 (dimensionless)
            from Weiss (1974)
        k1 : float
            default value: 8.00E-7 mol/Kg
            from Mehrbach et al (1973)
        k2 : float
            default value: 4.53E-10 mol/Kg
            from Mehrbach et al (1973)
        AM : float
            default value: 1.77E20 mol
            from Warneck (1999)
        OM : float
            default value: 7.8E22 mol
            from Garrison (2009), conversion:
                OM
                ~~ 1.37 · 10^9 km^3 ocean · 10^15 cm^3 / km^3 · 1.027g seawater / cm^3 seawater · 1 mol water / 18 g
                ~~ 7.8 · 10^22 moles
        Alk : float
            default value: 767.0 Gt C
            from Glotter et al. (2015)
        GtCtoppm : float
            default value: 0.47 ppm in atmosphere / Gt C in atmosphere

    * Starting conditions year 2000

        Mat2000 : float
            default value: 809 Gt C
        Mup2000 : float
            default value: 725 Gt C
        Mlo2000 : float
            default value: 35642 Gt C


    * Temperature:

        * BEAM temperature:

            alpha: float
                DICE assumptions of the forcing per doubling of CO_2
                default value: 3.8 Watts / m^2
            gamma: float
                relates atmosphere-ocean heat transfer to temperature anomaly
                default value: 0.3 Watts / (m^2 * Celsius)
            Lambda: float
                climate sensitivity
                default value: 1.3 Watts / (m^2 * Celsius)
                --- if we want to incorporate uncertainty its value,
                this will contained in the unobservable part of the state ---
            muat: float
                default value: 0.022 years^{-1}
            mulo: float
                default value: 1 / 60 years^{-1}
            MatPreInd: float
                default value: 596 Gt C

        * Aerosol following Helwegen et al.:

            eta: float
                default value: 0.742 (dimensionless)
            alphaSO2: float
                default value: 65 Watts / m^2
            betaSO2: float
                default value: 2246 Mt of S / year
            gammaSO2: float
                default value: 0.23 (dimensionless)

        * Starting conditions year 2000

            Tat2000 : float
                default value: 0.633
                Temperature anomaly in year 2000 with respect to pre-indstrial (1750)
            Tlo2000 : float
                default value: 0.084
                Temperature anomaly in year 2000 with respect to pre-indstrial (1750)


    """

    # ---------------- #
    # Setup parameters #
    # ---------------- #

    # fixed parameters
    # ---------------- #

    ka: float        = 0.2              # 1/years
    kd: float        = 0.05             # 1/years
    delta: int       = 50               # dimensionless
    kH: float        = 1.23E3           # dimensionless
    k1: float        = 8.00E-7          # mol/Kg
    k2: float        = 4.53E-10         # mol/Kg
    AM: float        = 1.77E20          # mol
    OM: float        = 7.8E22           # mol
    Alk: float       = 767.0            # Gt C
    GtCtoppm: float  = 0.47             # ppm in atmosphere/ Gt C in atmosphere

    ## Starting conditions year 2000

    Mat2000: float   = 809              # Gt C
    Mup2000: float   = 725              # Gt C
    Mlo2000: float   = 35642            # Gt C

    # parameters entering temperature computation
    # ------------------------------------------- #

    ## carbon parameters

    alpha: float     = 3.8              # Watts / m^2
    gamma: float     = 0.3              # Watts / (m^2 * Celsius)
    # Lambda: float    = 1.3              # Watts / (m^2 * Celsius)
    muat: float      = 0.022            # 1/years
    mulo: float      = 1 / 60           # 1/years
    MatPreInd: float = 596              # Gt C

    ## aerosol parameters (taken from Helwegen2019)

    eta: float        = 0.742            # dimensionless
    alphaSO2: float   = 65               # Watts / m^2
    betaSO2: float    = 2246             # Mt of S / year
    gammaSO2: float   = 0.23             # dimensionless

    ## Starting conditions year 2000

    Tat2000: float   = 0.633            # Celsius
    Tlo2000: float   = 0.084            # Celsius

    # ----------------------------------------------- #
    # #### Initialize the class                       #
    # ----------------------------------------------- #

    State = State
    Ctrl = Ctrl
    all_ctrls = [True]

    def __init__(self, co2_scenario, srm_scenario):
        super().__init__(self.State, self.Ctrl, self.all_ctrls)
        self.emissions = self.set_co2_scenario(co2_scenario)
        self.injections = self.set_srm_scenario(srm_scenario)
        self.co2_scenario = co2_scenario
        self.srm_scenario = srm_scenario

    # ---------------------------------------------------------- #
    # Transition function
    # ---------------------------------------------------------- #

    def nexts(self, t: int, x: State, y: Ctrl) -> 'SimpleProb(State)':
        """An experimental transition function using the carbon cycle step function
            combined with the temperature step function;
            all uncertainty comes from parameter uncertainty about climate sensitivity which is treated in
            the first transition step. All subsequent transitions are deterministic.

            Args
            ----

            t: int
                the current decision step
            x: State
                the current state
            y: Ctrl
                the current control

            Result
            ------

            xps: SimpleProb(State)
                a probability distribution of successor states
            """

        if t == 0:

            tx = x.get_time()

            xp1 = x.__copy__()
            xp2 = x.__copy__()
            xp3 = x.__copy__()
            xp4 = x.__copy__()
            # Just a couple of dummy values...
            xp1.set_climate_sensitivity(1.3)
            xp2.set_climate_sensitivity(2.5)
            xp3.set_climate_sensitivity(4.0)
            xp4.set_climate_sensitivity(6.5)
            # and dummy-probabilities!
            xps = [(xp1, 0.1), (xp2, 0.3), (xp3, 0.5), (xp4, 0.1)]

        elif t > 0:

            tx = x.get_time()
            # print(tx)

            dt = x.get_dt()
            dt1 = x.get_dt1()
            dt2 = x.get_dt2()

            steps_carbon = int(dt / dt1)
            steps_temp = int(dt / dt2)

            # Carbon:
            # -------

            Mat = x.get_mat()
            Mup = x.get_mup()
            Mlo = x.get_mlo()

            tc_list = x.get_tc_list()
            Mat_list = x.get_Mat_list()

            tc = tx
            i = 0

            while i < steps_carbon:
                tc, Mat, Mup, Mlo = self.next_carbon(tc, dt1, Mat, Mup, Mlo)
                tc_list = np.append(tc_list, tc)
                Mat_list = np.append(Mat_list, Mat)
                i += 1

            # print("tc_list:", self.tc_list)
            # print("Mat_list:", self.Matlist)

            Mat_int = interpolate.interp1d(tc_list, Mat_list, kind='cubic')

            # Temperature:
            # ------------

            dt2 = x.get_dt2()
            Tat = x.get_tat()
            Tlo = x.get_tlo()
            Lambda = x.get_climate_sensitivity()

            tt_list = x.get_tt_list()
            Tat_list = x.get_Tat_list()

            tt = tx

            for i in range(steps_temp):
                tt, Tat, Tlo = self.next_temp(tt, dt2, Tat, Tlo, Lambda, Mat_int)
                tt_list = np.append(tt_list, tt)
                Tat_list = np.append(Tat_list, Tat)
                i += 1

            # print("tt_list:", tt_list)
            # print("Tat_list:", Tat_list)
            # Tat_int = interpolate.interp1d(self.tt_list, self.Tatlist, kind='cubic')

            # Prepare next state:
            # -------------------

            xp = x.__copy__()
            xp.set_time(tx + dt)
            xp.set_mat(Mat)
            xp.set_mup(Mup)
            xp.set_mlo(Mlo)
            xp.set_tc_list(tc_list)
            xp.set_Mat_list(Mat_list)
            xp.set_tat(Tat)
            xp.set_tlo(Tlo)
            xp.set_tt_list(tt_list)
            xp.set_Tat_list(Tat_list)
            # print(xp)

            xps = [(xp, 1.0)]

        else:
            raise ValueError("Invalid time.")

        return xps

    def next_carbon(self, t, dt, Mat, Mup, Mlo):
        """Euler integrator for carbon cycle eqs - transistion function for the carbon cycle subsystem."""

        flowATtoUP = self.ka * (Mat -
                                (self.AM * self.kH * (1 + self.delta)
                                 * (- self.Alk * self.k1 + 4 * self.Alk * self.k2 + self.k1 * Mup - 8 * self.k2 * Mup
                                    + np.sqrt(
                                                    self.k1 * (self.Alk ** 2 * (self.k1 - 4 * self.k2)
                                                               - 2 * self.Alk * (
                                                                           self.k1 - 4 * self.k2) * Mup + self.k1 * Mup ** 2))
                                    )
                                 )
                                / (2 * (self.k1 - 4 * self.k2) * self.OM))

        flowUPtoLO = self.kd * (-(Mlo / self.delta) + Mup)

        dMat = self.emissions(t) - flowATtoUP
        dMup = - flowUPtoLO + flowATtoUP
        dMlo = flowUPtoLO

        return t + dt, Mat + dMat * dt, Mup + dMup * dt, Mlo + dMlo * dt

    def next_temp(self, t, dt, Tat, Tlo, Lambda: float, Mat_int):
        """Transition function for the temperature subsystem."""

        flowATtoLO = self.gamma * (Tat - Tlo)
        dTat = self.muat * (Lambda * (self.equilibrium_temperature(t, Lambda, Mat_int) - Tat) - flowATtoLO)
        dTlo = self.mulo * flowATtoLO

        return t + dt, Tat + dTat * dt, Tlo + dTlo * dt


    def reward(self, t: int, x: State, y: Ctrl, xp: State):
        """A dummy reward function."""

        if self.is_terminal(xp):
            # print("terminal!")
            if self.test_commitment(xp):  # for the moment, this case is unreachable
                print("committed!")
                rew = 0

            else:
                rew = 1000 * xp.get_tat()

        else:

            rew = 0

        return rew

    def is_terminal(self, x: State) -> bool:
        """Check whether x is a terminal state."""
        # print("t:", x.get_time(), "h:", self.horizon)
        t = x.get_time()
        h = self.horizon
        return t == h

    # Added parameter :Lambda: for climate sensitivity as part of state
    def equilibrium_temperature(self, t, Lambda, Mat_int):
        """Computes equilibrium temperature ::Teq:: due to carbon and aerosol forcing"""

        if self.injections(t) == 0:  # no SRM case
            return (1 / Lambda) * (self.alpha * np.log2(Mat_int(t) / self.MatPreInd))

        else:  # all other cases
            return (1 / Lambda) * (self.alpha * np.log2(Mat_int(t) / self.MatPreInd) -
                                   self.eta * self.alphaSO2
                                   * np.exp(-np.power(self.betaSO2 / self.injections(t), self.gammaSO2)))


    def test_commitment(self, x: State) -> bool:
        """Placeholder for a potential commitment test."""
        return False


    # --------------------------- #
    # #### CO2 emission scenarios #
    # --------------------------- #

    def set_co2_scenario(self, co2_scenario) -> Callable[[float], float]:
        """
        Sets a CO2 emission scenario according to ::co2_scenario:: where scenarios are encoded as follows:
        * ::co2_scenario = 0:: : constant CO2 emission rate forever, 8GtC/year
        * ::co2_scenario = 1:: : A2+ scenario (the one explained in the paper and in the BEAM notebook)
        * ::co2_scenario = 2:: : increase by 5% every 4 years until 50GtC/yr, then constant. Start with 8GtC/yr at 2000
        * ::co2_scenario = 3:: : decrease by 5% every 4 years. Start with 8GtC/yr at 2000

        :param co2_scenario: int
            Needs to be 0, 1, 2 or 3.
        :return: Callable[[float], float]
            A function that returns the current CO2 emissions at a time ::t: float::.
        """


        if co2_scenario == 0:         # constant CO2 emission rate forever, 8GtC/year
            def emissions(t):
                return 8

        elif co2_scenario == 1:         # A2+ scenario
            def emissions(t):
                a = 75
                if t <= 2100:
                    return 8 * np.exp((t - 2000) / a)
                elif 2100 < t <= 2300:
                    return (-8 * np.exp((2100 - 2000) / a) / 200) * (t - 2100) + 8 * np.exp((2100 - 2000) / a)
                elif t > 2300:
                    return 0

        elif co2_scenario == 2:         # increase by 5% every 4 years untill 50GtC/yr, then constant. Start with 8GtC/yr at 2000
            def emissions(t):
                return min(50, 8 * np.power(105 / 100, max(0, int((t - 2000) / 4))))

        elif co2_scenario == 3:         # decrease by 5% every 4 years. Start with 8GtC/yr at 2000
            def emissions(t):
                return 8 * np.power(95 / 100, max(0, int((t - 2000) / 4)))

        else:
            raise ValueError("CO2scenario has to be 0,1,2 or 3.")

        return lambda t: emissions(t)


    # ---------------------- #
    # #### Aerosol scenarios #
    # ---------------------- #

    def set_srm_scenario(self, srm_scenario) -> Callable[[float], float]:
        """Sets a SO2 injection scenario according to ::srm_scenario:: where scenarios are encoded as follows:
                * ::srm_scenario = 0:: : no aerosols
                * ::srm_scenario = 1:: : SRM only curve
                * ::srm_scenario = 2:: : Abatement+SRM curve
                * ::srm_scenario = 3:: : emitting 75MtS/yr from 2030 onwards
                * ::srm_scenario = 4:: : emitting 40MtS/yr from 2030 onwards

            **Remark**: Defining possible SO2 emission scenarios (we look at figure 2b of [Helwegen2019])
                        Notice that we need to give Is in Mt of S / year and that figure 2b is in 100 Mt of S / year
                        They have a typo in the y axis of figure 2b.


            :param srm_scenario: int
                Needs to be 0, 1, 2, 3 or 4.
            :return: Callable[[float], float]
                A function that returns the current SO2 injections at a time ::t: float::.
            """

        if srm_scenario == 0:                    # no aerosols
            def injections(t):
                return 0

        elif srm_scenario == 1:                  # SRM only curve
            def injections(t):
                if t < 2015:
                    return 0
                if 2015 <= t < 2400:
                    return 10 + 75 * (t - 2015) / (2400 - 2015)
                if t >= 2400:
                    return 85

        elif srm_scenario == 2:                  # Abatement+SRM curve
            def injections(t):
                if t < 2015:
                    return 0
                if 2015 <= t < 2200:
                    return 35 * np.power(10 / 35, (t - 2200) ** 2 / (2015 - 2200) ** 2)
                if 2200 <= t < 2400:
                    return 35 - 5 * (t - 2200) / 200
                if t >= 2400:
                    return 30  # 0.0001

        elif srm_scenario == 3:                  # emitting 75MtS/yr from 2030 onwards
            def injections(t):
                if t < 2030:
                    return 0
                if t >= 2030:
                    return 75

        elif srm_scenario == 4:                  # emitting 40MtS/yr from 2030 onwards
            def injections(t):
                if t < 2030:
                    return 0
                if t >= 2030:
                    return 40

        else:
            raise ValueError("srm_scenario should be 0, 1, 2, 3 or 4.")

        return lambda t: injections(t)


    # ------------- #
    # #### Plotting #
    # ------------- #

    def plot_emmissions(self, file: str=None) -> None:
        """ Plots the emission rate and cumulated emissions.

            :param file: : str, optional
                If a a string `name` is given, the plots are written to
                `img/co2_rate:name.png` and `img/co2_cumulated:name.png`.
                Otherwise they are displayed.
            :return: None
        """

        # vectorized version of emission functions
        vemissions = np.vectorize(self.emissions)

        # plots of emission rate and cumulative emissions
        xs = np.linspace(2000, 2500, 500)

        plt.plot(xs, vemissions(xs), lw=2)
        plt.xlabel('year')
        plt.ylabel('CO2 emission rate (Gt C / year)')
        plt.title('CO2 emissions scenario')
        plt.grid()
        if type(file) == str:
            plt.savefig("img/co2_rate:"+file)
        else:
            plt.show()

        fig = plt.plot(xs, self.GtCtoppm * np.add.accumulate(vemissions(xs)), 'k--', lw=2)
        plt.xlabel('year')
        plt.ylabel('CO2 cumulative emissions (ppm)')
        plt.title('Cumulative emissions')
        plt.grid()
        if type(file) == str:
            plt.savefig("img/co2_cumulated:"+file)
        else:
            plt.show()

    def plot_injections(self, file=None) -> None:
        """ Plots the SO2 injection rate and cumulated injections.

            :param file: : str, optional
                If a a string `name` is given, the plots are written to
                `img/so2_rate:name.png` and `img/so2_cumulated:name.png`.
                Otherwise they are displayed.
            :return: None
        """
        # vectorized version of injection functions
        vinjections = np.vectorize(self.injections)

        # plots of injection rate and cumulative injections
        xs = np.linspace(2000, 2500, 500)

        plt.plot(xs, vinjections(xs), lw=2)
        plt.xlabel('year')
        plt.ylabel('SO2 injection rate (Mt S / year?)')
        plt.title('SO2 injection scenario')
        plt.grid()
        if type(file) == str:
            plt.savefig("img/so2_rate:"+file)
        else:
            plt.show()

        plt.plot(xs, self.GtCtoppm * np.add.accumulate(vinjections(xs)), 'k--', lw=2)
        plt.xlabel('year')
        plt.ylabel('SO2 cumulative injections (??)')
        plt.title('Cumulative injections')
        plt.grid()
        if type(file) == str:
            plt.savefig("img/so2_cumulated:"+file)
        else:
            plt.show()


    # ---------------------- #
    # Computation of the SDP #
    # ---------------------- #
        """Wrapper for default computations."""

    def compute_SDP(self,
                        x0: State = State((2000, 10),
                                        (0.05, 809, 725, 35642, np.array(2000), np.array(809)),
                                        (10, 0.633, 0.084, np.array(2000), np.array(0.633)), 1)
                        , steps: int = 5):

        self.horizon = x0.get_time() + x0.get_dt() * (steps -1)
        print(self.horizon)

        # mx = self.nexts(0, x0, Ctrl(True))
        # print(mx)

        print("trj:", self.trj(0, [lambda x: True]*steps, x0))

        # print("val:", self.val(0, x0, [lambda x: True]*steps))
        # print("val_iter:", self.val_iter(0, x0, [lambda x: True]*steps))

        # current reward-function needs coordination between number of steps and horizon:
        bi = self.backwards_induction(0, steps)

        bi = bi
        print("val(bi):", self.val_iter(0, x0, bi))

        # ------------- #
        # plot scenario #
        # ------------- #

        self.plot_emmissions("co2:"+str(self.co2_scenario)+",so2:"+str(self.srm_scenario)+".png")
        self.plot_injections("co2:" + str(self.co2_scenario) + ",so2:" + str(self.srm_scenario) + ".png")

        # ------------ #
        # plot results #
        # ------------ #

        #TODO
