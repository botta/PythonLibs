
# Imports
import numpy as np
import random
import multiprocessing as mp

from sdp_libs.examples.BEAMSRMICEControlsExample.SDP import \
    SDP_BEAM_SRM, \
    SDP_family_deterministic, \
    SDP_family_binary_uncertainty
from sdp_libs.examples.BEAMSRMICEControlsExample.State import State
from sdp_libs.examples.BEAMSRMICEControlsExample.Control import *


#----------------------------------------------------------------------------------------------------------------------#
# Testing ...

# Value for fixed climate sensitivity versions of the SDP
cs = 2.4

# value for probability of non-implementation of decisions
prob_no_action = 0.6

sdp = SDP_BEAM_SRM()
sdp1 = SDP_family_deterministic(cs)
sdp2 = SDP_family_binary_uncertainty(cs, prob_no_action)

## Chooose whether to compute trajectories or not:
#co_trjs = True
co_trjs = False

## Chooose whether to print trajectories or not
# (only if co_trjs = True):
#pr_trjs = True
pr_trjs = False

## Choose whether to perform a commitment test for the
# final states of the trajectories (only if co_trjs = True):
#test_comm = True
test_comm = False

## Choose number of steps:
steps = 4

# approach 2'(simple-minded internalized)
#sdp.compute_SDP(steps=steps, compute_trjs=co_trjs, print_trjs=pr_trjs, test_commit=test_comm)

# approach pseudo simply-minded
sdp1.compute_SDP(steps=steps, compute_trjs=co_trjs, print_trjs=pr_trjs, test_commit=test_comm)

# approach pseudo simply-minded + implementation of decisions uncertainty
# sdp2.compute_SDP(steps=steps, compute_trjs= co_trjs, print_trjs=pr_trjs, test_commit=test_comm)

#"""
# ---------------------------------------------------------------- #
# Computing the instances for different climate sensitivity values #
# in parallel vs. "ordinarily"                                     #
# ---------------------------------------------------------------- #
prob_cs = [(1.3, 0.1), (2.5, 0.3), (4.0, 0.5), (6.5, 0.1)]
sdp_list = [SDP_family_deterministic(fst(cspr)) for cspr in prob_cs]
#sdp_list = [SDP_family_binary_uncertainty(fst(cspr), prob_no_action) for cspr in prob_cs]

t1 = timer()
result = Parallel(n_jobs=cpu_count())(delayed(lambda x: x.compute_SDP(steps=steps))(sdp) for sdp in sdp_list)
t2 = timer()

print(f"\n\nParallel computation completed in {t2 - t1:0.4f} seconds.")

t1 = timer()
result = [sdp.compute_SDP(steps=steps) for sdp in sdp_list]
t2 = timer()

print(f"\n\nOrdinary list comprehension computation completed in {t2 - t1:0.4f} seconds.")

#"""


# ------------------------------------- #
# Computing with fixed policy sequences #
# ------------------------------------- #

x0: State = State((2000, 10),
                   (8, 0),
                   (0.05, 809, 725, 35642, np.array(2000), np.array(809)),
                   (1, 0.633, 0.084, np.array(2000), np.array(0.633)), 1, 1.3)

x1: State = State((2000, 10),
                   (8, 0),
                   (0.05, 809, 725, 35642, np.array(2000), np.array(809)),
                   (1, 0.7, 0.2, np.array(2000), np.array(0.633)), 1, 1.3)

# steps: int = 10
# steps: int = 30
# steps: int = 50
# steps: int = 100

sdp.horizon = x0.get_time() + x0.get_dt() * (steps - 1)

# ----------------------------------------------- #
# Uncomment to run commitment tests for x0 or x1:
# ----------------------------------------------- #

#print("commitment test x0:", sdp.test_commitment(x0))
#print("commitment test x1:", sdp.test_commitment(x1))

# ------------------------------------------- #
# Uncomment to run constant policy sequences:
# ------------------------------------------- #

"""

print("Running constant policy sequences:")

pss = [[lambda x: Ctrl(c)]*steps for c in sdp.all_ctrls]
for ps in pss:
    # sdp.run_ps(x0, ps, False, False)  # do not print trajectories, do not test commitment
    sdp.run_ps(x0, ps, False, True)  # do not print trajectories, test commitment
    # sdp.run_ps(x0, ps, True, False)  # print trajectories, do not test commitment
    # sdp.run_ps(x0, ps, True, True)  # print trajectories, test commitment

"""

# ------------------------------------------ #
# Uncomment to run a random policy sequence:
# ------------------------------------------ #

"""

def random_policy(x: State) -> Ctrl:
    random.seed()
    y0 = random.randint(0, 2)
    y1 = random.randint(0, 2)
    return Ctrl((y0, y1))

ps = [random_policy for i in range(steps)]

# sdp.run_ps(x0, ps, False, False)  # do not print trajectories, do not test commitment
sdp.run_ps(x0, ps, False, True)  # do not print trajectories, test commitment
# sdp.run_ps(x0, ps, True, False)  # print trajectories, do not test commitment
# sdp.run_ps(x0, ps, True, True)  # print trajectories, test commitment

"""
