# Toy probabilistic SDP with parameter uncertainty using BEAM+SRM

This directory contains a toy SDP with the BEAM+SRM carbon and temperature models as dynamical subsystems including uncertainty 
about the value of the climate sensitivity parameter.

The SDP is degenerated in the sense that the control space is morally a singleton and
thus there are no decisions to take.

The parameter uncertainty is incorporated by adding a first decision step that simply chooses a value for
climate sensitivity according to a (for the moment arbitrary, invented) probability distribution.

## Specification of the SDP:

**Remarks**:

* To add an abstraction layer between the concrete choice of data type we give names
to the types of individual components of the state space.
This layer is not present in the Python code at the moment.

* The definitions need cleaning up, at the moment they mix different kinds of notation.
They probably should be presented side-by-side in informal, Idris- and Python-syntax.
Maybe use a table?

### State Space

```Python
TimeT, DtTimeT = int
MatT, MupT, MloT, TatT, TloT, TempDtT, CarbonDtT = float
CarbonTimeListT, TempTimeListT, CarbonMatListT, TempTatListT = np.ndarray (having float entries)
ClimateSensitivityT = float

X_Time = (TimeT, DTTimeT)
X_Carbon = (CarbonDtT, MatT, MupT, MloT, CarbonTimeListT, CarbonMatListT)
X_Temp = (TempDtT, TatT, TloT, TempTimeListT, TempTatListT)
X_Param = ClimateSensitivityT

X = (X_Time, X_Carbon, X_Temp, X_Param)
```
In the implementation, actual states are created as instances of the class `State`
(which is a subclass of `AbstractStateClass`):
```Python

x = State(x_time, x_carbon, x_temp, x_param)
```
with
```Python

x_time: X_Time, x_carbon: X_Carbon, x_temp: X_Temp, x_param: X_Param
```

### Control Space

Morally we have

```Python
Y = Unit
```
In the implementation we have a class `Ctrl`, subclass of `AbstractControlClass`
which upon instantiation accepts only values from a list of all available controls, which here is
```Python
all_ctrls : List bool = [True]
```
Actual controls are then created as instances of the `Ctrl` class:
```Python
y = Ctrl(True)
```
This is the only valid control - any other value instead of `True` will raise a `ValueError`.

### Monad

The monad is pre-instantiated to the `SimpleProb` monad, as we implement the SDP
of the class `SP_SDP` which itself is a subclass of `FiniteControlSequentialDecisionProblem`,
and .

```Python
M = SimpleProb
```

### Val, oplus, LTE, zero

```Python
Val = float
zero = 0
oplus = add
LTE = <=
```

### Transition function

### Reward function

For the moment we have just a dummy reward function that uses the auxiliary functions
`is_terminal` and `test_commitment`.
The reward function assigns reward `0` if the next state `xp` is non-terminal.
If `xp` is terminal, it calls the auxiliary function
`commitment_check` which however is constant false at the moment.
Thus the "is_committed=True"-case is unreachable.
The "is_committed=False" case computes the reward as `2000 - Mat`, where `Mat`
is the carbon concentration in the atmosphere in the terminal state.


## Global parameters

(from the BEAM+SRM model)

...

## Auxiliary functions (methods)

### Terminality check

...

### Commitment Test
This is just a placeholder for a real commitment test.
It returns `False` on any input `x`:
```Python
test_commitment(self, x: State) -> bool
self.test_commitment(x) = False
```

### Equilibrium temperature

Computes the equilibrium temperature as in the BEAM+SRM model.

...

### Setting scenarios

The scenarios are taken from the BEAM+SRM code:

#### CO2 scenario

`set_co2_scenario(self, co2_scenario) -> Callable[[float], float]`

Possible CO2 emission scenarios:

 * `co2_scenario = 0` constant CO2 emission rate forever, 8 GtC/year
 * `co2_scenario = 1` A2+ scenario (the one explained in the paper and in the BEAM notebook)
 * `co2_scenario = 2` increase by 5% every 4 years until 50 GtC/yr, then constant. Start with 8 GtC/yr at 2000
 * `co2_scenario = 3` decrease by 5% every 4 years. Start with 8 GtC/yr at 2000


#### SRM scenario

`set_srm_scenario(self, srm_scenario) -> Callable[[float], float]`

Possible aerosol injection scenarios:

* `srm_scenario = 0`: no aerosols
* `srm_scenario = 1`: SRM only curve
* `srm_scenario = 2`: Abatement+SRM curve
* `srm_scenario = 3`: emitting 75MtS/yr from 2030 onwards
* `srm_scenario = 4`: emitting 40MtS/yr from 2030 onwards

*Remarks*: Defining possible SO2 emission scenarios (we look at figure 2b of [Helwegen2019]):
  Notice we need to give Is in Mt of S / year and that figure 2b is in 100 Mt of S / year.
  They have a typo in the y axis of figure 2b.

#### Compute CO2 emissions

...

#### Compute SO2 injections

...

#### Computation

`compute_BEAMSRM(self, x0: State = default_initial_state
                        , steps: int = default_steps)`

with

```Python
default_initial_State = State(  (2000, 10),
                                (0.05, 809, 725, 35642, np.array(2000), np.array(809)),
                                (10, 0.633, 0.084, np.array(2000), np.array(0.633)),
                                1
                              )
default_steps = 10
```

...

### Plotting

(not working properly as of yet)

`plot_emmissions(self, file=None) -> None`

`plot_injectionss(self, file=None) -> None`

If given a file the plots are redirected to .png files in the `img` subdirectory.
...
