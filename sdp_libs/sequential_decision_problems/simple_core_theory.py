#!/usr/bin/env python
# coding: utf-8

# Port from *IdrisLibs/lectures/.../NaiveTheory.lidr*

# imports

from typing import Any, Union, Iterable

# pythonLibs imports
from sdp_libs.category_theory.definitions import Monad
from sdp_libs.order_theory.preorder import PointedPreorderSemiGroup
from sdp_libs.utils.base import *  # head, tail, cons, fst, snd

# Type variables

A = TypeVar('A')

"""
----------------------------------------
## Generic sequential decision processes
----------------------------------------
"""


# Deviating from the naming convention for fields that
# will serve as "types": for these we follow the naming convention for classes.

# potentially split up into two classes: The underlying dynamical system and the optimality part.

class SequentialDecisionProcess:

    def __init__(self, monad: Monad, State, Ctrl, nexts):
        self.monad = monad
        self.State = State
        self.Ctrl = Ctrl
        self.nexts = nexts

    def monad(self) -> Monad:
        """returns the monad."""
        return self.monad

    def __mco(self):
        return self.monad.m_functor.src.obj

    def __M(self, X: object) -> object:
        return self.monad.m_functor.f_obj(X)

    def __m_map(self, f: Callable[[__mco], __mco]) -> Callable[['__M(__mco)'], '__M(__mco)']:
        return self.monad.m_functor.f_mor(f)

    def __m_pure(self, x: __mco) -> '__M(__mco)':
        return self.monad.m_pure(x)

    def __m_bind(self, f, mx):
        return self.monad.m_bind(f, mx)

    def State(self) -> type:
        """returns the state space."""
        return self.State

    def Ctrl(self) -> type:
        """returns the control space."""
        return self.Ctrl

    def nexts(self, t: int, x: State, y: Ctrl) -> '__M(State)':
        """return monadic transition function s.t.
        nexts(t,x,y) is the successor state at time t+1."""
        return self.nexts(t, x, y)

    Policy = Callable[[State], Ctrl]

    PolicySeq = List[Policy]

    # TODO: maybe change to "uniform" version of StateCtrlSeq?
    #  would also allow to use head_left from utils instead of head_state
    StateCtrlSeq = List[Union[Tuple[State, Ctrl], State]]

    @staticmethod
    def head_state(scs: StateCtrlSeq) -> State:
        if scs == []:
            raise ValueError("Empty StateCtrlSequence!")
        elif len(scs) == 1:
            return head(scs)
        else:
            return fst(head(scs))

    def trj(self, t: int, ps: PolicySeq, x: State) -> '__M(StateCtrlSeq)':

        # define some notation for readability and type annotations

        M = self.__M
        State = self.State
        StateCtrlSeq = self.StateCtrlSeq
        PolicySeq = self.PolicySeq
        m_map = self.__m_map
        m_bind = self.__m_bind
        trj = self.trj

        # calculation of the trajectories

        if is_nil(ps):
            result = self.__m_pure([x])

        else:
            p = head(ps)
            psp = tail(ps)

            y = p(x)
            mxp = self.nexts(t, x, y)

            def trj_ps(t: int, ps: PolicySeq) -> Callable[[State], 'M(StateCtrlSeq)']:
                return lambda x: trj(t, ps, x)

            trajectories = trj_ps(t + 1, psp)
            bind_trj_mxp = m_bind(trajectories, mxp)
            result = m_map(cons((x, y)))(bind_trj_mxp)

        return result


class FiniteControlSequentialDecisionProblem(SequentialDecisionProcess):

    def __init__(self, monad: Monad, State, Ctrl, nexts, val_ord: PointedPreorderSemiGroup,
                 measure, reward, to_iterable_ctrl):
        super().__init__(monad, State, Ctrl, nexts)
        self.val_ord = val_ord
        self.measure = measure
        self.reward = reward
        self.to_iterable_ctrl = to_iterable_ctrl

    def to_iterable_ctrl(self) -> Iterable:
        """returns an iterable object containing the controls."""
        return self.to_iterable_ctrl()

    def val_ord(self) -> PointedPreorderSemiGroup:
        """returns the underlying type of val_ord."""
        return self.val_ord

    def __Val(self) -> type:
        return self.val_ord.base_type

    @property
    def __zero(self) -> __Val:
        return self.val_ord.base_point

    def __le(self, v1: __Val, v2: __Val) -> __Val:
        return self.val_ord.comparison_op(v1, v2)

    def __oplus(self, v1: __Val, v2: __Val) -> __Val:
        return self.val_ord.bin_op(v1, v2)

    def ooplus(self, f: Callable[[Any], __Val], g: Callable[[Any], __Val]) -> Callable[[Any], __Val]:
        return lambda x: self.__oplus(f(x), g(x))

    def measure(self, mv: '__M(__Val)') -> __Val:
        """measure function, e.g. expectedValue."""
        return self.measure(mv)

    def reward(self) -> Callable[[int, 'State', 'Ctrl', 'State'], __Val]:
        """The *reward* / *cost* / *utility* function"""
        return self.reward

    Policy = Callable[['State'], 'Ctrl']

    PolicySeq = List['Policy']

    StateCtrlSeq = List[Union[Tuple['State', 'Ctrl'], 'State']]

    def sum_of_rewards(self, t: int, scs: StateCtrlSeq) -> __Val:

        if is_nil(scs):
            raise ValueError('Problem in sum_of_rewards function: empty StateCtrlSequence.')

        elif is_singleton(scs):
            result = self.__zero

        else:
            sc = head(scs)
            s = fst(sc)
            c = snd(sc)
            scsp = tail(scs)

            if len(scsp) == 1:
                sp = head(scsp)

            else:
                spcp = head(scsp)
                sp = fst(spcp)

            sr = self.reward(t, s, c, sp) + self.sum_of_rewards(t, scsp)
            result = sr

        return result

    def valp(self, t: int, x: 'State', ps: PolicySeq) -> __Val:

        m_map = self.monad.m_functor.f_mor

        trajectories = self.trj(t, ps, x)
        sr = m_map(lambda r: self.sum_of_rewards(t, r))(trajectories)

        meas = self.measure(sr)

        return meas

    def sval(self, t: int, x: 'State', y: 'Ctrl', ps: PolicySeq) -> Callable[['State'],__Val]:

        def rv(xp):
            r = self.reward(t, x, y, xp)
            v = self.val(t + 1, xp, ps)

            return self.__oplus(r, v)

        return rv

    def cval(self, t: int, x: 'State', ps: PolicySeq, y: 'Ctrl') -> __Val:

        m_map = self.monad.m_functor.f_mor

        mxp = self.nexts(t, x, y)
        cvy = self.measure(m_map(self.sval(t, x, y, ps))(mxp))
        return cvy

    def val(self, t: int, x: 'State', ps: PolicySeq) -> __Val:

        if is_nil(ps):
            result = self.__zero
        else:
            p = head(ps)
            psp = tail(ps)
            y = p(x)

            result = self.cval(t, x, psp, y)

        return result

    def cval_argmax(self, t: int, x: 'State', ps: PolicySeq) -> 'Ctrl':

        cval_list = [(y, self.cval(t, x, ps, y)) for y in self.to_iterable_ctrl]

        argmax = fst(cval_list[0])
        v_argmax = snd(cval_list[0])

        for ycv in cval_list:

            if self.__le(v_argmax, snd(ycv)):
                argmax = fst(ycv)

        return argmax

    def optimal_extension(self, t: int, ps: PolicySeq) -> Policy:
        """returns the optimal_extension function"""
        return lambda x: self.cval_argmax(t, x, ps)

    def backwards_induction(self, t: int, n: int) -> PolicySeq:
        if n == 0:
            return []
        else:
            ps = self.backwards_induction(t + 1, n - 1)
            return [self.optimal_extension(t, ps)] + ps
